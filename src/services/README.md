# Services

Attempt to break existing laterail api monolith (`src/monolith`) into \[micro\]services which can be independently scaled.
- [branch `epic/46`](https://gitlab.com/alexchadwick/laterail-go/-/tree/epic/46-messaged-based-microservice-for-external-api-requests)

- [producer](./cmd/producer/README.md): Accepts user request, sends message(s) to queue service
- [consumer](./cmd/consumer/README.md): Pulls messages from queue service and processes
- [events](./cmd/events/README.md): Track `consumer` progress via Server Side Events