package db

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
)

var Conn *pgxpool.Pool

// Connect to postgres database using URI set by env variable
func AddConnection(url string) error {
	config, err := pgxpool.ParseConfig(url)
	if err != nil {
		return fmt.Errorf("unable to set database Connection config: %q", err)
	}
	config.MaxConns = 50
	config.MinConns = 5
	config.MaxConnLifetime = 30 * time.Minute //default 1hr

	Conn, err = pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		return fmt.Errorf("unable to Connect to database: %q", err)
	}

	return nil
}

func NewTestDatabaseWithConfig(t testing.TB, server string) *pgxpool.Pool {
	t.Helper()

	// create pool and context
	ctx := context.Background()
	pool, err := dockertest.NewPool("")
	if err != nil {
		t.Fatalf("Could not connect to docker: %s", err)
	}

	// start postgres container
	dbname, u, p := "test", "postgres", "secret"
	container, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "12-alpine",
		Env: []string{
			"LANG=C",
			"POSTGRES_DB=" + dbname,
			"POSTGRES_USER=" + u,
			"POSTGRES_PASSWORD=" + p,
		},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{Name: "no"}
	})
	if err != nil {
		t.Fatalf("Could not start container: %s", err)
	}

	// Ensure container is cleaned up
	defer t.Cleanup(func() {
		if err := pool.Purge(container); err != nil {
			t.Fatalf("failed to cleanup postgres container: %s", err)
		}
	})

	// Create database connection pool and test connection
	var connURL string
	if err = pool.Retry(func() error {
		var err error
		connURL = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", u, p, server, container.GetPort("5432/tcp"), dbname)
		config, err := pgxpool.ParseConfig(connURL)
		if err != nil {
			return fmt.Errorf("unable to set database connection config: %q", err)
		}
		Conn, err = pgxpool.ConnectConfig(ctx, config)
		if err != nil {
			return err
		}
		return Conn.Ping(ctx)
	}); err != nil {
		t.Fatalf("Could not connect to docker: %s", err)
	}

	// Create role required for migrations
	_, err = Conn.Exec(context.Background(),
		`-- name: Add role write_app, used in migration :exec
		CREATE ROLE write_users`)
	if err != nil {
		t.Fatalf("failed to create role write_app: %s", err)
	}

	// Run the migrations.
	if err := Migrate(connURL); err != nil {
		t.Fatalf("failed to migrate database: %s", err)
	}

	container.Expire(120) // Tell docker to hard kill the container in 120 seconds
	return Conn
}
