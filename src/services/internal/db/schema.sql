BEGIN;

create table stations (
    station_id varchar(4) not null unique primary key,
    station_name varchar(50)
);
GRANT SELECT, INSERT, UPDATE ON TABLE stations TO write_users;

create table services (
    service_id int not null unique primary key,
    origin_station varchar(4) not null references stations(station_id),
    destination_station varchar(4) not null references stations(station_id),
    list_stations text[]
);
GRANT SELECT, INSERT, UPDATE ON TABLE services TO write_users;

create table runs (
    rid bigint not null unique primary key,
    service_id int not null references services(service_id)
);
GRANT SELECT, INSERT, UPDATE ON TABLE runs TO write_users;

create table details (
    detail_id serial unique primary key,
    rid bigint not null references runs(rid),
    service_id int not null references services(service_id),
    station varchar(4) not null references stations(station_id),
    sched_depart timestamp with time zone, 
    sched_arrival timestamp with time zone,
    actual_depart timestamp with time zone, 
    actual_arrival timestamp with time zone,
    delay_depart int,
    delay_arrival int,
    late_canc_reason int
);
GRANT SELECT, INSERT, UPDATE ON TABLE details TO write_users;

END;