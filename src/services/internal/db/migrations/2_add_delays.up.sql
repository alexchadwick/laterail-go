BEGIN;

ALTER TABLE IF EXISTS details
ALTER COLUMN delay DROP NOT NULL,
ADD COLUMN IF NOT EXISTS  delay_depart int,
ADD COLUMN IF NOT EXISTS  delay_arrival int,
DROP COLUMN IF EXISTS delay;

END;