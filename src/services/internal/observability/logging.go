package observability

import (
	"os"

	"github.com/rs/zerolog"
)

// Set log levels in zerolog. Defaults to Debug
func SetGlobalLevel(level string) zerolog.Logger {
	log := zerolog.New(os.Stdout).With().Timestamp().Logger()
	if level == "INFO" {
		level = "release"
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		level = "debug"
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	return log
}
