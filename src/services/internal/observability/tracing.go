package observability

import (
	"context"
	"fmt"
	"runtime"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
)

// A tracer which is consumed when creating spans
var Tracer = otel.GetTracerProvider().Tracer(tracerName)

const (
	tracerName = "gitlab.com/alexchadwick/laterail"
)

type ErrorWithTrace struct {
	TraceID string
	SpanID  string
	Err     error
}

func (e *ErrorWithTrace) Error() string {
	return fmt.Sprint(e.Err)
}

func InitTracerProvider(url, serviceName, env string) (*sdktrace.TracerProvider, error) {
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
	if err != nil {
		return nil, fmt.Errorf("failed to create Jaeger exporter: %w", err)
	}

	// Generate new service UUID
	uuid := uuid.New().String()

	tp := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithBatcher(exp),
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(serviceName),
			attribute.String("environment", env),
			attribute.String("ID", uuid),
		)),
	)

	// Required to use var Tracer
	otel.SetTracerProvider(tp)

	// Enables propagation of trace context to downstream services
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}))

	return tp, nil
}

// Start new span, using function/method name from runtime.Caller()
func StartSpan(ctx context.Context) (context.Context, trace.Span, string, string) {
	pc, _, _, _ := runtime.Caller(1)
	funcName := runtime.FuncForPC(pc)

	ctx, span := Tracer.Start(ctx, funcName.Name())
	traceID, spanID := GetTraceAndSpanID(span)

	return ctx, span, traceID, spanID
}

// Get trace and span id from span context
func GetTraceAndSpanID(span trace.Span) (string, string) {
	spanContext := span.SpanContext()
	return spanContext.TraceID().String(), spanContext.SpanID().String()
}

// Wrap errors with trace info and record errors in trace
// Use when bubbling up/returning errors
func NewErrorWithTrace(span trace.Span, err error) *ErrorWithTrace {
	span.RecordError(err)
	span.SetStatus(codes.Error, err.Error())
	spanContext := span.SpanContext()

	return &ErrorWithTrace{
		TraceID: spanContext.TraceID().String(),
		SpanID:  spanContext.SpanID().String(),
		Err:     err,
	}
}

// Add trace info to err log and record error message in trace
// Use when logging errors
func NewErrorLogWithTrace(err error, span trace.Span, msg string) {
	// Get IDs as string from span context
	spanContext := span.SpanContext()

	// Add error and status code to trace span
	span.RecordError(err)
	span.SetStatus(codes.Error, err.Error())

	log.Error().
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msg(msg)
}

// Add trace info to fatal log and record error message in trace
// Use when logging fatal errors
func NewFatalLogWithTrace(err error, span trace.Span, msg string) {
	// Get IDs as string from span context
	spanContext := span.SpanContext()

	// Add error and status code to trace span
	span.RecordError(err)
	span.SetStatus(codes.Error, err.Error())

	log.Fatal().
		Err(err).
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msg(msg)
}
