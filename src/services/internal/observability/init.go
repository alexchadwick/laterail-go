package observability

import (
	"fmt"

	"github.com/rs/zerolog"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
)

// Creates a new logger with the specified log level and a global tracer
// provider
func Init(logLevel, traceURL, serviceName, environment string) (zerolog.Logger, *sdktrace.TracerProvider, error) {
	log := SetGlobalLevel(logLevel)

	tp, err := InitTracerProvider(traceURL, serviceName, environment)
	if err != nil {
		return log, nil, fmt.Errorf("failed to initialize tracer provider: %w", err)
	}

	return log, tp, nil
}
