package observability

import (
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog/log"
)

type Metric struct {
	Name       string
	Help       string
	Objectives map[float64]float64
	Buckets    []float64
	Type       string
	Label      string
}

type MetricsCollection struct {
	CounterMap    map[string]*prometheus.CounterVec
	HistogramMap  map[string]*prometheus.HistogramVec
	HistogramList []*prometheus.HistogramVec
	SummaryMap    map[string]*prometheus.SummaryVec
}

// Creates and registers new prometheus metrics
func InitMetrics(metrics []Metric) (MetricsCollection, error) {

	var c = map[string]*prometheus.CounterVec{}
	var s = map[string]*prometheus.SummaryVec{}
	var hm = map[string]*prometheus.HistogramVec{}
	var h []*prometheus.HistogramVec

	for _, v := range metrics {
		if v.Type == "counter" {
			counter := prometheus.NewCounterVec(
				prometheus.CounterOpts{
					Name: v.Name,
					Help: v.Help,
				},
				[]string{v.Label})
			c[v.Name] = counter
			if err := prometheus.Register(counter); err != nil {
				log.Error().Err(err).Msgf("Failed to register metric: %s", v.Name)
				return MetricsCollection{}, err
			}
		} else if v.Type == "summary" {
			summary := prometheus.NewSummaryVec(
				prometheus.SummaryOpts{
					Name:       v.Name,
					Help:       v.Help,
					Objectives: v.Objectives,
				},
				[]string{v.Label})
			s[v.Name] = summary
			if err := prometheus.Register(summary); err != nil {
				log.Error().Err(err).Msgf("Failed to register metric: %s", v.Name)
				return MetricsCollection{}, err
			}
		} else if v.Type == "histogram" {
			histogram := prometheus.NewHistogramVec(
				prometheus.HistogramOpts{
					Name:    v.Name,
					Help:    v.Help,
					Buckets: v.Buckets,
				},
				[]string{v.Label})
			h = append(h, histogram)
			hm[v.Name] = histogram
			if err := prometheus.Register(histogram); err != nil {
				log.Error().Err(err).Msgf("Failed to register metric: %s", v.Name)
				return MetricsCollection{}, err
			}
		} else {
			err := fmt.Errorf("failed to register metric %s", v.Name)
			log.Error().Err(err).Msgf("Unknown Metric type %s", v.Type)
			return MetricsCollection{}, err
		}
	}

	collection := MetricsCollection{
		CounterMap:    c,
		HistogramList: h,
		HistogramMap:  hm,
		SummaryMap:    s,
	}

	return collection, nil
}
