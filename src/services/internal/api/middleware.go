package api

import (
	"context"
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel/trace"
	"laterail.com/internal/observability"
)

// Used for context.Context value, requires key with non-primitive type
type ContextKey string

// Default metrics for all requests
var requestMetrics = []observability.Metric{
	{
		Name:  "laterail_endpoint_requests_total",
		Help:  "Total number of requests processed",
		Type:  "counter",
		Label: "path",
	},
	{
		Name:       "laterail_summary_request_duration_seconds",
		Help:       "Latency for processed requests",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		Type:       "summary",
		Label:      "path",
	},
	{
		Name:    "laterail_histogram_request_duration_seconds",
		Help:    "Latency for processed requests",
		Buckets: prometheus.ExponentialBucketsRange(0.001, 720, 20),
		Type:    "histogram",
		Label:   "path",
	},
	{
		Name:       "laterail_response_size_bytes",
		Help:       "Size of response for processed requests (bytes)",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		Type:       "summary",
		Label:      "path",
	},
}

const ContextKeyRequestID ContextKey = "requestID"

// Set UUID request ID for each request
func RequestID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		ctx := context.WithValue(
			r.Context(),
			ContextKeyRequestID,
			uuid.New().String(),
		)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

// Chi request logger, extracts trace and space id from request context
// Dependent on github.com/riandyrn/otelchi which manages these spans
func Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		t := time.Now().UTC()
		wrappedRw := middleware.NewWrapResponseWriter(rw, r.ProtoMajor)
		next.ServeHTTP(wrappedRw, r)

		latency := float32(time.Since(t).Seconds())
		log.Debug().
			Str("trace_id", trace.SpanFromContext(r.Context()).SpanContext().TraceID().String()).
			Str("span_id", trace.SpanFromContext(r.Context()).SpanContext().SpanID().String()).
			Str("request_id", r.Context().Value(ContextKeyRequestID).(string)).
			Str("client_ip", r.RemoteAddr).
			Str("user_agent", r.Header.Get("User-Agent")).
			Str("method", r.Method).
			Str("path", r.URL.Path).
			Float32("latency", latency).
			Int("status", wrappedRw.Status()).
			Str("bytes_in", r.Header.Get("Content-Length")).
			Int("bytes_out", wrappedRw.BytesWritten()).
			Send()
	})
}

// MetricsHandler is a chi middleware handler that records metrics for each
// request which doesn't use the OPTIONS method.
func Metrics(col observability.MetricsCollection) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()

			// Run other pending handlers then return
			wrappedRw := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
			next.ServeHTTP(wrappedRw, r)
			elapsed := time.Since(start)

			// Don't record metrics for OPTIONS requests
			if r.Method != "OPTIONS" {
				// Aggregate metrics by first 3 path segments. Removing request ID from some request paths
				path := strings.Join(strings.Split(r.URL.Path, "/")[:3], "/")
				col.CounterMap["laterail_endpoint_requests_total"].WithLabelValues(path).Inc()
				col.SummaryMap["laterail_summary_request_duration_seconds"].WithLabelValues(path).Observe(elapsed.Seconds())
				col.HistogramMap["laterail_histogram_request_duration_seconds"].WithLabelValues(path).Observe(elapsed.Seconds())
				col.SummaryMap["laterail_response_size_bytes"].WithLabelValues(path).Observe(float64(wrappedRw.BytesWritten()))
			}
		})
	}
}
