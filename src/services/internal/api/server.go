package api

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	"laterail.com/internal/observability"

	"github.com/go-chi/cors"
	"github.com/riandyrn/otelchi"
)

type Config struct {
	Name         string        `mapstructure:"service-name"`
	Port         int           `mapstructure:"port"`
	PortMetrics  int           `mapstructure:"port-metrics"`
	IdleTimeout  time.Duration `mapstructure:"http-idle-timeout"`
	ReadTimeout  time.Duration `mapstructure:"http-read-timeout"`
	WriteTimeout time.Duration `mapstructure:"http-write-timeout"`
}

type Server struct {
	config         *Config
	logger         *zerolog.Logger
	metrics        observability.MetricsCollection
	Router         *chi.Mux
	tracerProvider *sdktrace.TracerProvider
}

func NewServer(
	config Config,
	logger *zerolog.Logger,
	tracerProvider *sdktrace.TracerProvider,
) (*Server, error) {

	m, err := observability.InitMetrics(requestMetrics)
	if err != nil {
		return &Server{}, err
	}

	return &Server{
		config:         &config,
		logger:         logger,
		metrics:        m,
		Router:         chi.NewRouter(),
		tracerProvider: tracerProvider,
	}, nil
}

func (s *Server) ListenAndServe(optionalRoutes ...chi.Router) *http.Server {
	go s.startMetricsServer()

	s.addMiddleware()
	s.addRoutes(optionalRoutes)

	srv := &http.Server{
		Addr:         fmt.Sprintf("0.0.0.0:%v", s.config.Port),
		Handler:      s.Router,
		IdleTimeout:  s.config.IdleTimeout,
		ReadTimeout:  s.config.ReadTimeout,
		WriteTimeout: s.config.WriteTimeout,
	}

	go func() {
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			s.logger.Fatal().Err(err).Msg("chi server error")
		}
	}()

	return srv
}

// Adds default middleware
func (s *Server) addMiddleware() {
	s.Router.Use(middleware.Recoverer)
	s.Router.Use(RequestID)
	s.Router.Use(otelchi.Middleware(s.config.Name, otelchi.WithChiRoutes(s.Router), otelchi.WithTracerProvider(s.tracerProvider)))
	s.Router.Use(Logger)
	s.Router.Use(Metrics(s.metrics))
	s.Router.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type"},
		AllowCredentials: true,
		MaxAge:           300, // Max not ignored by any of major browsers
	}))
}

func (s *Server) addRoutes(optionalRoutes []chi.Router) {
	// Add optional chi routers at the root
	for _, route := range optionalRoutes {
		s.Router.Mount("/", route)
	}
}

// Run prometheus metrics and health endpoints on their own http server
func (s *Server) startMetricsServer() {
	mux := http.DefaultServeMux
	mux.Handle("/metrics", promhttp.Handler())
	mux.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("OK"))
	})

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%v", s.config.PortMetrics),
		Handler: mux,
	}

	srv.ListenAndServe()
}
