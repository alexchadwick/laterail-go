package initial

import (
	"context"
	"fmt"
	"sort"
	"time"

	"laterail.com/internal/hsp"
	"laterail.com/internal/observability"
)

const (
	Weekday          = "WEEKDAY"
	Saturday         = "SATURDAY"
	Sunday           = "SUNDAY"
	timeRefTimeRange = "1504"
	timeRefDateRange = "2006-01-02"
	timeRefDatetime  = "200601021504"
)

type InitialRequest struct {
	FromLoc   string `json:"from_loc,omitempty" validate:"required,csr"`
	ToLoc     string `json:"to_loc,omitempty" validate:"required,csr"`
	FromTime  string `json:"from_time,omitempty" validate:"required,time"`
	ToTime    string `json:"to_time,omitempty" validate:"required,time"`
	FromDate  string `json:"from_date,omitempty" validate:"required,date"`
	ToDate    string `json:"to_date,omitempty" validate:"required,date"`
	Tolerance []int  `json:"tolerance,omitempty"`
}

// Break initial requests into var ir.TimeRangeMax hour intervals
func (ir InitialRequest) TimeRange(timeRangeHrMax time.Duration) ([]time.Time, error) {
	fromDateTime, err := time.Parse(timeRefTimeRange, ir.FromTime)
	if err != nil {
		return nil, fmt.Errorf("failed to parse time.Time: %w", err)
	}
	toDateTime, err := time.Parse(timeRefTimeRange, ir.ToTime)
	if err != nil {
		return nil, fmt.Errorf("failed to parse time.Time: %w", err)
	}

	irPeriods := newTimeSlice(fromDateTime, toDateTime, timeRangeHrMax)

	return irPeriods, nil
}

// Break initial requests into var ir.DateRangeMax date intervals
func (ir InitialRequest) DateRange(dateRangeDayMax time.Duration) ([]time.Time, error) {
	fromDateTime, err := time.Parse(timeRefDateRange, ir.FromDate)
	if err != nil {
		return nil, fmt.Errorf("failed to parse time.Time: %w", err)
	}
	toDateTime, err := time.Parse(timeRefDateRange, ir.ToDate)
	if err != nil {
		return nil, fmt.Errorf("failed to parse time.Time: %w", err)
	}

	irPeriods := newTimeSlice(fromDateTime, toDateTime, dateRangeDayMax)

	return irPeriods, nil
}

// Create slice of ServiceMetrics requests. Processed into time, date and day intervals
func (ir InitialRequest) Split(ctx context.Context, dateRangeDayMax, timeRangeHrMax time.Duration) ([]hsp.MetricsRequest, error) {
	ctx, span, _, _ := observability.StartSpan(ctx)
	defer span.End()

	var requestSlice []hsp.MetricsRequest
	times, err := ir.TimeRange(timeRangeHrMax)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to create initial request time range: %w", err))
		return nil, err
	}
	dates, err := ir.DateRange(dateRangeDayMax)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to create initial request date range: %w", err))
		return nil, err
	}
	timesString := sliceTimeToString(times, timeRefTimeRange)
	datesString := sliceTimeToString(dates, timeRefDateRange)

	// Loop over date, time and day type ranges
	for i := 0; i < len(datesString)-1; i++ {
		days, err := getDayTypes(dates[i], dates[i+1])
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed evaluate day types in date range: %w", err))
			return nil, err
		}

		for j := 0; j < len(timesString)-1; j++ {
			for k := 0; k < len(days); k++ {
				requestSlice = append(requestSlice, hsp.MetricsRequest{
					FromLoc:  ir.FromLoc,
					ToLoc:    ir.ToLoc,
					FromTime: timesString[j],
					ToTime:   timesString[j+1],
					FromDate: datesString[i],
					ToDate:   datesString[i+1],
					Days:     days[k],
				})
			}
		}
	}
	return requestSlice, nil
}

// Build slice of time.Time using interval of time.Duration
func newTimeSlice(start, end time.Time, interval time.Duration) []time.Time {
	t := []time.Time{}
	delta := end.Sub(start)
	timeItems := delta / interval

	// t := []time.Time{start, n+1 * interval, ... , len(timeItems) * interval, end}
	t = append(t, start)
	for i := 0; i < int(timeItems); i++ {
		n := time.Duration(i + 1)
		j := start.Add(n * interval)
		t = append(t, j)
	}
	t = append(t, end)

	return t
}

// Convert []time.Time to []String using layout
func sliceTimeToString(timeSlice []time.Time, layout string) []string {
	stringSlice := []string{}

	for i := 0; i < len(timeSlice); i++ {
		s := timeSlice[i].Format(layout)
		stringSlice = append(stringSlice, s)
	}

	return stringSlice
}

// Sort into WEEKDAY (1-5), SAT (6) or SUN (0)
func getDayGroup(dateTime time.Time) string {
	var day string
	if dateTime.Weekday() < 6 && dateTime.Weekday() > 0 {
		day = Weekday
	} else if dateTime.Weekday() == 6 {
		day = Saturday
	} else if dateTime.Weekday() == 0 {
		day = Sunday
	}
	return day
}

// Check if ServiceMetrics Days types (e.g. WEEK/SAT/SUNDAY) are represented in date range
func getDayTypes(start, end time.Time) ([]string, error) {
	startyd := start.YearDay()
	endyd := end.YearDay()
	totalYearDays := (endyd - startyd) + 1

	daytypeMap := map[string]bool{
		"WEEKDAY":  false,
		"SATURDAY": false,
		"SUNDAY":   false,
	}

	// Loop over dates, check day type, flip the map bool if found and break if all keys true
	t := start
	for i := 0; i < int(totalYearDays) && (!daytypeMap["WEEKDAY"] || !daytypeMap["SATURDAY"] || !daytypeMap["SUNDAY"]); i++ {
		j := getDayGroup(t)
		value, ok := daytypeMap[j]
		if ok && !value {
			daytypeMap[j] = true
		}

		t = t.Add(24 * time.Hour)
	}

	dayList := []string{}
	for k, v := range daytypeMap {
		if v {
			dayList = append(dayList, k)
		}
	}
	sort.Strings(dayList)

	return dayList, nil
}
