package initial

import (
	"context"
	"os"
	"reflect"
	"testing"
	"time"

	"laterail.com/internal/hsp"
)

var (
	ir = InitialRequest{
		FromLoc:   "BDG",
		ToLoc:     "GLC",
		FromTime:  "0600",
		ToTime:    "2300",
		FromDate:  "2021-06-01",
		ToDate:    "2021-08-31",
		Tolerance: []int{},
	}
)

const (
	irTimeRangeMax = 4 * time.Hour
	irDateRangeMax = 14 * 24 * time.Hour
)

func TestMain(m *testing.M) {
	// Prepare stuff for tests

	//Run tests
	code := m.Run()
	os.Exit(code)
}

func Test_DateRange(t *testing.T) {

	w, _ := time.Parse(timeRefDateRange, ir.FromDate)
	last, _ := time.Parse(timeRefDateRange, ir.ToDate)
	want := []time.Time{
		w,
		w.Add(irDateRangeMax),
		w.Add(2 * irDateRangeMax),
		w.Add(3 * irDateRangeMax),
		w.Add(4 * irDateRangeMax),
		w.Add(5 * irDateRangeMax),
		w.Add(6 * irDateRangeMax),
		last,
	}
	got, err := ir.DateRange(irDateRangeMax)
	if err != nil {
		t.Fatalf("failed to create DateRange slice: %v", err)
	}

	for i := 0; i < len(want); i++ {
		if !reflect.DeepEqual(got[i], want[i]) {
			t.Errorf("InitialRequest.DateRange(), index %d = %v, want %v", i, got[i], want[i])
		}
	}
}

func Test_TimeRange(t *testing.T) {

	w, _ := time.Parse(timeRefTimeRange, ir.FromTime)
	last, _ := time.Parse(timeRefTimeRange, ir.ToTime)
	want := []time.Time{
		w,
		w.Add(6 * time.Hour),
		w.Add(12 * time.Hour),
		last,
	}
	got, err := ir.TimeRange(irTimeRangeMax)
	if err != nil {
		t.Fatalf("failed to create TimeRange slice: %v", err)
	}

	for i := 0; i < len(want); i++ {
		if !reflect.DeepEqual(got[i], want[i]) {
			t.Errorf("InitialRequest.TimeRange(), index %d = %v, want %v", i, got[i], want[i])
		}
	}
}

func Test_Split(t *testing.T) {

	ctx := context.Background()
	t.Run("Large", func(t *testing.T) {
		want := []hsp.MetricsRequest{
			{"BDG", "GLC", "0600", "1200", "2021-06-01", "2021-06-15", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-01", "2021-06-15", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-01", "2021-06-15", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-01", "2021-06-15", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-01", "2021-06-15", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-01", "2021-06-15", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-01", "2021-06-15", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-01", "2021-06-15", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-01", "2021-06-15", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-15", "2021-06-29", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-15", "2021-06-29", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-15", "2021-06-29", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-15", "2021-06-29", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-15", "2021-06-29", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-15", "2021-06-29", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-15", "2021-06-29", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-15", "2021-06-29", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-15", "2021-06-29", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-29", "2021-07-13", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-29", "2021-07-13", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-29", "2021-07-13", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-29", "2021-07-13", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-29", "2021-07-13", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-29", "2021-07-13", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-29", "2021-07-13", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-29", "2021-07-13", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-29", "2021-07-13", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-13", "2021-07-27", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-13", "2021-07-27", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-13", "2021-07-27", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-13", "2021-07-27", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-13", "2021-07-27", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-13", "2021-07-27", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-13", "2021-07-27", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-13", "2021-07-27", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-13", "2021-07-27", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-27", "2021-08-10", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-27", "2021-08-10", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-27", "2021-08-10", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-27", "2021-08-10", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-27", "2021-08-10", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-27", "2021-08-10", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-27", "2021-08-10", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-27", "2021-08-10", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-27", "2021-08-10", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-10", "2021-08-24", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-10", "2021-08-24", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-10", "2021-08-24", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-10", "2021-08-24", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-10", "2021-08-24", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-10", "2021-08-24", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-10", "2021-08-24", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-10", "2021-08-24", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-10", "2021-08-24", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-24", "2021-08-31", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-24", "2021-08-31", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-24", "2021-08-31", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-24", "2021-08-31", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-24", "2021-08-31", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-24", "2021-08-31", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-24", "2021-08-31", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-24", "2021-08-31", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-24", "2021-08-31", "WEEKDAY", hsp.MetricsRequestTolerance},
		}
		got, err := ir.Split(ctx, irDateRangeMax, irTimeRangeMax)
		if err != nil {
			t.Fatalf("failed to splitting InitialRequest: %v", err)
		}

		for i := 0; i < len(want); i++ {
			if !reflect.DeepEqual(got[i], want[i]) {
				t.Errorf("InitialRequest.TimeRange(), index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})

	t.Run("Small", func(t *testing.T) {
		want := []hsp.MetricsRequest{
			{"BDG", "GLC", "0600", "0630", "2021-07-31", "2021-07-31", "SATURDAY", hsp.MetricsRequestTolerance},
		}
		ir := InitialRequest{"BDG", "GLC", "0600", "0630", "2021-07-31", "2021-07-31", []int{1, 5, 10}}
		got, err := ir.Split(ctx, irDateRangeMax, irTimeRangeMax)
		if err != nil {
			t.Fatalf("failed to splitting InitialRequest: %v", err)
		}

		for i := 0; i < len(want); i++ {
			if !reflect.DeepEqual(got[i], want[i]) {
				t.Errorf("InitialRequest.TimeRange(), index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})

	t.Run("Weekend", func(t *testing.T) {
		want := []hsp.MetricsRequest{
			{"BDG", "GLC", "1200", "1800", "2022-08-05", "2022-08-07", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2022-08-05", "2022-08-07", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2022-08-05", "2022-08-07", "WEEKDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2100", "2022-08-05", "2022-08-07", "SATURDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2100", "2022-08-05", "2022-08-07", "SUNDAY", hsp.MetricsRequestTolerance},
			{"BDG", "GLC", "1800", "2100", "2022-08-05", "2022-08-07", "WEEKDAY", hsp.MetricsRequestTolerance},
		}
		ir := InitialRequest{"BDG", "GLC", "1200", "2100", "2022-08-05", "2022-08-07", []int{1, 5, 10}}
		got, err := ir.Split(ctx, irDateRangeMax, irTimeRangeMax)
		if err != nil {
			t.Fatalf("failed to splitting InitialRequest: %v", err)
		}

		for i := 0; i < len(want); i++ {
			if !reflect.DeepEqual(got[i], want[i]) {
				t.Errorf("InitialRequest.TimeRange(), index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})
}

func Test_sliceTimeToString(t *testing.T) {

	t.Run("DateRange", func(t *testing.T) {
		timeSlice, err := ir.DateRange(irDateRangeMax)
		if err != nil {
			t.Fatalf("failed to create TimeRange slice: %v", err)
		}
		want := []string{
			"2021-06-01",
			"2021-06-15",
			"2021-06-29",
			"2021-07-13",
			"2021-07-27",
			"2021-08-10",
			"2021-08-24",
			"2021-08-31",
		}
		got := sliceTimeToString(timeSlice, timeRefDateRange)

		for i := 0; i < len(want); i++ {
			if got[i] != want[i] {
				t.Errorf("Index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})

	t.Run("TimeRange", func(t *testing.T) {
		timeSlice, err := ir.TimeRange(irTimeRangeMax)
		if err != nil {
			t.Fatalf("failed to create TimeRange slice: %v", err)
		}
		want := []string{
			"0600",
			"1200",
			"1800",
			"2300",
		}
		got := sliceTimeToString(timeSlice, timeRefTimeRange)

		for i := 0; i < len(want); i++ {
			if got[i] != want[i] {
				t.Errorf("Index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})
}

func Test_GetDayGroup(t *testing.T) {

	t.Run("Sunday", func(t *testing.T) {
		dateTime := time.Date(2021, time.August, 1, 0, 0, 0, 0, time.UTC)
		want := Sunday
		got := getDayGroup(dateTime)

		if got != want {
			t.Fatalf("got %v, want %v", got, want)
		}
	})
}

func Test_getDayTypes(t *testing.T) {

	t.Run("Big", func(t *testing.T) {
		timeSlice, err := ir.DateRange(irDateRangeMax)
		if err != nil {
			t.Fatalf("failed to create TimeRange slice: %v", err)
		}
		want := []string{"SATURDAY", "SUNDAY", "WEEKDAY"}

		got, err := getDayTypes(timeSlice[0], timeSlice[1])
		if err != nil {
			t.Fatalf("failed to evaluate day types: %v", err)
		}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got = %v, want %v", got, want)
		}
	})

	t.Run("Small", func(t *testing.T) {
		small := InitialRequest{"BDG", "GLC", "1200", "2100", "2022-08-05", "2022-08-07", []int{1, 5, 10}}
		timeSlice, err := small.DateRange(irDateRangeMax)
		if err != nil {
			t.Fatalf("failed to create TimeRange slice: %v", err)
		}
		want := []string{"SATURDAY", "SUNDAY", "WEEKDAY"}

		got, err := getDayTypes(timeSlice[0], timeSlice[1])
		if err != nil {
			t.Fatalf("failed to evaluate day types: %v", err)
		}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got = %v, want %v", got, want)
		}
	})
}
