// RabbitMQ resources
// Based on example of automatic reconnect here - https://raw.githubusercontent.com/rabbitmq/amqp091-go/feb4ba5dd2d04ea8bdb625460478eff9c52eaa03/example_client_test.go

package rabbitmq

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
	"go.opentelemetry.io/otel"
	"laterail.com/internal/hsp"
	"laterail.com/internal/initial"
	"laterail.com/internal/observability"
)

// RabbitMQ client resources
type Client struct {
	Log             zerolog.Logger
	connection      *amqp.Connection
	Channel         *amqp.Channel
	exchange        Exchange
	queue           Queue
	deliverMode     uint8
	RoutingKey      string
	SubQoSPrefetch  int
	Done            chan bool
	notifyConnClose chan *amqp.Error
	notifyChanClose chan *amqp.Error
	notifyConfirm   chan amqp.Confirmation
	IsReady         bool
}

type Exchange struct {
	Name    string
	Kind    string // Direct or Topic
	Durable bool   // Will survive server restarts and remain declared when there are no remaining bindings
}

type Queue struct {
	Name       string
	Durable    bool // Value must match relevant Exchange.durable
	AutoDelete bool
	Exclusive  bool // Accessible by one connection
}

// Messages in the queue service
type ServiceMetric struct {
	RequestId          string                 `json:"request_id"`
	MessageId          string                 `json:"message_id"`
	Time               string                 `json:"time"`
	ParentRequestCount int                    `json:"parent_request_count"`
	Request            hsp.MetricsRequest     `json:"request"`
	InitialRequest     initial.InitialRequest `json:"initial_request,omitempty"`
}

type ServiceDetails struct {
	RequestId string             `json:"request_id"`
	MessageId string             `json:"message_id"`
	Time      string             `json:"time"`
	Request   hsp.DetailsRequest `json:"request"`
	Stations  Stations           `json:"stations"`
}

type Stations struct {
	Start string `json:"start"`
	End   string `json:"end"`
}

const (
	// When reconnecting to the server after connection failure
	ReconnectDelay = 5 * time.Second

	// When setting up the channel after a channel exception
	reInitDelay = 2 * time.Second

	// When resending messages the server didn't confirm
	resendDelay = 5 * time.Second

	// When waiting for a client connection to be established
	WaitConnectDelay = 500 * time.Millisecond

	// When waiting for a client connection to be established
	WaitConnectTimeout = 3 * ReconnectDelay
)

var (
	errNotConnected  = errors.New("not connected to a server")
	errAlreadyClosed = errors.New("already closed: not connected to the server")
	errShutdown      = errors.New("client is shutting down")
	errTimeout       = errors.New("timed out waiting for connection")
)

// Creates a RabbitMQ client, and automatically attempts to connect to the
// server
func New(addr string, routingKey string, deliveryMode uint8, QoSPrefetch int, queue Queue, exchange Exchange, l zerolog.Logger) *Client {
	client := Client{
		Log:            l,
		queue:          queue,
		exchange:       exchange,
		deliverMode:    deliveryMode,
		RoutingKey:     routingKey,
		SubQoSPrefetch: QoSPrefetch,
		Done:           make(chan bool),
	}
	go client.handleReconnect(addr)
	return &client
}

// Continuously attempts to connect until it succeeds or the Done channel is
// closed. If the connection fails, it waits for ReconnectDelay before trying
// again. If the connection is successful, it calls the handleReInit function
// passing the established connection
func (client *Client) handleReconnect(addr string) {
	for {
		client.IsReady = false
		client.Log.Debug().
			Str("exchange", client.exchange.Name).
			Msg("Attempting to connect to message broker")

		conn, err := client.connect(addr)

		if err != nil {
			client.Log.Warn().
				Str("exchange", client.exchange.Name).
				Msgf("Failed to connect to message broker. Retrying in %.1f seconds", ReconnectDelay.Seconds())

			select {
			case <-client.Done:
				return
			case <-time.After(ReconnectDelay):
			}
			continue
		}

		// If connection successful, init channel, exchange and queues
		if done := client.handleReInit(conn); done {
			break
		}
	}
}

// Create a new AMQP connection
func (client *Client) connect(addr string) (*amqp.Connection, error) {
	conn, err := amqp.Dial(addr)
	if err != nil {
		return nil, err
	}

	client.changeConnection(conn)
	client.Log.Debug().
		Str("exchange", client.exchange.Name).
		Msg("New message broker connection established")
	return conn, nil
}

// handleReconnect will wait for a channel error
// and then continuously attempt to re-initialize both channels
func (client *Client) handleReInit(conn *amqp.Connection) bool {
	for {
		client.IsReady = false

		err := client.init(conn)

		if err != nil {
			client.Log.Warn().
				Err(err).
				Str("exchange", client.exchange.Name).
				Msgf("Failed to initialize channel. Retrying in %.1f seconds", reInitDelay.Seconds())

			select {
			case <-client.Done:
				return true
			case <-time.After(reInitDelay):
			}
			continue
		}

		select {
		case <-client.Done:
			return true
		case <-client.notifyConnClose:
			client.Log.Warn().
				Str("exchange", client.exchange.Name).
				Msg("Connection closed. Reconnecting...")
			return false
		case <-client.notifyChanClose:
			client.Log.Warn().
				Str("exchange", client.exchange.Name).
				Msg("Channel closed. Re-running init...")
		}
	}
}

// init will initialize channel & declare queue
func (client *Client) init(conn *amqp.Connection) error {
	ch, err := conn.Channel()
	if err != nil {
		return err
	}

	err = ch.Confirm(false)
	if err != nil {
		return err
	}

	err = ch.ExchangeDeclare(
		client.exchange.Name,
		client.exchange.Kind,
		client.exchange.Durable,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	// Only create queue for direct exchanges
	_, err = client.AddQueue(ch, "")
	if err != nil {
		return err
	}

	client.IsReady = true
	client.changeChannel(ch)
	client.Log.Info().
		Str("exchange", client.exchange.Name).
		Str("routing_key", client.RoutingKey).
		Msg("Message broker connection complete")

	return nil
}

// Add queue to existing channel
func (client *Client) AddQueue(ch *amqp.Channel, routingKey string) (amqp.Queue, error) {
	var q amqp.Queue
	var err error

	// Direct exchanges are expected to have a queue declared on startup
	if client.exchange.Kind == "direct" {
		q, err = ch.QueueDeclare(
			client.queue.Name,
			client.queue.Durable,
			client.queue.AutoDelete,
			client.queue.Exclusive,
			false, // No-wait
			nil,   // Arguments
		)
		if err != nil {
			return amqp.Queue{}, err
		}

		err = ch.QueueBind(
			q.Name,
			client.RoutingKey,
			client.exchange.Name,
			false,
			nil,
		)
		if err != nil {
			return amqp.Queue{}, err
		}
		client.Log.Debug().
			Str("exchange", client.exchange.Name).
			Str("routing_key", client.RoutingKey).
			Msg("Setup message broker queue")
	} else if client.exchange.Kind == "topic" && routingKey != "" {
		// Topic exchanges are expected to declare temporary queues on demand. E.g.
		// within a http handler
		q, err = ch.QueueDeclare(
			client.queue.Name,
			client.queue.Durable,
			client.queue.AutoDelete,
			client.queue.Exclusive,
			false, // No-wait
			nil,   // Arguments
		)
		if err != nil {
			return amqp.Queue{}, err
		}

		err = ch.QueueBind(
			q.Name,
			routingKey,
			client.exchange.Name,
			false,
			nil,
		)
		if err != nil {
			return amqp.Queue{}, err
		}
		client.Log.Debug().
			Str("exchange", client.exchange.Name).
			Str("routing_key", routingKey).
			Msg("Setup message broker queue")
	}

	return q, nil
}

// Takes a new connection to rabbitmq, and updates the close listener to reflect
// this
func (client *Client) changeConnection(connection *amqp.Connection) {
	client.connection = connection
	client.notifyConnClose = make(chan *amqp.Error, 1)
	client.connection.NotifyClose(client.notifyConnClose)
}

// Takes a new channel to rabbitmq, and updates the channel listeners to reflect
// this
func (client *Client) changeChannel(channel *amqp.Channel) {
	client.Channel = channel
	client.notifyChanClose = make(chan *amqp.Error, 1)
	client.notifyConfirm = make(chan amqp.Confirmation, 1)
	client.Channel.NotifyClose(client.notifyChanClose)
	client.Channel.NotifyPublish(client.notifyConfirm)
}

// Pushes data onto the queue, and waits for a confirm. If no confirms are
// received until within the resendTimeout, it continuously re-sends messages
// until a confirm is received. This will block until the server sends a
// confirm. Errors are only returned if the push action itself fails, see
// UnsafePush
func (client *Client) Push(message any, routingKey string, ctx context.Context) error {
	if err := client.WaitWithTimeout(ctx); err != nil {
		return err
	}

	ctx, span, traceID, spanID := observability.StartSpan(ctx)
	defer span.End()

	data, err := json.Marshal(message)
	if err != nil {
		return err
	}

	// Pass otel context to the message headers
	headers := make(AmqpHeadersCarrier)
	otel.GetTextMapPropagator().Inject(ctx, headers)

	for {
		err := client.UnsafePush(data, routingKey, headers)
		if err != nil {
			client.Log.Warn().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Msgf("Message push failed. Retrying in %.1f seconds", resendDelay.Seconds())
			select {
			case <-client.Done:
				return errShutdown
			case <-time.After(resendDelay):
			}
			continue
		}
		select {
		case confirm := <-client.notifyConfirm:
			if confirm.Ack {
				client.Log.Debug().
					Str("trace_id", traceID).
					Str("span_id", spanID).
					Msg("Message push confirmed")
				return nil
			}
		case <-time.After(resendDelay):
		}
		client.Log.Warn().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msgf("Message push timed out before confirmation. Retrying in %.1f seconds", resendDelay.Seconds())
	}
}

// Implement otel/propagation.TextMapCarrier interface
// https://pkg.go.dev/go.opentelemetry.io/otel/propagation#TextMapCarrier
type AmqpHeadersCarrier map[string]interface{}

func (a AmqpHeadersCarrier) Get(key string) string {
	v, ok := a[key]
	if !ok {
		return ""
	}
	return v.(string)
}

func (a AmqpHeadersCarrier) Set(key string, value string) {
	a[key] = value
}

func (a AmqpHeadersCarrier) Keys() []string {
	i := 0
	r := make([]string, len(a))

	for k := range a {
		r[i] = k
		i++
	}

	return r
}

// ExtractAMQPHeaders extracts the tracing from the header and puts it into the context
func ExtractAMQPHeaders(ctx context.Context, headers map[string]interface{}) context.Context {
	return otel.GetTextMapPropagator().Extract(ctx, AmqpHeadersCarrier(headers))
}

// Push to the queue without checking for confirmation. It returns an error if
// it fails to connect. No guarantees are provided for whether the server will
// receive the message
func (client *Client) UnsafePush(data []byte, routingKey string, headers map[string]interface{}) error {
	if !client.IsReady {
		return errNotConnected
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	return client.Channel.PublishWithContext(
		ctx,
		client.exchange.Name,
		routingKey,
		false, // Mandatory
		false, // Immediate
		amqp.Publishing{
			Headers:      headers,
			DeliveryMode: client.deliverMode,
			Timestamp:    time.Now().UTC(),
			ContentType:  "text/plain",
			Body:         data,
		},
	)
}

// Continuously put queue items on the channel. It is required to call
// delivery.Ack when it has been successfully processed, or delivery.Nack when
// it fails. Ignoring this will cause data to build up on the server
func (client *Client) Consume() (<-chan amqp.Delivery, error) {
	if !client.IsReady {
		return nil, errNotConnected
	}

	if err := client.Channel.Qos(
		client.SubQoSPrefetch, // prefetchCount
		0,                     // prefrechSize
		false,                 // global
	); err != nil {
		return nil, err
	}

	return client.Channel.Consume(
		client.queue.Name,
		"",                     // Consumer
		false,                  // Auto-Ack
		client.queue.Exclusive, // Exclusive
		false,                  // No-local
		false,                  // No-Wait
		nil,                    // Args
	)
}

// Close will cleanly shutdown the channel and connection.
func (client *Client) Close() error {
	if !client.IsReady {
		return errAlreadyClosed
	}
	close(client.Done)
	err := client.Channel.Close()
	if err != nil {
		return err
	}
	err = client.connection.Close()
	if err != nil {
		return err
	}

	client.IsReady = false
	return nil
}

// Waits for the client to be ready, the context to be cancelled. Will never timeout
func (client *Client) Wait(ctx context.Context) error {
	ctx, span, traceID, spanID := observability.StartSpan(ctx)
	defer span.End()

	for {
		if !client.IsReady {
			select {
			case <-ctx.Done():
				return errShutdown
			case <-time.After(WaitConnectDelay):
				client.Log.Debug().
					Str("trace_id", traceID).
					Str("span_id", spanID).
					Str("routing_key", client.RoutingKey).
					Msg("Waiting for message broker client to be ready")
			}
			continue
		} else {
			// Client is ready
			break
		}
	}
	return nil
}

// Waits for the client to be ready, times out after var WaitConnectTimeout.
func (client *Client) WaitWithTimeout(ctx context.Context) error {
	timeoutCtx, cancel := context.WithTimeout(ctx, WaitConnectTimeout)
	defer cancel()

	err := client.Wait(timeoutCtx)
	if err != nil {
		return errTimeout
	}
	return nil
}

// Close RabbitMQ connections
func Close(c ...*Client) error {
	for _, client := range c {
		err := client.Close()
		if err == amqp.ErrClosed {
			client.Log.Debug().Msg("RabbitMQ connection already closed")
		} else if err != nil {
			return err
		}
	}
	return nil
}

// Unmarshal any type. Used for both ServiceMetric and ServiceDetails messages
func Unmarshal[T any](source []byte) (T, error) {
	var message T

	if err := json.Unmarshal(source, &message); err != nil {
		return message, err
	}

	return message, nil
}
