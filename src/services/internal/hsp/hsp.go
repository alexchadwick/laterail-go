package hsp

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http"
	"sort"
	"strconv"
	"sync"
	"time"
	_ "time/tzdata" // Embeds time zone db. Required for running in containers

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel/attribute"
	"laterail.com/internal/db"
	"laterail.com/internal/observability"
)

const (
	MetricsURI      = "https://hsp-prod.rockshore.net/api/v1/serviceMetrics"
	DetailsURI      = "https://hsp-prod.rockshore.net/api/v1/serviceDetails"
	timeRefDatetime = "200601021504"
)

type HspConfig struct {
	Username string
	Password string
}

type HspResponse struct {
	Data interface{}
}

func (u *HspResponse) Unmarshal(b []byte) error {
	metrics := &MetricsResponse{}
	err := json.Unmarshal(b, metrics)
	// no error, but we also need to make sure we unmarshaled something
	if err == nil && metrics.Header.FromLocation != "" {
		u.Data = metrics
		return nil
	}
	// abort if we have an error other than the wrong type
	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return fmt.Errorf("Error unmarshalling metrics json")
	}

	details := &DetailsResponse{}
	err = json.Unmarshal(b, details)
	if err != nil {
		return fmt.Errorf("Error unmarshalling details json")
	}
	u.Data = details

	return nil
}

type BadStatusError struct {
	Status int
}

func (b BadStatusError) Error() string {
	return fmt.Sprintf("GetHspResponse(): the external HSP API returned the HTTP response status code %d", b.Status)
}

// Start of response body returned by HSP ServiceMetrics request
type MetricsResponse struct {
	Header   Header     `json:"header"`
	Services []Services `json:"Services"`
}

type Header struct {
	FromLocation string `json:"from_location"`
	ToLocation   string `json:"to_location"`
}

type Services struct {
	ServiceAttributesMetrics ServiceAttributesMetrics `json:"serviceAttributesMetrics"`
	Metrics                  []Metrics                `json:"Metrics"`
}

type Metrics struct {
	ToleranceValue   string `json:"tolerance_value"`
	NumNotTolerance  string `json:"num_not_tolerance"`
	NumTolerance     string `json:"num_tolerance"`
	PercentTolerance string `json:"percent_tolerance"`
	GlobalTolerance  bool   `json:"global_tolerance"`
}

// Parse MetricsResponse into struct db.Station
func (mresp MetricsResponse) Station() []db.AddStationParams {
	var stations []db.AddStationParams
	stationMap := map[string]bool{}

	// Get all stations in response. Only add to []db.Station once
	mapKeys := []string{mresp.Header.FromLocation, mresp.Header.ToLocation}
	for _, i := range mresp.Services {
		mapKeys = append(mapKeys, i.ServiceAttributesMetrics.OriginLocation)
		mapKeys = append(mapKeys, i.ServiceAttributesMetrics.DestinationLocation)
	}
	for _, v := range mapKeys {
		_, ok := stationMap[v]
		if !ok {
			stationMap[v] = true
			stations = append(stations, db.AddStationParams{
				StationID: v,
				StationName: sql.NullString{
					String: "",
					Valid:  false},
			})
		}
	}

	return stations
}

// Parse MetricsResponse into structs db.AddServiceParams and db.AddRunParams
func (mresp MetricsResponse) Service() ([]db.AddServiceParams, []db.AddRunParams, error) {
	services := []db.AddServiceParams{}
	runs := []db.AddRunParams{}
	serviceMap := map[string]bool{}

	for i := 0; i < len(mresp.Services); i++ {
		rids := mresp.Services[i].ServiceAttributesMetrics.Rids
		for j := 0; j < len(rids); j++ {
			// ServiceID is the last 8 chars of Rid
			rid := mresp.Services[i].ServiceAttributesMetrics.Rids[j]
			ridInt, err := strconv.Atoi(rid)
			if err != nil {
				return nil, nil, fmt.Errorf("failed strconv rid: %w", err)
			}
			serviceId := mresp.Services[i].ServiceAttributesMetrics.Rids[j][8:]
			serviceIdInt, err := strconv.Atoi(serviceId)
			if err != nil {
				return nil, nil, fmt.Errorf("failed strconv serviceId: %w", err)
			}
			// Only store a ServiceID once
			_, ok := serviceMap[serviceId]
			if !ok {
				serviceMap[serviceId] = true
				services = append(services, db.AddServiceParams{
					ServiceID:          int32(serviceIdInt),
					StationOrigin:      mresp.Services[i].ServiceAttributesMetrics.OriginLocation,
					StationDestination: mresp.Services[i].ServiceAttributesMetrics.DestinationLocation,
				})
				runs = append(runs, db.AddRunParams{
					Rid:       int64(ridInt),
					ServiceID: int32(serviceIdInt),
				})
			}
		}
	}
	return services, runs, nil
}

// Parse metrics response into db structs
func (mresp MetricsResponse) Parse(ctx context.Context) ([]db.AddStationParams, []db.AddServiceParams, []db.AddRunParams, error) {
	_, span, _, _ := observability.StartSpan(ctx)
	defer span.End()

	stations := mresp.Station()
	services, runs, err := mresp.Service()
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("MetricsResponse.Service: %w", err))
		return nil, nil, nil, err
	}

	return stations, services, runs, nil
}

// End of response body returned by HSP ServiceMetrics request

// Start of response body returnd by HSP ServiceDetails request
type ServiceAttributesMetrics struct {
	OriginLocation      string   `json:"origin_location"`
	DestinationLocation string   `json:"destination_location"`
	GbttPtd             string   `json:"gbtt_ptd"`
	GbttPta             string   `json:"gbtt_pta"`
	TocCode             string   `json:"toc_code"`
	MatchedServices     string   `json:"matched_services"`
	Rids                []string `json:"rids"`
}

type DetailsResponse struct {
	ServiceAttributesDetails ServiceAttributesDetails `json:"serviceAttributesDetails"`
}

type ServiceAttributesDetails struct {
	DateOfService string      `json:"date_of_service"`
	TocCode       string      `json:"toc_code"`
	Rid           string      `json:"rid"`
	Locations     []Locations `json:"locations"`
}

type Locations struct {
	Location       string `json:"location"`
	GbttPtd        string `json:"gbtt_ptd"`
	GbttPta        string `json:"gbtt_pta"`
	ActualTd       string `json:"actual_td"`
	ActualTa       string `json:"actual_ta"`
	LateCancReason string `json:"late_canc_reason"`
}

// Convert detailsResponse into list of db.AddDetailParams, which represent railway stops of a service
func (d DetailsResponse) Location() ([]db.AddDetailParams, error) {
	details := []db.AddDetailParams{}

	ridString := d.ServiceAttributesDetails.Rid
	ridInt, err := strconv.Atoi(ridString)
	if err != nil {
		return nil, fmt.Errorf("failed strconv rid: %w", err)
	}
	rid := int64(ridInt)

	serviceInt, err := strconv.Atoi(d.ServiceAttributesDetails.Rid[8:])
	if err != nil {
		return nil, fmt.Errorf("failed strconv rid: %w", err)
	}
	serviceID := int32(serviceInt)
	for _, i := range d.ServiceAttributesDetails.Locations {
		ScheduledDeparture, err := NullTime(ridString[:8], i.GbttPtd)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullTime: %w", err)
		}
		ScheduledArrival, err := NullTime(ridString[:8], i.GbttPta)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullTime: %w", err)
		}
		ActualDeparture, err := NullTime(ridString[:8], i.ActualTd)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullTime: %w", err)
		}
		ActualArrival, err := NullTime(ridString[:8], i.ActualTa)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullTime: %w", err)
		}
		LateCancelReason, err := NullInt(i.LateCancReason)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullInt32: %w", err)
		}

		// Calculate delays
		// First station in a service should not have arrival times, only departure. Inverse for last
		// But the HSP API sometimes returns null or invalid responses
		var delayDepartFloat64, delayArrivalFloat64 float64
		var delayDepartMins, delayArrivalMins sql.NullInt32

		if ScheduledDeparture.Valid && ActualDeparture.Valid {
			delayDepartFloat64 = ActualDeparture.Time.Sub(ScheduledDeparture.Time).Minutes()
			delayDepartMins = sql.NullInt32{
				Int32: int32(math.Round(delayDepartFloat64)),
				Valid: true,
			}
		} else {
			delayDepartMins = sql.NullInt32{}
		}
		if ScheduledArrival.Valid && ActualArrival.Valid {
			delayArrivalFloat64 = ActualArrival.Time.Sub(ScheduledArrival.Time).Minutes()
			delayArrivalMins = sql.NullInt32{
				Int32: int32(math.Round(delayArrivalFloat64)),
				Valid: true,
			}
		} else {
			delayArrivalMins = sql.NullInt32{}
		}

		details = append(details, db.AddDetailParams{
			Rid:              rid,
			ServiceID:        serviceID,
			Station:          i.Location,
			SchedDepart:      ScheduledDeparture,
			SchedArrival:     ScheduledArrival,
			ActualDepart:     ActualDeparture,
			ActualArrival:    ActualArrival,
			DelayDepart:      delayDepartMins,
			DelayArrival:     delayArrivalMins,
			LateCancelReason: LateCancelReason,
		})
	}

	return details, nil
}

// Convert detailsResponse into list of db.AddStationParams
func (d DetailsResponse) Station() []db.AddStationParams {
	var stations []db.AddStationParams
	stationMap := map[string]bool{}

	// Get all stations in response. Only add to []db.Station once
	mapKeys := []string{}
	for _, i := range d.ServiceAttributesDetails.Locations {
		mapKeys = append(mapKeys, i.Location)
	}
	for _, v := range mapKeys {
		_, ok := stationMap[v]
		if !ok {
			stationMap[v] = true
			stations = append(stations, db.AddStationParams{
				StationID: v,
				StationName: sql.NullString{
					String: "",
					Valid:  false},
			})
		}
	}

	return stations
}

// Parse details response into db structs
func (d DetailsResponse) Parse() ([]db.AddDetailParams, []db.AddStationParams, error) {
	stations := d.Station()
	details, err := d.Location()
	if err != nil {
		return nil, nil, fmt.Errorf("MetricsResponse.Service: %w", err)
	}

	return details, stations, nil
}

// End of response body returnd by HSP ServiceDetails request

// Convert strings of UK time + rid to sql.NullTime of UTC
func NullTime(ridString, timeString string) (sql.NullTime, error) {
	var nulltime sql.NullTime
	var valid = true

	// Parse datetime as UK local time
	location, err := time.LoadLocation("Europe/London")
	if err != nil {
		return nulltime, fmt.Errorf("NullTime: failed to load local time: %w", err)
	}

	timeParsedLocal, err := time.ParseInLocation(timeRefDatetime, ridString+timeString, location)
	if err != nil {
		// Expected failure if there is no time string value for location in details response
		if timeString != "" {
			return nulltime, fmt.Errorf("NullTime: failed to parse time string: %w", err)
		}
		valid = false
	}

	nulltime = sql.NullTime{
		Time:  timeParsedLocal.UTC(),
		Valid: valid,
	}

	return nulltime, nil
}

// Convert strings of int to sql.NullInt32
func NullInt(intString string) (sql.NullInt32, error) {
	var nullInt sql.NullInt32
	i, err := strconv.Atoi(intString)
	if err != nil {
		if intString != "" {
			return nullInt, fmt.Errorf("failed to convert string to int")
		}
	} else {
		nullInt = sql.NullInt32{
			Int32: int32(i),
			Valid: true,
		}
	}

	return nullInt, nil
}

// Make request to external HSP API. Returns a sorted array of RIDs
func SubmitHSPRequest(request GenericRequest[Request], hspConfig HspConfig, hspGetter HSPGetter, ctx context.Context, logger *zerolog.Logger, metrics observability.MetricsCollection) (sortedRidSlice []int64, detailsResponses []db.AddDetailParams, err error) {
	ctx, span, traceID, spanID := observability.StartSpan(ctx)
	defer span.End()

	// Check type of input. Must be Metric or Details
	requestType := request.Request.GetType()
	if (requestType != "hsp.MetricsRequest") && (requestType != "hsp.DetailsRequest") {
		err := observability.NewErrorWithTrace(span, fmt.Errorf("unexpected request type: %s", requestType))
		return nil, detailsResponses, err
	}

	// Http client with exp retry, using zerolog Logger
	client, err := newRetryClient(logger, metrics)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to create http client: %w", err))
		return nil, detailsResponses, err
	}

	// Limit to 1 concurrent request using semaphore channel
	sem := make(chan struct{}, 1)

	// Store parsed responses in an unbuffered channel and close using wait group
	parseChan := make(chan struct {
		stationChan []db.AddStationParams
		serviceChan []db.AddServiceParams
		runChan     []db.AddRunParams
		detailsChan []db.AddDetailParams
	})
	wg := sync.WaitGroup{}

	// Block if buffered channel full
	sem <- struct{}{}

	wg.Add(1)
	go func(request GenericRequest[Request], requestType string, ctx context.Context) error {

		defer wg.Done()

		// Where hspGetter is hsp.GetHSPResponse or a mock used in testing
		untypedResponse, err := hspGetter(client, hspConfig, request, ctx)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("SubmitHSPRequest: %w", err))
			return err
		}
		// Remove semaphore block, request completed
		<-sem

		// Type specific steps. Metrics vs Details
		var details []db.AddDetailParams
		var stations []db.AddStationParams
		var services []db.AddServiceParams
		var runs []db.AddRunParams

		if requestType == "hsp.MetricsRequest" {
			response := untypedResponse.(*MetricsResponse)
			stations, services, runs, err = response.Parse(ctx)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("SubmitHSPRequest: %w", err))
				return err
			}
		} else if requestType == "hsp.DetailsRequest" {
			response := untypedResponse.(*DetailsResponse)
			details, stations, err = response.Parse()
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("SubmitHSPRequest: %w", err))
				return err
			}
		}

		// Add to parseChan
		batch := struct {
			stationChan []db.AddStationParams
			serviceChan []db.AddServiceParams
			runChan     []db.AddRunParams
			detailsChan []db.AddDetailParams
		}{stations, services, runs, details}
		parseChan <- batch

		return nil
	}(request, requestType, ctx)

	// Close channels once Wait Group has completed. Ends for loop below
	go func() {
		wg.Wait()
		close(parseChan)
	}()

	// Empty channels into db and sorted rid slices
	ridSlice := []int64{}
	for i := range parseChan {
		for _, stationChanItem := range i.stationChan {
			_, err := db.New(db.Conn).AddStation(ctx, stationChanItem)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("stations insert failed: %w", err))
				return nil, detailsResponses, err
			}
		}
		for _, serviceChanItem := range i.serviceChan {
			err := db.New(db.Conn).AddService(ctx, serviceChanItem)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("service insert failed: %w", err))
				return nil, detailsResponses, err
			}
		}
		for _, runChanItem := range i.runChan {
			ridSlice = append(ridSlice, runChanItem.Rid)
			err := db.New(db.Conn).AddRun(ctx, runChanItem)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("run insert failed: %w", err))
				return nil, detailsResponses, err
			}
		}
		for _, detailChanItem := range i.detailsChan {
			ridSlice = append(ridSlice, detailChanItem.Rid)
			detailsResponses = append(detailsResponses, detailChanItem)
			err := db.New(db.Conn).AddDetail(ctx, detailChanItem)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("details insert failed: %w", err))
				return nil, detailsResponses, err
			}
		}
	}
	sort.Slice(ridSlice, func(i, j int) bool { return ridSlice[i] < ridSlice[j] })
	sortedRidSlice = ridSlice

	// No services are returned
	if (len(sortedRidSlice) == 0) && (requestType == "hsp.MetricsRequest") {
		log.Debug().
			Str("request_type", requestType).
			Interface("post_body", request).
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msg("No matching services for query")
	}
	return sortedRidSlice, detailsResponses, nil
}

// Used to pass hsp.GetHspResponse as parameter, allow replacement in testing, instead of making external HSP API requests
type HSPGetter func(client *http.Client, hspConfig HspConfig, inputBody GenericRequest[Request], ctx context.Context) (interface{}, error)

// Get response from HSP API endpoints serviceMetrics and serviceDetails, from input of MetricsRequest or DetailsRequest post body
func GetHspResponse(client *http.Client, hspConfig HspConfig, inputBody GenericRequest[Request], ctx context.Context) (interface{}, error) {
	_, span, traceID, spanID := observability.StartSpan(ctx)
	defer span.End()

	HspResp := HspResponse{}

	postBody, err := json.Marshal(inputBody.Request)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("Error marshalling post json: %q", err))
		return nil, err
	}
	span.SetAttributes(attribute.String("requestBody", string(postBody)))

	// Set HSP API endpoint
	var reqURI string
	requestType := inputBody.Request.GetType()
	if requestType == "hsp.MetricsRequest" {
		reqURI = MetricsURI
	} else if requestType == "hsp.DetailsRequest" {
		reqURI = DetailsURI
	} else {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("unexpected request type: %s", requestType))
		return nil, err
	}
	span.SetAttributes(attribute.String("requestType", requestType))

	// Build and make request
	req, err := http.NewRequest("POST", reqURI, bytes.NewBuffer(postBody))
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("Error buidling request: %q", err))
		return nil, err
	}
	req.Header.Add("Content-Type", `application/json`)
	req.SetBasicAuth(hspConfig.Username, hspConfig.Password)

	resp, err := client.Do(req)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("Error attempting request: %q", err))
		return nil, err
	}
	defer resp.Body.Close() // Close body only if response non-nil
	if resp.StatusCode != http.StatusOK {
		err = observability.NewErrorWithTrace(span, BadStatusError{resp.StatusCode})
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("Error reading response: %q", err))
		return nil, err
	}

	// Unmarshal either metric or detail response using interface
	HspResp.Unmarshal(body)

	// Log number services are returned
	if requestType == "hsp.MetricsRequest" {
		responseData := HspResp.Data.(*MetricsResponse)
		log.Debug().
			Str("request_type", requestType).
			Interface("post_body", inputBody).
			Int("returned_services", len(responseData.Services)).
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msg("New Metrics request complete")
	} else if requestType == "hsp.DetailsRequest" {
		responseData := HspResp.Data.(*DetailsResponse)
		log.Debug().
			Str("request_type", requestType).
			Interface("post_body", inputBody).
			Str("returned_services", responseData.ServiceAttributesDetails.DateOfService).
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msg("New Details request complete")
	}

	return HspResp.Data, nil
}
