package hsp

import (
	"context"
	"fmt"
	"sort"

	"github.com/rs/zerolog/log"
	"laterail.com/internal/db"
	"laterail.com/internal/observability"
)

// Used to check for new RIDs which have not yet been ran through PostDetails()
func GroupRIDs(ridArray []int64, ctx context.Context) (ridsNew []int64, ridsExisting []int64, err error) {
	_, span, traceID, spanID := observability.StartSpan(ctx)
	defer span.End()

	// Pull all known details RIDs from datastore and binary search for given rids
	ridsKnown, err := db.New(db.Conn).ListRangeDetailsUniqueRIDs(
		context.Background(),
		db.ListRangeDetailsUniqueRIDsParams{
			MinRid: ridArray[0],
			MaxRid: ridArray[len(ridArray)-1],
		},
	)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to retrieve rids from db: %w", err))
		return ridsNew, ridsExisting, err
	}
	for _, rid := range ridArray {
		i := sort.Search(len(ridsKnown), func(i int) bool { return ridsKnown[i] >= rid })
		if i < len(ridsKnown) && ridsKnown[i] == rid {
			ridsExisting = append(ridsExisting, rid)
			log.Debug().
				Int64("existing_rid", rid).
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Msg("Found existing rid")
		} else {
			ridsNew = append(ridsNew, rid)
			log.Debug().
				Int64("new_rid", rid).
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Msg("Found new rid to search")
		}
	}

	if len(ridsNew) == 0 {
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msg("No new rid in request to search. Request processing complete.")
	}

	return ridsNew, ridsExisting, err
}
