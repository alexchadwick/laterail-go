package hsp

import (
	"context"
	"net/http"
	"time"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/rs/zerolog"
	"laterail.com/internal/observability"
)

type MetricsRequest struct {
	FromLoc  string `json:"from_loc"`
	ToLoc    string `json:"to_loc"`
	FromTime string `json:"from_time"`
	ToTime   string `json:"to_time"`
	FromDate string `json:"from_date"`
	ToDate   string `json:"to_date"`
	Days     string `json:"days"`
}

func (mr MetricsRequest) GetType() string {
	return "hsp.MetricsRequest"
}

type DetailsRequest struct {
	Rid string `json:"rid"`
}

func (mr DetailsRequest) GetType() string {
	return "hsp.DetailsRequest"
}

type Request interface {
	GetType() string
}

type GenericRequest[R Request] struct {
	Request R `json:"request"`
}

// A custom logger that implements the `retryablehttp.Logger` interface
type zerologAdapter struct {
	logger *zerolog.Logger
}

func (z zerologAdapter) Printf(format string, v ...interface{}) {
	z.logger.Printf(format, v...)
}

// Client with retry and exponentional backoff, using zerolog logger without levels
func newRetryClient(logger *zerolog.Logger, metrics observability.MetricsCollection) (*http.Client, error) {
	// New transport that wraps the default transport with our logging middleware
	transport := http.DefaultTransport
	transport = &loggingTransport{Transport: transport, logger: logger, metrics: metrics}

	// Retryable client that uses our custom transport
	retryClient := retryablehttp.NewClient()
	retryClient.HTTPClient = &http.Client{Transport: transport, Timeout: 2 * time.Minute}
	retryClient.RetryMax = 10
	retryClient.RetryWaitMin = 10 * time.Second
	retryClient.RetryWaitMax = 240 * time.Second
	retryClient.Logger = zerologAdapter{logger}

	retryClient.CheckRetry = func(ctx context.Context, resp *http.Response, err error) (bool, error) {
		ok, err := retryablehttp.DefaultRetryPolicy(ctx, resp, err)
		if ok || err != nil {
			metrics.CounterMap["laterail_hsp_requests_retry_total"].WithLabelValues(resp.Request.URL.Redacted()).Inc()
		}
		// Return true to retry, false to stop
		return ok, err
	}

	return retryClient.StandardClient(), nil
}

// A custom transport that logs requests and responses with `zerolog`
type loggingTransport struct {
	Transport http.RoundTripper
	logger    *zerolog.Logger
	metrics   observability.MetricsCollection
}

func (t *loggingTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	url := req.URL.Redacted()
	t.metrics.CounterMap["laterail_hsp_requests_total"].WithLabelValues(url).Inc()

	start := time.Now()
	resp, err := t.Transport.RoundTrip(req)
	elapsed := time.Since(start)
	elapsedSec := elapsed.Seconds()
	t.metrics.SummaryMap["laterail_hsp_request_duration_seconds"].WithLabelValues(url).Observe(elapsedSec)
	t.metrics.HistogramMap["laterail_hsp_histogram_request_duration_seconds"].WithLabelValues(url).Observe(elapsedSec)

	if err != nil {
		t.logger.Error().
			Err(err).
			Str("method", req.Method).
			Str("url", req.URL.Redacted()).
			Dur("elapsed", elapsed).
			Msg("HTTP request failed")
	} else {
		t.metrics.SummaryMap["laterail_hsp_response_size_bytes"].WithLabelValues(url).Observe(float64(resp.ContentLength))
		t.logger.Debug().
			Str("method", req.Method).
			Str("url", req.URL.Redacted()).
			Int("status", resp.StatusCode).
			Dur("elapsed", elapsed).
			Msg("HTTP request completed")
	}

	return resp, err
}
