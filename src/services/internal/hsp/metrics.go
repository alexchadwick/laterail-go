package hsp

import (
	"github.com/prometheus/client_golang/prometheus"
	"laterail.com/internal/observability"
)

// Default metrics for requests to external HSP API
var DefaultMetrics = []observability.Metric{
	{
		Name:  "laterail_hsp_requests_total",
		Help:  "Total number of requests made to external HSP API",
		Type:  "counter",
		Label: "URL",
	},
	{
		Name:  "laterail_hsp_requests_retry_total",
		Help:  "Total number of request retries to external HSP API",
		Type:  "counter",
		Label: "URL",
	},
	{
		Name:       "laterail_hsp_request_duration_seconds",
		Help:       "Latency for requests made to external HSP API",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		Type:       "summary",
		Label:      "URL",
	},
	{
		Name:    "laterail_hsp_histogram_request_duration_seconds",
		Help:    "Latency for requests made to external HSP API",
		Buckets: prometheus.ExponentialBucketsRange(0.001, 60, 10),
		Type:    "histogram",
		Label:   "URL",
	},
	{
		Name:       "laterail_hsp_response_size_bytes",
		Help:       "Size of response for requests made to external HSP API",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		Type:       "summary",
		Label:      "URL",
	},
}
