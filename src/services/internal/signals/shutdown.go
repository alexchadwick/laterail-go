package signals

import (
	"context"
	"net/http"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog/log"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	"laterail.com/internal/rabbitmq"
)

// Shutdown gracefully shuts down the application. Stopping the http server,
// rabbitMQ connections and flushing the tracer provider.
func Shutdown(
	stop context.CancelFunc,
	timeout time.Duration,
	server *http.Server,
	tp *sdktrace.TracerProvider,
	rc ...*rabbitmq.Client,
) {
	// Allows further signals to force shutdown
	stop()
	log.Debug().
		Msg("shutting down gracefully, press Ctrl+C again to force")

	// Graceful shutdown timeout
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	// Shutdown server
	if server != nil {
		if err := server.Shutdown(ctx); err != nil {
			log.Info().Msg("Shutting down server")
		}
	}

	// Close RabbitMQ connections
	for _, client := range rc {
		err := client.Close()
		if err == amqp.ErrClosed {
			log.Debug().Msg("RabbitMQ connection already closed")
		} else if err != nil {
			log.Error().Err(err).Msg("failed to close RabbitMQ connection")
		}
	}

	// Flush tracer provider and shutdown
	if tp != nil {
		if err := tp.ForceFlush(ctx); err != nil {
			log.Error().Err(err).Msg("Error flushing tracer spans before shutdown")
		}
		if err := tp.Shutdown(ctx); err != nil {
			log.Error().Err(err).Msg("Error shutting down tracer provider")
		}
	}
}
