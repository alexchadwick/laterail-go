package validate

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
)

type ErrValidation struct {
	Errors []string `json:"errors"`
}

// CSR: Uppercase, 3 letter code for a rail station
func CSR(fl validator.FieldLevel) bool {
	CSRRegexString := "^[A-Z]{3}$"
	CSRRegex := regexp.MustCompile(CSRRegexString)

	return CSRRegex.MatchString(fl.Field().String())
}

// Time format: 4 digits
func Time(fl validator.FieldLevel) bool {
	TimeRegexString := "^\\d{4}$"
	TimeRegex := regexp.MustCompile(TimeRegexString)

	return TimeRegex.MatchString(fl.Field().String())
}

// Date format: YYYY-mm-dd
func Date(fl validator.FieldLevel) bool {
	DateRegexString := "^\\d{4}-\\d{2}-\\d{2}$"
	DateRegex := regexp.MustCompile(DateRegexString)

	return DateRegex.MatchString(fl.Field().String())
}

// Initialise new validator
func New() *validator.Validate {
	validate := validator.New()

	validate.RegisterValidation("csr", CSR)
	validate.RegisterValidation("time", Time)
	validate.RegisterValidation("date", Date)

	// Return the struct json tag, rather than the validate tag
	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		// skip if tag key says it should be ignored
		if name == "-" {
			return ""
		}
		return name
	})

	return validate
}

// Return helpful errors to the http client
func ToErrResponse(err error) *ErrValidation {
	if fieldErrors, ok := err.(validator.ValidationErrors); ok {
		resp := ErrValidation{
			Errors: make([]string, len(fieldErrors)),
		}
		for i, err := range fieldErrors {
			switch err.Tag() {
			case "required":
				resp.Errors[i] = fmt.Sprintf("%s is a required field", err.Field())
			case "csr":
				resp.Errors[i] = fmt.Sprintf("%s must be 3 upper case alphabet characters. e.g. GLA ", err.Field())
			case "time":
				resp.Errors[i] = fmt.Sprintf("%s must be 24hr time expressed as 4 digits. e.g. 1300 ", err.Field())
			case "date":
				resp.Errors[i] = fmt.Sprintf("%s must be a date in the format YYYY-mm-dd. e.g. 2021-12-30", err.Field())
			default:
				resp.Errors[i] = fmt.Sprintf("invalid format: %s; %s", err.Field(), err.Tag())
			}
		}
		return &resp
	}
	return nil
}
