package mongodb

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Collection model which wraps the mongo.Client connection pool
type CollectionModel struct {
	DB   *mongo.Client
	Coll *mongo.Collection
}

// Used to import station data
type Stations []struct {
	Name         string `json:"name"`
	Crs          string `json:"crs"`
	Lat          string `json:"lat"`
	Lon          string `json:"lon"`
	Updated      string `json:"updated"`
	Toc          string `json:"toc"`
	Extra        string `json:"extra"`
	IsTocStation bool   `json:"is_toc_station"`
	IsStation    bool   `json:"is_station"`
}

// Return a completed mongodb collection model for a specific collection
func InitCollectionModel(collectionName string, client *mongo.Client) CollectionModel {
	model := CollectionModel{
		DB: client,
	}
	model.Coll = model.GetCollection(collectionName)

	return model
}

// Connect to Stations collection. Used to populate CollectionModel.coll
func (s CollectionModel) GetCollection(collectionName string) *mongo.Collection {
	collection := s.DB.Database("laterail").Collection(collectionName)

	return collection
}

// Import station data used by frontend from json stored in repo
func (s CollectionModel) AddFromFile(ctx context.Context) (*mongo.InsertManyResult, error) {

	// Get data from file
	docs := []interface{}{}
	var seedFile string
	if _, err := os.Stat("../../deploy/migrations/stations.json"); !os.IsNotExist(err) {
		// Import relative to /src/api
		seedFile = "../../deploy/migrations/stations.json"
	} else if _, err := os.Stat("/migrations/stations.json"); !os.IsNotExist(err) {
		seedFile = "/migrations/stations.json"
	} else {
		return nil, fmt.Errorf("import json not found: %w", err)
	}

	importData, err := os.ReadFile(seedFile)
	if err != nil {
		return nil, fmt.Errorf("failed to open import json: %w", err)
	}
	if err := json.Unmarshal(importData, &docs); err != nil {
		return nil, fmt.Errorf("failed to unmarshal import data: %w", err)
	}

	opts := options.InsertMany().SetOrdered(false)
	resultMany, err := s.Coll.InsertMany(ctx, docs, opts)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal import data: %w", err)
	}

	return resultMany, nil
}

// Find all in collection. Used by frontend station vselect components
func (s CollectionModel) ListStations(ctx context.Context) ([]bson.M, error) {

	cursor, err := s.Coll.Find(ctx, bson.D{})
	if err != nil {
		return nil, fmt.Errorf("find stations in mongo: %w", err)
	}
	defer cursor.Close(ctx)

	// Get all results from cursor, decode and then close
	var stations []bson.M
	if err := cursor.All(ctx, &stations); err != nil {
		return nil, fmt.Errorf("get all results from cursor and close: %w", err)
	}

	return stations, nil
}
