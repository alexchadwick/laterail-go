package main

import (
	"github.com/go-chi/chi/v5"
)

func (app *application) routes() chi.Router {
	r := chi.NewRouter()

	// Initial release
	r.Route("/v0", func(r chi.Router) {
		r.Get("/events/{id}", app.getEvents)
	})

	return r
}
