package main

import (
	"fmt"
	"io"
	"net/http"

	"github.com/go-chi/chi/v5"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog/log"

	"laterail.com/internal/observability"
	"laterail.com/internal/rabbitmq"
)

var (
	errMsg = "internal server error"
)

// Get progress and results of existing request IDs
func (app application) getEvents(w http.ResponseWriter, r *http.Request) {
	ctx, span, traceID, spanID := observability.StartSpan(r.Context())
	defer span.End()

	h := w.Header()
	h.Set("Content-Type", "text/event-stream")
	h.Set("Cache-Control", "no-cache")
	h.Set("Connection", "keep-alive")
	h.Set("X-Accel-Buffering", "no")

	// Get request id from URL
	requestID := chi.URLParam(r, "id")

	// Setup response flusher
	flusher, ok := w.(http.Flusher)
	if !ok {
		err := fmt.Errorf("failed to setup flusher")
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}
	io.WriteString(w, "Connected to SSE Service\n\n")
	flusher.Flush()

	// Accept RabbitMQ messages for request_id
	if err := app.rabbitmq.WaitWithTimeout(ctx); err != nil {
		observability.NewErrorLogWithTrace(err, span, "failed to wait for RabbitMQ client")
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
	q, err := app.rabbitmq.AddQueue(app.rabbitmq.Channel, requestID)
	if err != nil {
		observability.NewErrorLogWithTrace(err, span, "failed to add queue")
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
	msgs, err := app.rabbitmq.Consume()
	if err != nil {
		observability.NewErrorLogWithTrace(err, span, "could not start consuming messages")
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}

	chClosedCh := make(chan *amqp.Error, 1)
	app.rabbitmq.Channel.NotifyClose(chClosedCh)

	for {
		select {
		case <-r.Context().Done():
			log.Debug().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Str("request_id", requestID).
				Msg("client closed the connection")

			// Delete Queue when client disconnects
			_, err = app.rabbitmq.Channel.QueueDelete(
				q.Name, // name
				false,  // ifUnused
				false,  // ifEmpty
				true,   // no-wait
			)
			if err != nil {
				observability.NewErrorLogWithTrace(err, span, "failed to delete a queue on client disconnect")
			}
			return

		case <-app.ctx.Done():
			log.Info().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Str("request_id", requestID).
				Msg("service closed the connection. Stopping")

			// Write response to client, using named event
			io.WriteString(w, "data: service closed the connection. Stopping...\n\n")
			flusher.Flush()
			return

		case amqErr := <-chClosedCh:
			// This case handles the event of closed channel e.g. abnormal shutdown
			log.Warn().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Str("request_id", requestID).
				Msgf("AMQP Channel closed due to: %s", amqErr)

			msgs, err = app.rabbitmq.Consume()
			if err != nil {
				// If the AMQP channel is not ready, it will continue the loop. Next
				// iteration will enter this case because chClosedCh is closed by the
				// library
				observability.NewErrorLogWithTrace(err, span, "Error trying to consume, will try again")
				continue
			}

			// Re-set channel to receive notifications
			// The library closes this channel after abnormal shutdown
			chClosedCh = make(chan *amqp.Error, 1)
			app.rabbitmq.Channel.NotifyClose(chClosedCh)

		case message := <-msgs:
			// Get message context from headers
			msgCtx := rabbitmq.ExtractAMQPHeaders(ctx, message.Headers)
			_, span, traceID, spanID := observability.StartSpan(msgCtx)

			// Write response to client, using named event/event type `message`
			fmt.Fprintf(w, "event: %s\n\ndata: %s\n\n", "message", message.Body)
			flusher.Flush()

			// Send positive acknowledgment
			if err := message.Ack(false); err != nil {
				observability.NewErrorLogWithTrace(err, span, "Error acknowledging message")
			}

			log.Debug().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Str("request_id", requestID).
				Msg("sent SSE")

			// Cannot defer this because it will be called after the for loop
			span.End()
		}
	}
}
