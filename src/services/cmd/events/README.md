# Events service
- For each Events request, accept message broker messages on a new temporary queue with topic of the incoming requests parent request ID (created by the Producer service as part of the initial `/submit` request)
- Return contents of these messages to client via Server Sent Events (SSE)
- On client disconnect, delete the temporary queue and all messages
  - If the client reconnects, it will first get an update of the current state of the request from the Consumer service `/track` endpoint, then receive any new SSEs

## Overall Process Context
- Client submits initial POST request to Consumer service Tracker endpoint, with request_id
  - Consumer returns current state
- Client starts SSE connection to Events service to receive updates
  - Only if backend HSP request process has not completed/errored
- Client interprets responses for the end user and closes when SSE signals request_id jobs have completed

# Issues
  - `http.Server` has a long `WriteTimeout` (3 mins), which affects all endpoints
    - Required to cut down on number of client-side reconnects for SSE streaming
    - [In Go 1.20 it will be possible to set timeouts on specific handlers](https://github.com/golang/go/issues/54136)
      - Use this to protect other endpoints from long running requests

# Datastore(s)
 - None, no persistent data

# Run
```
laterail-go/src/services$ go run ./cmd/events
```

## Environmental variables
| name | default | description |
|-|-|-|
| `LATERAIL_ENVIRONMENT` | `dev` | Application deployment stage environment |
| `LATERAIL_LOG_LEVEL` | `dev` | Structured log level |
| `LATERAIL_PORT_METRICS` | `8102` | Port for the Prometheus metrics and health endpoints |
| `LATERAIL_PORT` | `8002` | Port of the exposed service |
| `LATERAIL_RABBITMQ_EXCHANGE_EVENTS` | `laterail.events` | RabbitMQ Exchange Name for Events service |
| `LATERAIL_RABBITMQ_URI` | `amqp://guest:guest@localhost:5672/` | URL for RabbitMQ service |
| `LATERAIL_TRACE_URL` | `http://localhost:14268/api/traces` | Jaeger compatible service for traces |

# Example requests
```
# Requires request_id (returned by Producer service)
# E.g. request_id = "6343d4e4-51bb-4f0b-9be2-3ee31cce9de3"
curl -N --http2 -H "Accept:text/event-stream" http://localhost:8002/v0/events/6343d4e4-51bb-4f0b-9be2-3ee31cce9de3
```
