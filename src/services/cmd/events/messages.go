package main

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"

	"laterail.com/internal/rabbitmq"
)

// Exports client objects which automatically reconnect when the connection
// fails, and blocks all pushes until the connection succeeds. It also confirms
// every outgoing message, so none are lost. It doesn't automatically ack each
// message, but leaves that to the parent process, since it is usage-dependent.
func initMessages(log zerolog.Logger, uri, exchange string) *rabbitmq.Client {
	receiveEvents := rabbitmq.New(
		uri,
		"",
		amqp.Transient,
		1,
		rabbitmq.Queue{
			Name:       "",
			Durable:    false,
			AutoDelete: true,
			Exclusive:  true,
		},
		rabbitmq.Exchange{
			Name:    exchange,
			Kind:    "topic",
			Durable: false,
		},
		log,
	)

	return receiveEvents
}
