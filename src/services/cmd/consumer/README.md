# Consumer service
Two features, sharing a MongoDB collection
- Submitter endpoint:
  - stores request progress and results in MongoDB collection, shared with Tracker endpoint
  - accepts ServiceMetrics messages from message broker and submits HSP API ServiceMetrics requests
    - waits for response and checks db for existing ServiceDetails data
    - adds any new ServiceDetails rids (not already present in collection) to message broker for additional processing
    - sends request progress info to Events service (via message broker)
  - accepts ServiceDetails messages from message broker and submits HSP ServiceDetails requests
    - waits for response and updates database of details and search status
    - sends request progress info to Events service (via message broker)
- Tracker endpoint: 
  - listens for request id which represents a pending/completed request to Sumbitter
  - returns submitted requests and their results (details items) and metadata (in-progress/completed/failed etc)
  - Used by frontend to display results

# Datastore(s)
  - Postgres/cockroachdb: All rail journey data
  - MongoDB: Processed info for frontend client (e.g. individual request results)

# Run
```
laterail-go/src/services$ go run ./cmd/consumer
```

## Environmental variables
| name | default | description |
|-|-|-|
| `LATERAIL_DATABASE_URI` | `postgres://user:secret@localhost` | URI of relational database service, incl. credentials |
| `LATERAIL_ENVIRONMENT` | `dev` | Application deployment stage environment |
| `LATERAIL_HSP_PASSWORD` | `secret` | Password for external HSP API |
| `LATERAIL_HSP_USER` | `user` | Username for external HSP API |
| `LATERAIL_LOG_LEVEL` | `dev` | Structured log level |
| `LATERAIL_MONGO_URI` | `mongodb+srv://user:secret@localhost` | URI of MongoDB service, incl. credentials |
| `LATERAIL_PORT_METRICS` | `8101` | Port for the Prometheus metrics and health endpoints |
| `LATERAIL_PORT` | `8001` | Port of the exposed service |
| `LATERAIL_RABBITMQ_EXCHANGE_EVENTS` | `laterail.events` | RabbitMQ Exchange Name for Events service |
| `LATERAIL_RABBITMQ_EXCHANGE_HSP` | `laterail.hsp` | RabbitMQ Exchange Name for HSP requests |
| `LATERAIL_RABBITMQ_URI` | `amqp://guest:guest@localhost:5672/` | URL for RabbitMQ service |
| `LATERAIL_TRACE_URL` | `http://localhost:14268/api/traces` | Jaeger compatible service for traces |

# Example requests
## Tracker
```
curl --location --request POST 'http://<CONSUMER_SERVICE>/v0/track' \
--header 'Content-Type: application/json' \
--data-raw '{
    "request_ids": [
        "178bf31a-f901-4e59-98bb-a40fc75c00e1", 
        "178bf31a-f901-4e59-98bb-a40fc75c00e2", 
        "178bf31a-f901-4e59-98bb-a40fc75c00e3"
    ]
}'
```
## Stations
```
curl --location --request GET 'http://<CONSUMER_SERVICE>/v0/stations'
```

# Notes 
## Generics in Submitter
Go Generics are used to allow a single process to handle HSP ServieMetrics and ServiceDetails requests and responses. Summarised below:

- Incoming message are typed by `main.HandleMessage`
  - Unmarshals `[]byte` to `rabbitmq.ServiceMetric` and `rabbitmq.ServiceDetails`
    - Errors on failure or incomplete results
- `hsp.SubmitHSPRequest` accepts input as `hsp.GenericRequest`  
  - `hsp.GenericRequest`: generic type with contraint `hsp.Request` (must implement the `GetType()` method), satisfied by both `hps.MetricsRequest` and `hsp.DetailsRequest`
  - `hsp.SubmitHSPRequest` checks request type using `GetType()` 
- `hsp.GetHspResponse` accepts input as `hsp.GenericRequest`
  - Creates a POST body of `[]byte` to submit to the HSP API
  - Returns an `interface{}` containing either `hsp.MetricsResponse` or `hsp.DetailsResponse` to `hsp.SubmitHSPRequest`
- In `hsp.SubmitHSPRequest` continued, cast the `interface{}` of `untypedResponse` to `hsp.MetricsResponse` or `hsp.DetailsResponse` based on type of `hsp.Request`
  - End of type shenanighans 

## MongoDB `history` collection
- Stores request history, including all request progress and final results, for frontend client to consume
  - So there's a lot of duplicated data between mongodb and psql, within in mongodb between similar requests 
  - I think this is an acceptable trade off (?)