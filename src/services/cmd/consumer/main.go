package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/peterbourgon/ff/v3"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"laterail.com/internal/api"
	"laterail.com/internal/db"
	"laterail.com/internal/hsp"
	"laterail.com/internal/mongodb"
	"laterail.com/internal/observability"
	"laterail.com/internal/rabbitmq"
	"laterail.com/internal/signals"
)

type application struct {
	ctx       context.Context
	name      string
	api       *api.Server
	rClients  rClients
	hspConfig hsp.HspConfig
	mongo     *mongo.Client
	metrics   observability.MetricsCollection
}

// Organise multiple RabbitMQ clients
// Each has it's own connection and channel
type rClients struct {
	emitHSP    *rabbitmq.Client
	receiveHSP *rabbitmq.Client
	emitEvents *rabbitmq.Client
}

func main() {
	// Define flags
	fs := flag.NewFlagSet("default", flag.ContinueOnError)
	var (
		serviceName        = fs.String("service-name", "consumer-service", "service name")
		environment        = fs.String("environment", "dev", "environment name")
		port               = fs.Int("port", 8001, "Port to listen on")
		portMetrics        = fs.Int("port-metrics", 8101, "metrics port")
		logLevel           = fs.String("log-level", "debug", "log level")
		traceURL           = fs.String("trace-url", "http://localhost:14268/api/traces", "Jaeger compatible trace service URL")
		rabbitMQURI        = fs.String("rabbitmq-uri", "amqp://guest:guest@localhost:5672", "RabbitMQ service URI")
		rabbitMQExchHSP    = fs.String("rabbitmq-exchange-hsp", "laterail.hsp", "RabbitMQ exchange name for HSP requests")
		rabbitMQExchEvents = fs.String("rabbitmq-exchange-events", "laterail.events", "RabbitMQ exchange name for SSE events")
		dbURI              = fs.String("database-uri", "postgres://user:secret@localhost", "Relational database URI")
		mongoURI           = fs.String("mongo-uri", "mongodb+srv://user:secret@localhost", "MongoDB URI")
		hspUser            = fs.String("hsp-user", "user", "Username for HSP API")
		hspPassword        = fs.String("hsp-password", "secret", "Password for HSP API")
		idleTimeout        = fs.Duration("http-idle-timeout", time.Minute, "HTTP idle timeout")
		readTimeout        = fs.Duration("http-read-timeout", 5*time.Second, "HTTP read timeout")
		writeTimeout       = fs.Duration("http-write-timeout", 10*time.Second, "HTTP write timeout")
	)

	// Parse flags and env vars
	if err := ff.Parse(fs, os.Args[1:], ff.WithEnvVarPrefix("LATERAIL")); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

	// Graceful shutdown, context listens for interrupt signal from OS
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	log, tp, err := observability.Init(*logLevel, *traceURL, *serviceName, *environment)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to initialise observability resources")
	}
	metrics, err := observability.InitMetrics(hsp.DefaultMetrics)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to initialise metrics resources")
	}

	// Log startup
	log.Info().Msgf("Starting: %s", *serviceName)

	// Setup RabbitMQ resources
	emitHSP, receiveHSP, emitEvents := initMessages(log, *rabbitMQURI, *rabbitMQExchHSP, *rabbitMQExchEvents)

	// Open psql connection
	if err := db.AddConnection(*dbURI); err != nil {
		log.Fatal().Err(err).Msg("failed to add new postgres connection")
	}

	// Open mongo connection to collection and seed `stations` collection if empty
	mongo, err := mongodb.NewClient(*mongoURI)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to add new mongodb connection")
	}
	stations := mongodb.CollectionModel{
		DB: mongo,
	}
	stations.Coll = stations.GetCollection("stations")
	count, _ := stations.Coll.CountDocuments(context.Background(), bson.D{})
	if count == 0 {
		_, err = stations.AddFromFile(context.Background())
		if err != nil {
			log.Fatal().Err(err).Msg("failed to seed new mongodb collection")
		}
	}

	// Instantiate app
	app := &application{
		ctx:  ctx,
		name: *serviceName,
		// metrics: metrics,
		mongo: mongo,
		rClients: rClients{
			emitHSP:    emitHSP,
			receiveHSP: receiveHSP,
			emitEvents: emitEvents,
		},
		hspConfig: hsp.HspConfig{
			Username: *hspUser,
			Password: *hspPassword,
		},
		metrics: metrics,
	}

	// Setup API
	apiConfig := api.Config{
		Name:         *serviceName,
		Port:         *port,
		PortMetrics:  *portMetrics,
		IdleTimeout:  *idleTimeout,
		ReadTimeout:  *readTimeout,
		WriteTimeout: *writeTimeout,
	}
	app.api, err = api.NewServer(apiConfig, &log, tp)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to initialise API server")
	}
	// Add application specific routes
	additionalRoutes := app.routes()
	httpServer := app.api.ListenAndServe(additionalRoutes) // Will os.Exit(1) on error

	// Listen for RabbitMQ messages
	go app.Receive(app.rClients.receiveHSP)

	// Graceful shutdown
	<-ctx.Done()
	signals.Shutdown(stop, 5*time.Second, httpServer, tp, emitHSP, emitEvents, receiveHSP)

	// Stop DB resources
	mongo.Disconnect(ctx)
	db.Conn.Close()

	log.Info().Msg("shut down complete")
}
