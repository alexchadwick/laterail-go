package main

import (
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"laterail.com/internal/rabbitmq"
)

// Exports client objects which automatically reconnect when the connection
// fails, and blocks all pushes until the connection succeeds. It also confirms
// every outgoing message, so none are lost. It doesn't automatically ack each
// message, but leaves that to the parent process, since it is usage-dependent.
func initMessages(logger zerolog.Logger, uri, directExchange, topicExchange string) (emitHSP, receiveHSP, emitEvents *rabbitmq.Client) {
	emitHSP = rabbitmq.New(
		uri,
		directExchange,
		amqp.Persistent,
		1,
		rabbitmq.Queue{
			Name:       directExchange,
			Durable:    true,
			AutoDelete: false,
			Exclusive:  false,
		},
		rabbitmq.Exchange{
			Name:    directExchange,
			Kind:    "direct",
			Durable: true,
		},
		logger)

	receiveHSP = rabbitmq.New(
		uri,
		directExchange,
		amqp.Persistent,
		1,
		rabbitmq.Queue{
			Name:       directExchange,
			Durable:    true,
			AutoDelete: false,
			Exclusive:  false,
		},
		rabbitmq.Exchange{
			Name:    directExchange,
			Kind:    "direct",
			Durable: true,
		},
		logger)

	// Temporary queues created within handlers
	emitEvents = rabbitmq.New(
		uri,
		"",
		amqp.Transient,
		1,
		rabbitmq.Queue{},
		rabbitmq.Exchange{
			Name:    topicExchange,
			Kind:    "topic",
			Durable: false,
		},
		logger)

	return emitHSP, receiveHSP, emitEvents
}

// Waits for the connection to be ready, and then starts consuming messages from
// the message queue. Listens to channels to stop the consumer and detect
// channel closure events respectively. On each iteration of the loop, if
// ctx.Done() is closed, the consumer closes the connection and returns. If
// chClosedCh receives a channel closure event, the consumer tries to
// re-establish the connection and resume consuming. If a new message is
// received, the function acknowledges it and prints the message body. Closes
// the connection or channel, so must be the only consumer.
func (app *application) Receive(client *rabbitmq.Client) {
	if err := client.Wait(app.ctx); err != nil {
		return
	}

	deliveries, err := client.Consume()
	if err != nil {
		log.Warn().Msgf("Could not start consuming messages: %s", err)
		return
	}

	chClosedCh := make(chan *amqp.Error, 1)
	client.Channel.NotifyClose(chClosedCh)

	for {
		select {
		case <-app.ctx.Done():
			log.Warn().Msg("Service stopping. No longer receiving messages from broker")
			return

		case amqErr := <-chClosedCh:
			// This case handles the event of closed channel e.g. abnormal shutdown
			log.Warn().Msgf("AMQP Channel closed due to: %s. Attempting to re-open", amqErr)

			deliveries, err = client.Consume()
			if err != nil {
				// If the AMQP channel is not ready, it will continue the loop. Next
				// iteration will enter this case because chClosedCh is closed by the
				// library
				log.Warn().Msg("Error trying to consume messages, will try again")

				// Wait for reconnect delay
				time.Sleep(rabbitmq.ReconnectDelay)
				continue
			}

			// Re-set channel to receive notifications
			// The library closes this channel after abnormal shutdown
			chClosedCh = make(chan *amqp.Error, 1)
			client.Channel.NotifyClose(chClosedCh)

		case delivery := <-deliveries:
			// Handle new message
			if err := app.HandleMessage(delivery); err != nil {
				log.Error().Err(err).Msg("failed to handle message")
				delivery.Nack(false, false)
			}

			// Send positive acknowledgment
			if err := delivery.Ack(false); err != nil {
				log.Error().Err(err).Msg("failed to acknowledging message")
			}
		}
	}
}
