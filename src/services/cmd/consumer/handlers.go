package main

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"laterail.com/internal/db"
	"laterail.com/internal/hsp"
	"laterail.com/internal/initial"
	"laterail.com/internal/mongodb"
	"laterail.com/internal/observability"
	"laterail.com/internal/rabbitmq"
	"laterail.com/internal/util"
)

// Store history/progress of a request
// InitialRequest is a pointer to enable json omitempty
// See issue: https://github.com/golang/go/issues/11939
type historyDocument struct {
	RequestId      string                     `bson:"request_id,omitempty" json:"request_id,omitempty"`
	Metadata       metadata                   `bson:"metadata,omitempty" json:"metadata,omitempty"`
	InitialRequest *initial.InitialRequest    `bson:"initial_request,omitempty" json:"initial_request,omitempty"`
	Details        map[int64]*detailsFrontend `bson:"details,omitempty" json:"details,omitempty"`
}

type metadata struct {
	Status          string `bson:"status,omitempty" json:"status,omitempty"`
	TimeCreated     string `bson:"time_created,omitempty" json:"time_created,omitempty"`
	TimeFinished    string `bson:"time_completed,omitempty" json:"time_completed,omitempty"`
	MetricsTotal    int    `bson:"metrics_total,omitempty" json:"metrics_total,omitempty"`       // New HSP metrics requests to be ran
	MetricsDone     int    `bson:"metrics_done,omitempty" json:"metrics_done,omitempty"`         // New HSP metric requests completed for the parent request_id. Track progress
	DetailsTotal    int    `bson:"details_total,omitempty" json:"details_total,omitempty"`       // New HSP detail requests to be ran
	DetailsDone     int    `bson:"details_done,omitempty" json:"details_done,omitempty"`         // New HSP detail requests completed for the parent request_id. Track progress
	ResultsNew      int    `bson:"results_new,omitempty" json:"results_new,omitempty"`           // New HSP details responses
	ResultsExisting int    `bson:"results_existing,omitempty" json:"results_existing,omitempty"` // Relevant HSP details responses found in datastore
}

// Processed data in the format required by frontend client
type detailsFrontend struct {
	RID  int64       `bson:"rid" json:"rid"`
	From stationStop `bson:"from" json:"from"`
	To   stationStop `bson:"to" json:"to"`
}
type stationStop struct {
	Station          string `bson:"station" json:"station"`
	SchedDepart      string `bson:"sched_depart" json:"sched_depart"`
	SchedArrival     string `bson:"sched_arrival" json:"sched_arrival"`
	ActualDepart     string `bson:"actual_depart" json:"actual_depart"`
	ActualArrival    string `bson:"actual_arrival" json:"actual_arrival"`
	Delay            int32  `bson:"delay" json:"delay"`
	DelayValid       bool   `bson:"delay_valid" json:"delay_valid"`
	LateCancelReason int32  `bson:"late_cancel_reason" json:"late_cancel_reason"`
}

// Returning a non-nil error will automatically send a REQ command to NSQ to re-queue the message.
func (app *application) HandleMessage(m amqp.Delivery) error {
	// Get message context from headers
	msgCtx := rabbitmq.ExtractAMQPHeaders(app.ctx, m.Headers)
	ctx, span, traceID, spanID := observability.StartSpan(msgCtx)
	defer span.End()

	var requestType string

	// Unmarshal message into both Metrics and Details types
	metricMessage, err := rabbitmq.Unmarshal[rabbitmq.ServiceMetric](m.Body)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to unmarshall request interface to hsp MetricsRequest: %w", err))
		return err
	}
	detailsMessage, err := rabbitmq.Unmarshal[rabbitmq.ServiceDetails](m.Body)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to unmarshall request interface to hsp DetailsRequest: %w", err))
		return err
	}

	// Setup Mongodb resources
	historyMongo := mongodb.InitCollectionModel("history", app.mongo)

	// Metrics Request
	if metricMessage.Request.FromDate != "" && detailsMessage.Request.Rid == "" {
		requestType = metricMessage.Request.GetType()
		requestID := metricMessage.RequestId
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Str("request_id", requestID).
			Str("message_id", metricMessage.MessageId).
			Str("message_type", requestType).
			Msg("metrics message accepted from the queue")

		// Create document for request_id if it doesn't already exist
		eventDocFirst := historyDocument{
			RequestId:      requestID,
			InitialRequest: &metricMessage.InitialRequest,
			Metadata: metadata{
				Status:          "created",
				TimeCreated:     time.Now().UTC().Format(time.RFC3339),
				TimeFinished:    time.Time{}.UTC().Format(time.RFC3339),
				MetricsTotal:    metricMessage.ParentRequestCount,
				MetricsDone:     0,
				DetailsTotal:    0,
				DetailsDone:     0,
				ResultsNew:      0,
				ResultsExisting: 0,
			},
		}
		_, err := historyMongo.Coll.InsertOne(
			ctx,
			eventDocFirst,
		)
		if err != nil {
			// Existing request_id document
			if mongo.IsDuplicateKeyError(err) {
				log.Debug().
					Str("trace_id", traceID).
					Str("span_id", spanID).
					Str("request_id", requestID).
					Str("message_id", metricMessage.MessageId).
					Str("message_type", requestType).
					Msg("found existing request_id in collection.")
			} else {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to upsert request history document: %w", err))
				return err
			}
		} else {
			log.Debug().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Str("request_id", requestID).
				Str("message_id", metricMessage.MessageId).
				Str("message_type", requestType).
				Msg("inserted new document in history collection")

			// Push new parent request history document to SSE service
			err := app.rClients.emitEvents.Push(eventDocFirst, requestID, ctx)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to publish Event message: %w", err))
				return err
			}
		}

		// Make new HSP Metrics request
		sortedRidArray, _, err := hsp.SubmitHSPRequest(hsp.GenericRequest[hsp.Request]{Request: metricMessage.Request}, app.hspConfig, hsp.GetHspResponse, ctx, &log.Logger, app.metrics)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to process new metrics request: %w", err))
			return err
		}

		// Check if RIDs exist in database details table. Return list of rids not present
		updatedDoc := historyDocument{}
		updates := bson.D{{Key: "$inc", Value: bson.M{"metadata.metrics_done": 1}}}
		// If metrics response is not empty
		if len(sortedRidArray) > 0 {
			newRid, existingRid, err := hsp.GroupRIDs(sortedRidArray, ctx)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed determine which RIDs in HSP ServiceMetrics response are new: %w", err))
				return err
			}

			// Get existingRid details values from datastore
			existingDetails, err := db.New(db.Conn).ListDetailsByRIDs(ctx, existingRid)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to pull details of existing RIDs from datastore: %w", err))
				return err
			}

			countExisting := len(existingDetails)
			countExistingFiltered := 0
			if countExisting > 0 {
				existingDetailsConverted := []db.AddDetailParams{}
				for _, detail := range existingDetails {
					// Only store in MongoDB if matches start||end station
					if detail.Station == metricMessage.InitialRequest.FromLoc || detail.Station == metricMessage.InitialRequest.ToLoc {
						detailConverted, err := util.ConvertType[db.AddDetailParams](detail)
						if err != nil {
							err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to pull details of existing RIDs from datastore: %w", err))
							return err
						}
						existingDetailsConverted = append(existingDetailsConverted, *detailConverted)
						countExistingFiltered++

					}
				}
				// Get number of new complete journeys from existing RIDs found in database
				// Where an RID is created for each stop, and we return two stops in the filtered journey
				existingCompleteJourneys := math.Floor(float64(countExistingFiltered) / 2.0)

				// Format the results for frontend consumption before setting in mongodb
				updateExistingDetails := formatDetails(existingDetailsConverted, metricMessage.InitialRequest.FromLoc, metricMessage.InitialRequest.ToLoc)
				updateExistingDetails = append(updateExistingDetails,
					bson.E{Key: "$inc", Value: bson.M{"metadata.results_existing": existingCompleteJourneys}},
				)

				// Increment existing request counts in datastore
				_, err := historyMongo.Coll.UpdateOne(
					ctx,
					bson.D{
						{Key: "request_id", Value: requestID},
					},
					updateExistingDetails,
				)
				if err != nil {
					err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
					return err
				}
			}

			// Increment new details request count in datastore. Might be 0
			// and increment no. of metrics requests complete
			updates = append(updates,
				bson.E{Key: "$inc", Value: bson.M{"metadata.details_total": len(newRid)}},
			)

			// Insert RIDs into message queue
			err = app.PushDetailsMessage(newRid, requestID, metricMessage.InitialRequest, ctx)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to produce details messages: %w", err))
				return err
			}
		}

		// Push update to mongodb
		err = historyMongo.Coll.FindOneAndUpdate(
			ctx,
			bson.D{
				{Key: "request_id", Value: requestID},
			},
			updates,
			options.FindOneAndUpdate().SetReturnDocument(options.After),
		).Decode(&updatedDoc)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
			return err
		}

		// Push updated historyDoc to SSE service
		err = app.rClients.emitEvents.Push(updatedDoc, requestID, ctx)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to push Events message: %w", err))
			return err
		}

		// If that was the final metrics request
		if updatedDoc.Metadata.MetricsDone == metricMessage.ParentRequestCount {
			// If no new RIDs found in all metrics requests then parent request is done
			if updatedDoc.Metadata.DetailsTotal == 0 {
				var logMessage string
				timeFinished := time.Now().UTC().Format(time.RFC3339)
				eventDocFinalMetric := historyDocument{RequestId: requestID, Metadata: metadata{TimeFinished: timeFinished}}
				updates := bson.D{
					{Key: "$set", Value: bson.M{"metadata.time_completed": timeFinished}},
				}
				// If no existing results were found in datastore then response is empty
				if updatedDoc.Metadata.ResultsExisting == 0 {
					updates = append(updates,
						bson.E{Key: "$set", Value: bson.M{"metadata.status": "empty"}},
					)
					eventDocFinalMetric.Metadata.Status = "empty"
					logMessage = "No results"
				} else if updatedDoc.Metadata.ResultsExisting > 0 {
					updates = append(updates,
						bson.E{Key: "$set", Value: bson.M{"metadata.status": "complete"}},
					)
					eventDocFinalMetric.Metadata.Status = "complete"
					logMessage = "Found results, all are existing"
				} else {
					err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to mark request as complete: %w", err))
					return err
				}

				// Push results to mongodb
				_, err := historyMongo.Coll.UpdateOne(
					ctx,
					bson.D{
						{Key: "request_id", Value: requestID},
					},
					updates,
				)
				if err != nil {
					err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
					return err
				}

				log.Info().
					Str("trace_id", traceID).
					Str("span_id", spanID).
					Str("request_id", requestID).
					Msgf("request complete. %s", logMessage)

				// Push final metrics request message to SSE service
				err = app.rClients.emitEvents.Push(eventDocFinalMetric, requestID, ctx)
				if err != nil {
					err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to push Events message: %w", err))
					return err
				}
			}
		}

	} else if metricMessage.Request.FromDate == "" && detailsMessage.Request.Rid != "" {
		// Details Message
		requestType = detailsMessage.Request.GetType()
		requestID := detailsMessage.RequestId
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Str("request_id", requestID).
			Str("message_id", detailsMessage.MessageId).
			Str("message_type", requestType).
			Msg("details message accepted from the queue")

		_, newDetails, err := hsp.SubmitHSPRequest(hsp.GenericRequest[hsp.Request]{Request: detailsMessage.Request}, app.hspConfig, hsp.GetHspResponse, ctx, &log.Logger, app.metrics)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to process new details request: %w", err))
			return err
		}

		// Only add journeys which match the start||end stations to MongoDB
		filteredDetails := []db.AddDetailParams{}
		for _, details := range newDetails {
			if details.Station == detailsMessage.Stations.Start || details.Station == detailsMessage.Stations.End {
				filteredDetails = append(filteredDetails, details)
			}
		}

		// If details response contains valid station stop data
		updateNewDetails := bson.D{}
		countNew := len(filteredDetails)
		if countNew > 0 {
			// Format the results for frontend consumption before setting in mongodb
			newCompleteJourneys := math.Floor(float64(countNew) / 2.0)
			updateNewDetails = formatDetails(filteredDetails, detailsMessage.Stations.Start, detailsMessage.Stations.End)
			updateNewDetails = append(updateNewDetails,
				bson.E{Key: "$inc", Value: bson.M{"metadata.results_new": newCompleteJourneys}},
			)
		}

		// Increment even if there are no resuts
		updateNewDetails = append(updateNewDetails,
			bson.E{Key: "$inc", Value: bson.M{"metadata.details_done": 1}},
		)

		updatedDoc := historyDocument{}
		err = historyMongo.Coll.FindOneAndUpdate(
			ctx,
			bson.D{
				{Key: "request_id", Value: requestID},
			},
			updateNewDetails,
			options.FindOneAndUpdate().SetReturnDocument(options.After),
		).Decode(&updatedDoc)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
			return err
		}

		// Only push a scaling proportion of details messages to SSE to reduce
		// number of messages and data transmitted
		if filterDetailsMessages(updatedDoc) {
			err = app.rClients.emitEvents.Push(updatedDoc, requestID, ctx)
			if err != nil {
				return err
			}
			log.Debug().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Str("request_id", requestID).
				Msgf("publishing message number: %d", updatedDoc.Metadata.DetailsDone)
		}

		// If metrics requests have finished, check if this is last details request
		if updatedDoc.Metadata.MetricsDone == updatedDoc.Metadata.MetricsTotal &&
			updatedDoc.Metadata.DetailsDone == updatedDoc.Metadata.DetailsTotal {
			// Mark parent request as complete
			timeFinished := time.Now().UTC().Format(time.RFC3339)
			updates := bson.D{
				{Key: "$set", Value: bson.M{"metadata.status": "complete"}},
				{Key: "$set", Value: bson.M{"metadata.time_completed": timeFinished}},
			}
			eventDocFinalDetails := historyDocument{
				RequestId: requestID,
				Metadata: metadata{
					Status:       "complete",
					TimeFinished: timeFinished,
				}}
			_, err := historyMongo.Coll.UpdateOne(
				ctx,
				bson.D{
					{Key: "request_id", Value: requestID},
				},
				updates,
			)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
				return err
			}

			log.Info().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Str("request_id", requestID).
				Msg("request complete. Found matching results")

			// Push request finished message to SSE service
			err = app.rClients.emitEvents.Push(eventDocFinalDetails, requestID, ctx)
			if err != nil {
				return err
			}
		}
	} else {
		// Message doesn't match either metrics or details types
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to unmarshall request interface to either hsp DetailsRequest or MetricsRequest"))
		return err
	}

	return nil
}

// Produce new messages for HSP Service Details requests
func (app *application) PushDetailsMessage(rid []int64, requestID string, initial initial.InitialRequest, ctx context.Context) error {
	ctx, span, traceID, spanID := observability.StartSpan(ctx)
	defer span.End()

	// Add each HSP ServiceDetails request as single message in queue
	for i, req := range rid {
		msg := rabbitmq.ServiceDetails{
			RequestId: requestID,
			MessageId: uuid.New().String(),
			Time:      time.Now().UTC().Format(time.RFC3339),
			Request:   hsp.DetailsRequest{Rid: strconv.Itoa(int(req))},
			Stations: rabbitmq.Stations{
				Start: initial.FromLoc,
				End:   initial.ToLoc,
			},
		}

		err := app.rClients.emitHSP.Push(msg, app.rClients.emitHSP.RoutingKey, ctx)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to publish details message to topic: %w", err))
			return err
		}

		requestType := "hsp.DetailsRequest"
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Str("message_type", requestType).
			Msgf("Added message no: %d", i)
	}
	return nil
}

type tracker struct {
	IDs []string `json:"request_ids"`
}
type errResponse struct {
	Request_id string `json:"request_id"`
	Message    string `json:"message"`
}

// Get progress and results of existing request IDs
func (app application) tracker(w http.ResponseWriter, r *http.Request) {
	ctx, span, traceID, spanID := observability.StartSpan(r.Context())
	defer span.End()

	w.Header().Set("Content-Type", "application/json")

	// Decode json POST body
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	var request tracker
	if err := decoder.Decode(&request); err != nil {
		errMsg := "failed to decode POST body"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}

	// Setup Mongodb collection wrapper
	historyMongo := mongodb.InitCollectionModel("history", app.mongo)

	// Check the status of given request id
	responseArray := []historyDocument{}
	for _, id := range request.IDs {
		historyDoc := historyDocument{}
		err := historyMongo.Coll.FindOne(ctx, bson.D{{Key: "request_id", Value: id}}).Decode(&historyDoc)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				errResponse := []errResponse{
					{
						Request_id: id,
						Message:    "unknown request_id",
					},
				}
				// Respond to client
				w.WriteHeader(http.StatusNotFound)
				err := json.NewEncoder(w).Encode(errResponse)
				if err != nil {
					errMsg := "failed to encode unknown request_id tracker response"
					observability.NewErrorLogWithTrace(err, span, errMsg)
					http.Error(
						w,
						fmt.Sprintf("{\"message\": \"%s\"}", errMsg),
						http.StatusInternalServerError,
					)
				}
				return
			}
			observability.NewErrorLogWithTrace(err, span, "failed to search mongodb collection for request id")
			http.Error(
				w,
				"{\"message\": \"internal server error\"}",
				http.StatusInternalServerError,
			)
			return
		}
		responseArray = append(responseArray, historyDoc)
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msgf("Tracked status of requestID: %s", id)
	}

	// Respond to client
	w.WriteHeader(http.StatusOK)
	err := json.NewEncoder(w).Encode(responseArray)
	if err != nil {
		errMsg := "{\"message\": \"failed to encode completed tracker response\"}"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
}

// Get all stations
func (app application) getStations(w http.ResponseWriter, r *http.Request) {
	ctx, span, _, _ := observability.StartSpan(r.Context())
	defer span.End()

	// Setup Mongodb collection wrapper
	stationsColl := mongodb.InitCollectionModel("stations", app.mongo)

	stations, err := stationsColl.ListStations(ctx)
	if err != nil {
		errMsg := "{\"message\": \"failed to return station info\"}"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}

	// Respond to client
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(stations)
	if err != nil {
		errMsg := "{\"message\": \"failed to return station info\"}"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
}

// Only push a proportion of details request results to Events service
func filterDetailsMessages(historyDoc historyDocument) bool {
	detailsTotal := historyDoc.Metadata.DetailsTotal
	detailsDone := historyDoc.Metadata.DetailsDone
	push := false

	switch {
	// Always push the final message
	case detailsDone == detailsTotal:
		push = true
	case detailsTotal <= 10:
		push = true
	case detailsTotal > 10 && detailsTotal <= 20:
		if detailsDone%2 == 0 {
			push = true
		}
	case detailsTotal > 20 && detailsTotal <= 40:
		if detailsDone%5 == 0 {
			push = true
		}
	case detailsTotal > 40 && detailsTotal <= 100:
		if detailsDone%10 == 0 {
			push = true
		}
	case detailsTotal > 100:
		if detailsDone%20 == 0 {
			push = true
		}
	}

	return push
}

// Format the results for frontend consumption
// Necessary to use pointer values of struct in map to make the struct fields "To" and "From"
// addressible so we can assign values without copying and replacing the whole map
// See https://stackoverflow.com/a/70982246
func formatDetails(details []db.AddDetailParams, FromStation string, ToStation string) bson.D {
	formated := map[int64]*detailsFrontend{}

	for _, detail := range details {
		formated[detail.Rid] = &detailsFrontend{
			RID: detail.Rid,
		}
		// Compare other items in array with `detail`
		for _, d := range details {
			if d.Rid == detail.Rid {
				if d.Station == FromStation {
					formated[detail.Rid].From = stationStop{
						Station:          d.Station,
						SchedDepart:      d.SchedDepart.Time.Format(time.RFC3339),
						SchedArrival:     d.SchedArrival.Time.Format(time.RFC3339),
						ActualDepart:     d.ActualDepart.Time.Format(time.RFC3339),
						ActualArrival:    d.ActualArrival.Time.Format(time.RFC3339),
						Delay:            d.DelayDepart.Int32,
						DelayValid:       d.DelayDepart.Valid,
						LateCancelReason: d.LateCancelReason.Int32,
					}
				} else if d.Station == ToStation {
					formated[detail.Rid].To = stationStop{
						Station:          d.Station,
						SchedDepart:      d.SchedDepart.Time.Format(time.RFC3339),
						SchedArrival:     d.SchedArrival.Time.Format(time.RFC3339),
						ActualDepart:     d.ActualDepart.Time.Format(time.RFC3339),
						ActualArrival:    d.ActualArrival.Time.Format(time.RFC3339),
						Delay:            d.DelayArrival.Int32,
						DelayValid:       d.DelayArrival.Valid,
						LateCancelReason: d.LateCancelReason.Int32,
					}
				}
			}
		}
	}

	// Create mongodb update bson
	updateExistingDetails := bson.D{}
	for k, v := range formated {
		updateExistingDetails = append(updateExistingDetails, bson.E{Key: "$set", Value: bson.M{"details." + strconv.FormatInt(k, 10): v}})
	}

	return updateExistingDetails
}
