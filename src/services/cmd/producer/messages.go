package main

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"

	"laterail.com/internal/rabbitmq"
)

// Exports client objects which automatically reconnect when the connection
// fails, and blocks all pushes until the connection succeeds. It also confirms
// every outgoing message, so none are lost. It doesn't automatically ack each
// message, but leaves that to the parent process, since it is usage-dependent.
func initMessages(logger zerolog.Logger, uri, exchange string) *rabbitmq.Client {
	emitHSP := rabbitmq.New(
		uri,
		exchange,
		amqp.Persistent,
		1,
		rabbitmq.Queue{
			Name:       exchange,
			Durable:    true,
			AutoDelete: false,
			Exclusive:  false,
		},
		rabbitmq.Exchange{
			Name:    exchange,
			Kind:    "direct",
			Durable: true,
		},
		logger)

	return emitHSP
}
