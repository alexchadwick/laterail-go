# Producer service
  - Accepts and validates initial user request from the web frontend or API
  - Assigns a request id (uuid), which is re-used in all subsequent services and messages/requests
  - Breaks request down into valid HSP ServiceMetrics requests for external national rail HSP API
    - Requests are split by date range and time range, to avoid hitting the HSP API service limits (timeout after 1min). Adjust `LATERAIL_MAX_DATE_RANGE` and `LATERAIL_MAX_TIME_RANGE` to change this.
  - Adds ServiceMetrics request info to message broker queue, with request id and other metadata
    - These messages are received by the Consumer service
  - Returns http 202 (accepted) and the request id, or an error to client quickly

# Datastore(s)
 - None, no persistent data

# Run
```
laterail-go/src/services$ go run ./cmd/producer

# Requests will add messages to rabbitmq. Consume these in testing with:
nsq_to_file --topic=laterail.com.producer --output-dir=/tmp --lookupd-http-address=127.0.0.1:4161
```

## Environmental variables
| name | default | description |
|-|-|-|
| `LATERAIL_ENVIRONMENT` | `dev` | Port that http service listens on |
| `LATERAIL_LOG_LEVEL` | `debug` | Structured log level |
| `LATERAIL_MAX_DATE_RANGE` | `336h` | Max number of days covered by a single HSP ServiceMetrics request (hours) |
| `LATERAIL_MAX_TIME_RANGE` | `4h` | Max time duration of a single HSP ServiceMetrics request (hours) |
| `LATERAIL_PORT` | `8000` | Port that http service listens on |
| `LATERAIL_PORT_METRICS` | `8101` | Port for the Prometheus metrics and health endpoints |
| `LATERAIL_RABBITMQ_EXCHANGE` | `laterail.hsp` | RabbitMQ Exchange Name for HSP requests |
| `LATERAIL_RABBITMQ_URI` | `"amqp://guest:guest@localhost:5672/"` | URL for RabbitMQ service  | 
| `LATERAIL_TRACE_URL` | `http://localhost:14268/api/traces` | URL of tracing service/collector |

# Example requests
## Submit
```
curl --location --request POST 'http://<SERVICE-URL>:<PORT>/v0/submit' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from_loc":   "BDG",
    "to_loc":     "GLC",
    "from_time":  "0900",
    "to_time":    "1000",
    "from_date":  "2022-09-07",
    "to_date":    "2022-09-07"
}'
```