package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel/attribute"

	"laterail.com/internal/api"
	"laterail.com/internal/initial"
	"laterail.com/internal/observability"
	"laterail.com/internal/rabbitmq"
	"laterail.com/internal/validate"
)

// Response to client request
type response struct {
	RequestId    string `json:"request_id"`
	RequestCount int    `json:"request_count"`
	Info         string `json:"info,omitempty"`
}

// POST new submitted initial request
func (app application) submit(w http.ResponseWriter, r *http.Request) {
	ctx, span, traceID, spanID := observability.StartSpan(r.Context())
	defer span.End()

	reqIDInterface := r.Context().Value(api.ContextKeyRequestID)
	reqID, ok := reqIDInterface.(string)
	if !ok {
		errMsg := "cannot read request id"
		err := fmt.Errorf("failed request id type assertion to string")
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}
	span.SetAttributes(attribute.String("laterail.request_id", reqID))

	// Decode json POST body
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	var ir initial.InitialRequest
	if err := decoder.Decode(&ir); err != nil {
		errMsg := "failed to decode POST body"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}

	// Validate InitialRequest
	val := validate.New()

	// Include validation errors in response to client
	if err := val.Struct(ir); err != nil {
		errorResponse := validate.ToErrResponse(err)
		if errorResponse != nil {
			observability.NewErrorLogWithTrace(err, span, "failed to validate POST body")
			response := response{
				RequestId: reqID,
				Info:      strings.Join(errorResponse.Errors, ","),
			}

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			err = json.NewEncoder(w).Encode(response)
			if err != nil {
				errMsg := "failed to encode validation error response"
				observability.NewErrorLogWithTrace(err, span, errMsg)
				http.Error(w, errMsg, http.StatusInternalServerError)
				return
			}
		}
		// Return on validation error
		return
	}

	// Break initial request into HSP ServiceMetrics requests
	batchRequests, err := ir.Split(ctx, app.dateRangeDayMax, app.timeRangeHrMax)
	if err != nil {
		errMsg := "failed to decode POST body"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
	requestCount := len(batchRequests)
	span.SetAttributes(attribute.Int("laterail.producer.metric_message_count", requestCount))
	if requestCount == 0 {
		errMsg := "failed to parse input"
		observability.NewErrorLogWithTrace(
			fmt.Errorf("no valid requests returned by initialRequest.Split for %+v", ir),
			span,
			errMsg,
		)
		response := response{
			RequestId:    reqID,
			RequestCount: requestCount,
			Info:         fmt.Sprintf("%s: %+v", errMsg, ir),
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		err = json.NewEncoder(w).Encode(response)
		if err != nil {
			errMsg := "failed to encode validation error response"
			observability.NewErrorLogWithTrace(err, span, errMsg)
			http.Error(w, errMsg, http.StatusInternalServerError)
			return
		}
	}

	// Add each HSP ServiceMetric request as single message in queue
	for i, req := range batchRequests {
		msg := rabbitmq.ServiceMetric{
			RequestId:          reqID,
			MessageId:          uuid.New().String(),
			Time:               time.Now().UTC().String(),
			ParentRequestCount: requestCount,
			Request:            req,
			InitialRequest:     ir,
		}

		//Publish the Message
		err = app.rabbitmq.Push(msg, app.rabbitmq.RoutingKey, ctx)
		if err != nil {
			errMsg := "failed to publish message to broker"
			observability.NewErrorLogWithTrace(err, span, errMsg)
			http.Error(w, errMsg, http.StatusInternalServerError)
			return
		}

		requestType := "hsp.MetricsRequest"
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Str("message_type", requestType).
			Msgf("Added message no: %d", i)
	}

	// Respond to client
	response := response{
		RequestId:    reqID,
		RequestCount: requestCount,
		Info:         "Request submitted successfully",
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		errMsg := "failed to encode successful response"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
}
