package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/peterbourgon/ff/v3"

	"laterail.com/internal/api"
	"laterail.com/internal/observability"
	"laterail.com/internal/rabbitmq"
	"laterail.com/internal/signals"
)

type application struct {
	ctx             context.Context
	name            string
	rabbitmq        *rabbitmq.Client
	api             *api.Server
	timeRangeHrMax  time.Duration
	dateRangeDayMax time.Duration
}

func main() {
	// Define flags
	fs := flag.NewFlagSet("default", flag.ContinueOnError)
	var (
		serviceName     = fs.String("service-name", "producer-service", "service name")
		environment     = fs.String("environment", "dev", "environment name")
		port            = fs.Int("port", 8000, "Port to listen on")
		portMetrics     = fs.Int("port-metrics", 8100, "metrics port")
		maxDateRange    = fs.Duration("max-date-range", 14*24*time.Hour, "max date duration of a single HSP ServiceMetrics request (days in hours)")
		maxTimeRange    = fs.Duration("max-time-range", 4*time.Hour, "max time duration of a single HSP ServiceMetrics request (hours)")
		logLevel        = fs.String("log-level", "debug", "log level")
		traceURL        = fs.String("trace-url", "http://localhost:14268/api/traces", "Jaeger compatible trace service URL")
		rabbitMQURI     = fs.String("rabbitmq-uri", "amqp://guest:guest@localhost:5672", "RabbitMQ service URI")
		rabbitMQExchHSP = fs.String("rabbitmq-exchange", "laterail.hsp", "RabbitMQ exchange name")
		idleTimeout     = fs.Duration("http-idle-timeout", time.Minute, "HTTP idle timeout")
		readTimeout     = fs.Duration("http-read-timeout", 5*time.Second, "HTTP read timeout")
		writeTimeout    = fs.Duration("http-write-timeout", 10*time.Second, "HTTP write timeout")
	)

	// Parse flags and env vars
	if err := ff.Parse(fs, os.Args[1:], ff.WithEnvVarPrefix("LATERAIL")); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

	// Enable graceful shutdown, context listens for interrupt signals
	// Used to stop http server and message broker clients
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	log, tp, err := observability.Init(*logLevel, *traceURL, *serviceName, *environment)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to initialise observability resources")
	}

	// Log startup
	log.Info().Msgf("Starting: %s", *serviceName)

	// Setup RabbitMQ resources
	emitHSP := initMessages(log, *rabbitMQURI, *rabbitMQExchHSP)

	app := &application{
		ctx:             ctx,
		name:            *serviceName,
		rabbitmq:        emitHSP,
		timeRangeHrMax:  *maxTimeRange,
		dateRangeDayMax: *maxDateRange,
	}

	// Setup API
	apiConfig := api.Config{
		Name:         *serviceName,
		Port:         *port,
		PortMetrics:  *portMetrics,
		IdleTimeout:  *idleTimeout,
		ReadTimeout:  *readTimeout,
		WriteTimeout: *writeTimeout,
	}
	app.api, err = api.NewServer(apiConfig, &log, tp)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to initialise http server")
	}
	// Add application specific routes to http server
	additionalRoutes := app.routes()
	httpServer := app.api.ListenAndServe(additionalRoutes) // Will os.Exit(1) on error

	// Graceful shutdown
	<-ctx.Done()
	signals.Shutdown(stop, 5*time.Second, httpServer, tp, emitHSP)

	log.Info().Msg("shut down complete")
}
