package main

import (
	"github.com/go-chi/chi/v5"
)

// Routes creates application routes which will be added to the default router
// api router.
func (app *application) routes() chi.Router {
	r := chi.NewRouter()

	// Initial release
	r.Route("/v0", func(r chi.Router) {
		r.Post("/submit", app.submit)
	})

	return r
}
