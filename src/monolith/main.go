package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"go.mongodb.org/mongo-driver/bson"

	"go.opentelemetry.io/otel"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"

	"github.com/rs/zerolog"

	"laterail-api/api"
	"laterail-api/db"
	"laterail-api/mongodb"
	"laterail-api/util"
)

func main() {
	// Graceful shutdown, context listens for interrupt signal from OS
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	// Set log levels go gin and zerolog. Defaults to Debug
	// Alternative: gin = release, zerolog = Info
	log := zerolog.New(os.Stdout)
	logLevel := os.Getenv("LOG_LEVEL")
	if logLevel == "INFO" {
		logLevel = "release"
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		logLevel = "debug"
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	// OpenTelemetry tracing setup
	var tp *sdktrace.TracerProvider
	tp, err := util.InitTracerProvider(os.Getenv("TRACE_URL"))
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed to initialise tracer provider")
	}
	otel.SetTracerProvider(tp)

	// Parent span for main process
	ctx, span := util.Tracer.Start(ctx, "main")
	defer span.End()
	spanContext := span.SpanContext()

	// Log startup
	log.Debug().
		Time("startup_time", time.Now().UTC()).
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msg("Laterail API starting up")

	// Open psql connection and migrate schema
	if err := db.Migrate(os.Getenv("DATABASE_ENGINE") + "://" + os.Getenv("DATABASE_URL")); err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Failed relational db migration")
	}
	if err := db.AddConnection("postgres://" + os.Getenv("DATABASE_URL")); err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Failed to add new postgres connection")
	}

	// Open mongo connection to collection and seed if empty
	mongo, err := mongodb.NewClient(os.Getenv("MONGO_URI"))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Failed to add new mongodb connection")
	}
	m := mongodb.StationModel{
		DB: mongo,
	}
	m.Coll = m.GetCollection()
	count, _ := m.Coll.CountDocuments(context.Background(), bson.D{})
	if count == 0 {
		_, err = m.AddFromFile(context.Background())
		if err != nil {
			log.Fatal().
				Err(err).
				Str("trace_id", spanContext.TraceID().String()).
				Str("span_id", spanContext.SpanID().String()).
				Msg("Failed to seed new mongodb collection")
		}
	}

	// Env vars and connection strings
	// Get jwks for ID service
	jwksURL := os.Getenv("JWKS_URL")
	if jwksURL == "" {
		log.Fatal().
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Missing environmental variable 'JWKS_URL'")
	}

	env := api.Env{
		Stations: m,
		JwksURL:  jwksURL,
		HspConfig: api.HspConfig{
			Username: os.Getenv("HSP_USER"),
			Password: os.Getenv("HSP_SECRET"),
		},
		LogLevel: logLevel,
	}

	// Create new server
	apiPort := ":" + os.Getenv("API_PORT")
	router := api.SetupRouter(ctx, env)
	s := &http.Server{
		Addr:              apiPort,           // configure the bind address
		Handler:           router,            // set the default handler
		ReadHeaderTimeout: 5 * time.Second,   // max time to read request headers
		WriteTimeout:      300 * time.Second, // max time to write response to the client
		IdleTimeout:       60 * time.Second,  // max time for connections using TCP Keep-Alive
		// BaseContext:       func(net.Listener) context.Context { return ctx }, // Gin requests use main context
	}

	// Initialise server in a goroutine, won't block graceful shutdown handling below
	go func() {
		if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal().
				Err(err).
				Str("trace_id", spanContext.TraceID().String()).
				Str("span_id", spanContext.SpanID().String()).
				Msg("Gin server failed")
		}
	}()

	// Listen for the interrupt signal.
	<-ctx.Done()

	// Restore default behavior on the interrupt signal and notify user of shutdown.
	stop()
	log.Info().
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msg("shutting down gracefully, press Ctrl+C again to force")

	// Shutdown server and tracer, timeout 5 seconds. Close db pool connections
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := s.Shutdown(ctx); err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Server forced to shutdown")
	}
	// End and flush main span before shutdown to retain parent span
	span.End()
	if err := tp.ForceFlush(ctx); err != nil {
		log.Fatal().
			Err(err).
			Msg("Error flushing tracer spans before shutdown")
	}
	if err := tp.Shutdown(ctx); err != nil {
		log.Fatal().
			Err(err).
			Msg("Error shutting down tracer provider")
	}
	db.Conn.Close()
	mongo.Disconnect(ctx)
}
