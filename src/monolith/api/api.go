package api

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"reflect"
	"sort"
	"strconv"
	"sync"
	"time"

	"laterail-api/db"
	"laterail-api/util"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel/attribute"
)

const (
	Weekday                  = "WEEKDAY"
	Saturday                 = "SATURDAY"
	Sunday                   = "SUNDAY"
	metricsURI               = "https://hsp-prod.rockshore.net/api/v1/serviceMetrics"
	detailsURI               = "https://hsp-prod.rockshore.net/api/v1/serviceDetails"
	timeRefTimeRange         = "1504"
	timeRefDateRange         = "2006-01-02"
	timeRefDatetime          = "200601021504"
	irTimeRangeMax           = 6 * time.Hour
	irDateRangeMax           = 14 * 24 * time.Hour
	MetricRequestMaxInterval = 6 * time.Hour // Max number of hours long a single HSP Service Metrics request can span
)

var (
	metricsRequestTolerance = []int{5, 15, 30}
)

type MetricsRequest struct {
	FromLoc   string `json:"from_loc"`
	ToLoc     string `json:"to_loc"`
	FromTime  string `json:"from_time"`
	ToTime    string `json:"to_time"`
	FromDate  string `json:"from_date"`
	ToDate    string `json:"to_date"`
	Days      string `json:"days"`
	Tolerance []int  `json:"tolerance"`
}

type InitialRequest struct {
	FromLoc   string `json:"from_loc"`
	ToLoc     string `json:"to_loc"`
	FromTime  string `json:"from_time"`
	ToTime    string `json:"to_time"`
	FromDate  string `json:"from_date"`
	ToDate    string `json:"to_date"`
	Tolerance []int  `json:"tolerance"`
}

// Max number of hours long a single HSP Service Metrics request can span
func (ir InitialRequest) TimeRangeMax() time.Duration {
	return irTimeRangeMax
}

// Max number of days long a single HSP Service Metrics request can span
func (ir InitialRequest) DateRangeMax() time.Duration {
	return irDateRangeMax
}

// Break initial requests into var ir.TimeRangeMax hour intervals
func (ir InitialRequest) TimeRange() ([]time.Time, error) {
	fromDateTime, err := time.Parse(timeRefTimeRange, ir.FromTime)
	if err != nil {
		return nil, fmt.Errorf("failed to parse time.Time: %w", err)
	}
	toDateTime, err := time.Parse(timeRefTimeRange, ir.ToTime)
	if err != nil {
		return nil, fmt.Errorf("failed to parse time.Time: %w", err)
	}

	irPeriods := newTimeSlice(fromDateTime, toDateTime, ir.TimeRangeMax())

	return irPeriods, nil
}

// Break initial requests into var ir.DateRangeMax date intervals
func (ir InitialRequest) DateRange() ([]time.Time, error) {
	fromDateTime, err := time.Parse(timeRefDateRange, ir.FromDate)
	if err != nil {
		return nil, fmt.Errorf("failed to parse time.Time: %w", err)
	}
	toDateTime, err := time.Parse(timeRefDateRange, ir.ToDate)
	if err != nil {
		return nil, fmt.Errorf("failed to parse time.Time: %w", err)
	}

	irPeriods := newTimeSlice(fromDateTime, toDateTime, ir.DateRangeMax())

	return irPeriods, nil
}

// Create slice of ServiceMetrics requests. Processed into time, date and day intervals
func (ir InitialRequest) Split(ctx context.Context) ([]MetricsRequest, error) {
	ctx, span := util.StartSpan(ctx)
	defer span.End()

	var requestSlice []MetricsRequest
	times, err := ir.TimeRange()
	if err != nil {
		err = util.NewErrorWithTrace(span, fmt.Errorf("failed to create initial request time range: %w", err))
		return nil, err
	}
	dates, err := ir.DateRange()
	if err != nil {
		err = util.NewErrorWithTrace(span, fmt.Errorf("failed to create initial request date range: %w", err))
		return nil, err
	}
	timesString := sliceTimeToString(times, timeRefTimeRange)
	datesString := sliceTimeToString(dates, timeRefDateRange)

	// Loop over date, time and day type ranges
	for i := 0; i < len(datesString)-1; i++ {
		days, err := getDayTypes(dates[i], dates[i+1])
		if err != nil {
			err = util.NewErrorWithTrace(span, fmt.Errorf("failed evaluate day types in date range: %w", err))
			return nil, err
		}

		for j := 0; j < len(timesString)-1; j++ {
			for k := 0; k < len(days); k++ {
				requestSlice = append(requestSlice, MetricsRequest{
					FromLoc:   ir.FromLoc,
					ToLoc:     ir.ToLoc,
					FromTime:  timesString[j],
					ToTime:    timesString[j+1],
					FromDate:  datesString[i],
					ToDate:    datesString[i+1],
					Days:      days[k],
					Tolerance: metricsRequestTolerance,
				})
			}
		}
	}
	return requestSlice, nil
}

// Start of response body returned by HSP ServiceMetrics request
type MetricsResponse struct {
	Header   Header     `json:"header"`
	Services []Services `json:"Services"`
}

type Header struct {
	FromLocation string `json:"from_location"`
	ToLocation   string `json:"to_location"`
}

type Services struct {
	ServiceAttributesMetrics ServiceAttributesMetrics `json:"serviceAttributesMetrics"`
	Metrics                  []Metrics                `json:"Metrics"`
}

type Metrics struct {
	ToleranceValue   string `json:"tolerance_value"`
	NumNotTolerance  string `json:"num_not_tolerance"`
	NumTolerance     string `json:"num_tolerance"`
	PercentTolerance string `json:"percent_tolerance"`
	GlobalTolerance  bool   `json:"global_tolerance"`
}

type ServiceAttributesMetrics struct {
	OriginLocation      string   `json:"origin_location"`
	DestinationLocation string   `json:"destination_location"`
	GbttPtd             string   `json:"gbtt_ptd"`
	GbttPta             string   `json:"gbtt_pta"`
	TocCode             string   `json:"toc_code"`
	MatchedServices     string   `json:"matched_services"`
	Rids                []string `json:"rids"`
}

// Parse MetricsResponse into struct db.Station
func (mresp MetricsResponse) Station() []db.AddStationParams {
	var stations []db.AddStationParams
	stationMap := map[string]bool{}

	// Get all stations in response. Only add to []db.Station once
	mapKeys := []string{mresp.Header.FromLocation, mresp.Header.ToLocation}
	for _, i := range mresp.Services {
		mapKeys = append(mapKeys, i.ServiceAttributesMetrics.OriginLocation)
		mapKeys = append(mapKeys, i.ServiceAttributesMetrics.DestinationLocation)
	}
	for _, v := range mapKeys {
		_, ok := stationMap[v]
		if !ok {
			stationMap[v] = true
			stations = append(stations, db.AddStationParams{
				StationID: v,
				StationName: sql.NullString{
					String: "",
					Valid:  false},
			})
		}
	}

	return stations
}

// Parse MetricsResponse into structs db.AddServiceParams and db.AddRunParams
func (mresp MetricsResponse) Service() ([]db.AddServiceParams, []db.AddRunParams, error) {
	services := []db.AddServiceParams{}
	runs := []db.AddRunParams{}
	serviceMap := map[string]bool{}

	for i := 0; i < len(mresp.Services); i++ {
		rids := mresp.Services[i].ServiceAttributesMetrics.Rids
		for j := 0; j < len(rids); j++ {
			// ServiceID is the last 8 chars of Rid
			rid := mresp.Services[i].ServiceAttributesMetrics.Rids[j]
			ridInt, err := strconv.Atoi(rid)
			if err != nil {
				return nil, nil, fmt.Errorf("failed strconv rid: %w", err)
			}
			serviceId := mresp.Services[i].ServiceAttributesMetrics.Rids[j][8:]
			serviceIdInt, err := strconv.Atoi(serviceId)
			if err != nil {
				return nil, nil, fmt.Errorf("failed strconv serviceId: %w", err)
			}
			// Only store a ServiceID once
			_, ok := serviceMap[serviceId]
			if !ok {
				serviceMap[serviceId] = true
				services = append(services, db.AddServiceParams{
					ServiceID:          int32(serviceIdInt),
					StationOrigin:      mresp.Services[i].ServiceAttributesMetrics.OriginLocation,
					StationDestination: mresp.Services[i].ServiceAttributesMetrics.DestinationLocation,
				})
				runs = append(runs, db.AddRunParams{
					Rid:       int64(ridInt),
					ServiceID: int32(serviceIdInt),
				})
			}
		}
	}
	return services, runs, nil
}

// Parse metrics response into db structs
func (mresp MetricsResponse) Parse(ctx context.Context) ([]db.AddStationParams, []db.AddServiceParams, []db.AddRunParams, error) {
	_, span := util.StartSpan(ctx)
	defer span.End()

	stations := mresp.Station()
	services, runs, err := mresp.Service()
	if err != nil {
		err = util.NewErrorWithTrace(span, fmt.Errorf("MetricsResponse.Service: %w", err))
		return nil, nil, nil, err
	}

	return stations, services, runs, nil
}

// End of response body returned by HSP ServiceMetrics request

// Start of response body returnd by HSP ServiceDetails request

type DetailsResponse struct {
	ServiceAttributesDetails ServiceAttributesDetails `json:"serviceAttributesDetails"`
}

type ServiceAttributesDetails struct {
	DateOfService string      `json:"date_of_service"`
	TocCode       string      `json:"toc_code"`
	Rid           string      `json:"rid"`
	Locations     []Locations `json:"locations"`
}

type Locations struct {
	Location       string `json:"location"`
	GbttPtd        string `json:"gbtt_ptd"`
	GbttPta        string `json:"gbtt_pta"`
	ActualTd       string `json:"actual_td"`
	ActualTa       string `json:"actual_ta"`
	LateCancReason string `json:"late_canc_reason"`
}

// Convert detailsResponse into list of db.AddDetailParams, which represent railway stops of a service
func (d DetailsResponse) Location() ([]db.AddDetailParams, error) {
	details := []db.AddDetailParams{}

	ridString := d.ServiceAttributesDetails.Rid
	ridInt, err := strconv.Atoi(ridString)
	if err != nil {
		return nil, fmt.Errorf("failed strconv rid: %w", err)
	}
	rid := int64(ridInt)

	serviceInt, err := strconv.Atoi(d.ServiceAttributesDetails.Rid[8:])
	if err != nil {
		return nil, fmt.Errorf("failed strconv rid: %w", err)
	}
	serviceID := int32(serviceInt)
	for _, i := range d.ServiceAttributesDetails.Locations {
		ScheduledDeparture, err := NullTime(ridString[:8], i.GbttPtd)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullTime: %w", err)
		}
		ScheduledArrival, err := NullTime(ridString[:8], i.GbttPta)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullTime: %w", err)
		}
		ActualDeparture, err := NullTime(ridString[:8], i.ActualTd)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullTime: %w", err)
		}
		ActualArrival, err := NullTime(ridString[:8], i.ActualTa)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullTime: %w", err)
		}
		LateCancelReason, err := NullInt(i.LateCancReason)
		if err != nil {
			return nil, fmt.Errorf("failed to convert to sql.NullInt32: %w", err)
		}

		var delayFloat64 float64
		var delayMins sql.NullInt32

		// Calculate delay differently for each scenario:
		// Starting station (no arrival), skipped stations (no arrival/departure) or scheduled arrival
		if !ScheduledArrival.Valid {
			delayFloat64 = ActualDeparture.Time.Sub(ScheduledDeparture.Time).Minutes()
			delayMins = sql.NullInt32{
				Int32: int32(delayFloat64),
				Valid: true,
			}
		} else if !ActualArrival.Valid {
			delayMins = sql.NullInt32{}
		} else {
			delayFloat64 := ActualArrival.Time.Sub(ScheduledArrival.Time).Minutes()
			delayMins = sql.NullInt32{
				Int32: int32(delayFloat64),
				Valid: true,
			}
		}

		details = append(details, db.AddDetailParams{
			Rid:                rid,
			ServiceID:          serviceID,
			StationName:        i.Location,
			ScheduledDeparture: ScheduledDeparture,
			ScheduledArrival:   ScheduledArrival,
			ActualDeparture:    ActualDeparture,
			ActualArrival:      ActualArrival,
			DelayMins:          delayMins,
			LateCancelReason:   LateCancelReason,
		})
	}

	return details, nil
}

// Convert detailsResponse into list of db.AddStationParams
func (d DetailsResponse) Station() []db.AddStationParams {
	var stations []db.AddStationParams
	stationMap := map[string]bool{}

	// Get all stations in response. Only add to []db.Station once
	mapKeys := []string{}
	for _, i := range d.ServiceAttributesDetails.Locations {
		mapKeys = append(mapKeys, i.Location)
	}
	for _, v := range mapKeys {
		_, ok := stationMap[v]
		if !ok {
			stationMap[v] = true
			stations = append(stations, db.AddStationParams{
				StationID: v,
				StationName: sql.NullString{
					String: "",
					Valid:  false},
			})
		}
	}

	return stations
}

// Parse details response into db structs
func (d DetailsResponse) Parse() ([]db.AddDetailParams, []db.AddStationParams, error) {
	stations := d.Station()
	details, err := d.Location()
	if err != nil {
		return nil, nil, fmt.Errorf("MetricsResponse.Service: %w", err)
	}

	return details, stations, nil
}

// End of response body returnd by HSP ServiceDetails request

type ServiceDetails struct {
	Rid       string    `db:"rid"`
	FromLoc   string    `db:"from_location"`
	ToLoc     string    `db:"to_location"`
	GbttPtd   time.Time `db:"gbtt_ptd"`
	GbttPta   time.Time `db:"gbtt_pta"`
	ActualTd  string    `json:"actual_td"`
	ActualTa  string    `json:"actual_ta"`
	Dealy     int       `db:"dealy"`
	Cancelled string    `db:"cancelled"`
}

// Used to return and test response http error codes

type DetailsRequest struct {
	Rid string `json:"rid"`
}

type BadStatusError struct {
	Status int
}

func (b BadStatusError) Error() string {
	return fmt.Sprintf("GetHspResponse(): the external HSP API returned the HTTP response status code %d", b.Status)
}

type HspResponse struct {
	Data interface{}
}

func (u *HspResponse) Unmarshal(b []byte) error {
	metrics := &MetricsResponse{}
	err := json.Unmarshal(b, metrics)
	// no error, but we also need to make sure we unmarshaled something
	if err == nil && metrics.Header.FromLocation != "" {
		u.Data = metrics
		return nil
	}
	// abort if we have an error other than the wrong type
	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return fmt.Errorf("Error unmarshalling metrics json")
	}

	details := &DetailsResponse{}
	err = json.Unmarshal(b, details)
	if err != nil {
		return fmt.Errorf("Error unmarshalling details json")
	}
	u.Data = details

	return nil
}

// Credetials for external HSP API service
type HspConfig struct {
	Username string
	Password string
}

// Used to pass api.GetHspResponse as parameter, allow replacement in testing, instead of making external HSP API requests
type HSPGetter func(client *http.Client, hspConfig HspConfig, inputBody interface{}, ctx context.Context) (interface{}, error)

// Get response from HSP API endpoints serviceMetrics and serviceDetails, from input of MetricsRequest or DetailsRequest post body
func GetHspResponse(client *http.Client, hspConfig HspConfig, inputBody interface{}, ctx context.Context) (interface{}, error) {
	_, span := util.StartSpan(ctx)
	defer span.End()

	// Get span and trace IDs to include in logs
	spanCtx := span.SpanContext()
	traceID := spanCtx.TraceID().String()
	spanID := spanCtx.SpanID().String()

	HspResp := HspResponse{}

	postBody, err := json.Marshal(inputBody)
	if err != nil {
		err = util.NewErrorWithTrace(span, fmt.Errorf("Error marshalling post json: %q", err))
		return nil, err
	}
	span.SetAttributes(attribute.String("requestBody", string(postBody)))

	// Set HSP API endpoint
	var reqURI string
	requestType := reflect.TypeOf(inputBody).String()
	if requestType == "api.MetricsRequest" {
		reqURI = metricsURI
	} else if requestType == "api.DetailsRequest" {
		reqURI = detailsURI
	} else {
		err = util.NewErrorWithTrace(span, fmt.Errorf("unexpected request type: %s", requestType))
		return nil, err
	}
	span.SetAttributes(attribute.String("requestType", requestType))

	// Build and make request
	req, err := http.NewRequest("POST", reqURI, bytes.NewBuffer(postBody))
	if err != nil {
		err = util.NewErrorWithTrace(span, fmt.Errorf("Error buidling request: %q", err))
		return nil, err
	}
	req.Header.Add("Content-Type", `application/json`)
	req.SetBasicAuth(hspConfig.Username, hspConfig.Password)

	resp, err := client.Do(req)
	if err != nil {
		err = util.NewErrorWithTrace(span, fmt.Errorf("Error attempting request: %q", err))
		return nil, err
	}
	defer resp.Body.Close() // Close body only if response non-nil
	if resp.StatusCode != http.StatusOK {
		err = util.NewErrorWithTrace(span, BadStatusError{resp.StatusCode})
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = util.NewErrorWithTrace(span, fmt.Errorf("Error reading response: %q", err))
		return nil, err
	}

	// Unmarshal either metric or detail response using interface
	HspResp.Unmarshal(body)

	// Log number services are returned
	if requestType == "api.MetricsRequest" {
		responseData := HspResp.Data.(*MetricsResponse)
		log.Debug().
			Str("request_type", requestType).
			Interface("post_body", inputBody).
			Int("returned_services", len(responseData.Services)).
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msg("New Metrics request")
	} else if requestType == "api.DetailsRequest" {
		responseData := HspResp.Data.(*DetailsResponse)
		log.Debug().
			Str("request_type", requestType).
			Interface("post_body", inputBody).
			Str("returned_services", responseData.ServiceAttributesDetails.DateOfService).
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msg("New Details request")
	}

	return HspResp.Data, nil
}

// Covert local time, format "200601031504" for reference "Mon Jan 2 15:04:05 -0700 MST 2006", string to UTC
//func localStringToUTC(local string) (time.Time, error) {
//	loc, _ := time.LoadLocation("Europe/London")
//
//	ref := "200601021504"
//	output, err := time.ParseInLocation(ref, local, loc)
//	if err != nil {
//		return time.Time{}, fmt.Errorf("Error parsing time: %q", err)
//	}
//
//	return output.UTC(), nil
//}

// Sort into WEEKDAY (1-5), SAT (6) or SUN (0)
func getDayGroup(dateTime time.Time) string {
	var day string
	if dateTime.Weekday() < 6 && dateTime.Weekday() > 0 {
		day = Weekday
	} else if dateTime.Weekday() == 6 {
		day = Saturday
	} else if dateTime.Weekday() == 0 {
		day = Sunday
	}
	return day
}

// Build slice of time.Time using interval of time.Duration
func newTimeSlice(start, end time.Time, interval time.Duration) []time.Time {
	t := []time.Time{}
	delta := end.Sub(start)
	timeItems := delta / interval

	// t := []time.Time{start, n+1 * interval, ... , len(timeItems) * interval, end}
	t = append(t, start)
	for i := 0; i < int(timeItems); i++ {
		n := time.Duration(i + 1)
		j := start.Add(n * interval)
		t = append(t, j)
	}
	t = append(t, end)

	return t
}

// Convert []time.Time to []String using layout
func sliceTimeToString(timeSlice []time.Time, layout string) []string {
	stringSlice := []string{}

	for i := 0; i < len(timeSlice); i++ {
		s := timeSlice[i].Format(layout)
		stringSlice = append(stringSlice, s)
	}

	return stringSlice
}

// Check if ServiceMetrics Days types (e.g. WEEK/SAT/SUNDAY) are represented in date range
func getDayTypes(start, end time.Time) ([]string, error) {

	// Get time delta between start and end in days. Min 1 day forced
	delta := end.Sub(start)
	if delta < 24*time.Hour {
		delta = 24 * time.Hour
	}
	timeItems := delta / (24 * time.Hour)
	daytypeMap := map[string]bool{
		"WEEKDAY":  false,
		"SATURDAY": false,
		"SUNDAY":   false,
	}

	// Loop over dates, check day type, flip the map bool if found and break if all keys true
	t := start
	for i := 0; i < int(timeItems) && (!daytypeMap["WEEKDAY"] || !daytypeMap["SATURDAY"] || !daytypeMap["SUNDAY"]); i++ {
		j := getDayGroup(t)
		value, ok := daytypeMap[j]
		if ok && !value {
			daytypeMap[j] = true
		}

		t = t.Add(24 * time.Hour)
	}

	dayList := []string{}
	for k, v := range daytypeMap {
		if v {
			dayList = append(dayList, k)
		}
	}
	sort.Strings(dayList)

	return dayList, nil
}

// Used when PostMetrics returns a valid response which doesn't contain any services
// Expected when the submitted query doesn't match any journeys
type NoMatchingServicesError struct {
	InitialRequest InitialRequest
}

func (n NoMatchingServicesError) Error() string {
	return fmt.Sprintf("no matching services for query %+v", n.InitialRequest)
}

// Process new metrics POST request, returning RIDs which are added to database
// Accepts input of any HSPGetter function. Facilitate unit testing which doesn't make
// requests to the external HSP API service
func PostMetrics(post InitialRequest, hspGetter HSPGetter, hspConfig HspConfig, ctx context.Context) ([]int64, error) {
	ctx, span := util.StartSpan(ctx)
	defer span.End()

	// Break initial req into batches which can be submitted to the ServiceMetrics HSP API
	batchRequests, err := post.Split(ctx)
	if err != nil {
		return nil, fmt.Errorf("%q", err)
	}
	if len(batchRequests) == 0 {
		err = util.NewErrorWithTrace(span, fmt.Errorf("no requests returned by initialRequest.Split for %+v", post))
		return nil, err
	}

	// Client with exp retry, using zerolog Logger
	client := newRetryClient()

	// Limit to 4 concurrent requests using semaphore channel
	sem := make(chan struct{}, 4)

	// Store parsed responses in an unbuffered channel and close using wait group
	parseChan := make(chan struct {
		stationChan []db.AddStationParams
		serviceChan []db.AddServiceParams
		runChan     []db.AddRunParams
	})
	wg := sync.WaitGroup{}

	// Used for sections which must be ran sequentially in goroutine
	for i, req := range batchRequests {

		// Block if buffered channel full
		sem <- struct{}{}

		wg.Add(1)
		go func(req MetricsRequest, i int, ctx context.Context) error {

			defer wg.Done()

			// Where hspGetter is GetHSPResponse or a mock
			untypedResponse, err := hspGetter(client, hspConfig, req, ctx)
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("PostMetrics: %w", err))
				return err
			}
			response := untypedResponse.(*MetricsResponse)
			// Remove semaphore block, request completed
			<-sem

			stations, services, runs, err := response.Parse(ctx)
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("PostMetrics: %w", err))
				return err
			}

			// Add to parseChan
			batch := struct {
				stationChan []db.AddStationParams
				serviceChan []db.AddServiceParams
				runChan     []db.AddRunParams
			}{stations, services, runs}
			parseChan <- batch

			return nil
		}(req, i, ctx)
	}

	// Close channels once Wait Group has completed. Ends for loop below
	go func() {
		wg.Wait()
		close(parseChan)
	}()

	// Empty channels into db and sorted rid slices
	// ctx := context.Background()
	ridSlice := []int64{}
	for i := range parseChan {
		for _, stationChanItem := range i.stationChan {
			_, err := db.New(db.Conn).AddStation(ctx, stationChanItem)
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("stations insert failed: %w", err))
				return nil, err
			}
		}
		for _, serviceChanItem := range i.serviceChan {
			err := db.New(db.Conn).AddService(ctx, serviceChanItem)
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("service insert failed: %w", err))
				return nil, err
			}
		}
		for _, runChanItem := range i.runChan {
			ridSlice = append(ridSlice, runChanItem.Rid)
			err := db.New(db.Conn).AddRun(ctx, runChanItem)
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("run insert failed: %w", err))
				return nil, err
			}
		}
	}
	sort.Slice(ridSlice, func(i, j int) bool { return ridSlice[i] < ridSlice[j] })

	// No services are returned return error
	if len(ridSlice) == 0 {
		err := util.NewErrorWithTrace(span, NoMatchingServicesError{InitialRequest: post})
		return nil, err
	}
	return ridSlice, nil
}

// Convert strings of time + rid to sql.NullTime
func NullTime(ridString, timeString string) (sql.NullTime, error) {
	var nulltime sql.NullTime

	timeParsed, err := time.Parse(timeRefDatetime, ridString+timeString)
	if err != nil {
		if timeString != "" {
			return nulltime, fmt.Errorf("NullTime: failed to parse time string: %w", err)
		}
	} else {
		nulltime = sql.NullTime{
			Time:  timeParsed,
			Valid: true,
		}
	}

	return nulltime, nil
}

// Convert strings of int to sql.NullInt32
func NullInt(intString string) (sql.NullInt32, error) {
	var nullInt sql.NullInt32
	i, err := strconv.Atoi(intString)
	if err != nil {
		if intString != "" {
			return nullInt, fmt.Errorf("failed to convert string to int")
		}
	} else {
		nullInt = sql.NullInt32{
			Int32: int32(i),
			Valid: true,
		}
	}

	return nullInt, nil
}

// Used to check for new RIDs which have not yet been ran through PostDetails()
func NewDetails(ridArray []int64, ctx context.Context) (ridsNew []int64, err error) {
	_, span := util.StartSpan(ctx)
	defer span.End()

	// Get span and trace IDs to include in logs
	spanCtx := span.SpanContext()
	traceID := spanCtx.TraceID().String()
	spanID := spanCtx.SpanID().String()

	// Pull existing details RIDs and binary search for given rids
	ridsExisting, err := db.New(db.Conn).ListDetailsUniqueRIDs(context.Background())
	if err != nil {
		err = util.NewErrorWithTrace(span, fmt.Errorf("failed to retrieve rids from db: %w", err))
		return ridsNew, err
	}
	for _, rid := range ridArray {
		i := sort.Search(len(ridsExisting), func(i int) bool { return ridsExisting[i] >= rid })
		if i < len(ridsExisting) && ridsExisting[i] == rid {
			log.Debug().
				Int64("existing_rid", rid).
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Msg("Found existing rid")
		} else {
			ridsNew = append(ridsNew, rid)
			log.Debug().
				Int64("new_rid", rid).
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Msg("Found new rid to search")
		}
	}

	if len(ridsNew) == 0 {
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msg("No new rid in request")
	}

	return ridsNew, nil
}

// Make ServiceDetails Post request
func PostDetails(rids []int64, hspGetter HSPGetter, hspConfig HspConfig, ctx context.Context) ([]DetailsResponse, error) {
	ctx, span := util.StartSpan(ctx)
	defer span.End()

	ridsString := ""
	for _, v := range rids {
		if len(ridsString) > 0 {
			ridsString += ","
		}
		ridsString += strconv.FormatInt(v, 10)
	}
	span.SetAttributes(attribute.Int64Slice("rids", rids))

	detailsResponseSlice := []DetailsResponse{}

	// Client with exp retry
	client := newRetryClient()

	// Limit to 3 concurrent requests using semaphore channel
	sem := make(chan struct{}, 3)

	// Store parsed responses in an unbuffered channel and close using wait group
	parseChan := make(chan struct {
		detailsChan []db.AddDetailParams
		stationChan []db.AddStationParams
	})
	wg := sync.WaitGroup{}

	// Used for sections which must be ran sequentially in goroutine
	for i, v := range rids {

		// Block if buffered channel full
		sem <- struct{}{}

		wg.Add(1)
		go func(v int64, i int, ctx context.Context) error {
			defer wg.Done()

			// Convert rid int to string
			req := DetailsRequest{Rid: strconv.Itoa(int(v))}
			untypedResponse, err := hspGetter(client, hspConfig, req, ctx)
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("PostDetails: %w", err))
				return err
			}
			response := untypedResponse.(*DetailsResponse)
			detailsResponseSlice = append(detailsResponseSlice, *response)
			// Remove semaphore block, request completed
			<-sem

			details, stations, err := response.Parse()
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("PostDetails: %w", err))
				return err
			}

			// Add to parseChan
			batch := struct {
				detailsChan []db.AddDetailParams
				stationChan []db.AddStationParams
			}{details, stations}
			parseChan <- batch

			return nil
		}(v, i, ctx)
	}

	// Close channels once Wait Group has completed. Ends for loop below
	go func() {
		wg.Wait()
		close(parseChan)
	}()

	// Empty channels into db
	// ctx := context.Background()
	for i := range parseChan {
		for _, stationChanItem := range i.stationChan {
			_, err := db.New(db.Conn).AddStation(ctx, stationChanItem)
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("stations insert failed: %w", err))
				return []DetailsResponse{}, err
			}
		}
		for _, j := range i.detailsChan {
			err := db.New(db.Conn).AddDetail(ctx, j)
			if err != nil {
				err = util.NewErrorWithTrace(span, fmt.Errorf("stations insert failed: %w", err))
				return []DetailsResponse{}, err
			}
		}
	}

	return detailsResponseSlice, nil
}

// Client with retry and exponentional backoff, using zerolog logger without levels
func newRetryClient() *http.Client {
	retryClient := retryablehttp.NewClient()
	retryClient.RetryMax = 10
	retryClient.RetryWaitMin = 10 * time.Second
	retryClient.RetryWaitMax = 240 * time.Second
	retryClient.Logger = log.Logger
	client := retryClient.StandardClient()

	return client
}
