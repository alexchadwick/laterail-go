// JWT access token middleware for authenticated gin endpoints
package api

import (
	"context"
	"errors"
	"net/http"
	"strings"
	"time"

	"laterail-api/util"

	"github.com/MicahParks/keyfunc"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel/codes"
)

func jwtValidate(jwksURL string) gin.HandlerFunc {
	// Do some initialization logic here
	return func(c *gin.Context) {
		_, span := util.StartSpan(c.Request.Context())
		defer span.End()

		// Get the bearer token
		auth := c.Request.Header.Get("Authorization")
		if auth == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Unauthorized": "Missing Authorization header value"})
		}
		bearer := strings.TrimPrefix(auth, "Bearer ")
		if bearer == auth {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Unauthorized": "Missing Authorization header value"})
		}
		bearer = strings.TrimSpace(bearer)

		// Get Json Web Key Set from Auth0 to parse and verify tokens
		// Steals wholesale from recommended options reference: https://github.com/MicahParks/keyfunc/blob/master/examples/recommended_options/main.go
		// Create a context that, when cancelled, ends the JWKS background refresh goroutine
		ctx, cancel := context.WithCancel(context.Background())

		// Refresh the JWKS when a JWT signed by an unknown KID is found or at the specified interval. Rate limit these refreshes
		// Timeout the initial JWKS refresh request after 10 seconds. This timeout is also used to create the initial context.Context for keyfunc.Get
		options := keyfunc.Options{
			Ctx: ctx,
			RefreshErrorHandler: func(err error) {
				log.Info().
					Str("Error", err.Error()).
					Msg("Error during JWKS refresh")
			},
			RefreshInterval:   time.Hour,
			RefreshRateLimit:  time.Minute * 5,
			RefreshTimeout:    time.Second * 10,
			RefreshUnknownKID: true,
		}

		// Create the JWKS from the resource at the given URL
		jwks, err := keyfunc.Get(jwksURL, options)
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Failed to create JWKS from remote URL": err.Error()})
		}

		// Parse the JWT and check if the token is valid
		var token *jwt.Token
		if token, err = jwt.Parse(bearer, jwks.Keyfunc); err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Failed to parse the JWT bearer token": err.Error()})
		}

		if !token.Valid {
			err = errors.New("Invalid JWT bearer token")
			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Token valid": token.Valid})
		}

		cancel()
		span.End() // Close here to prevent span timer running until end of request
		c.Next()
	}
}
