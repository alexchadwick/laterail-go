package api

import (
	"context"
	"encoding/json"
	"time"

	"laterail-api/db"
	"laterail-api/mongodb"
	"laterail-api/util"

	"github.com/didip/tollbooth/v6"
	"github.com/didip/tollbooth/v6/limiter"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.opentelemetry.io/otel/attribute"
)

type Env struct {
	Stations  mongodb.StationModel
	JwksURL   string
	HspConfig HspConfig
	LogLevel  string
}

// Setup API server
func SetupRouter(ctx context.Context, env Env) *gin.Engine {
	// gin.SetMode(env.LogLevel)
	router := gin.New()

	// Global config and middleware
	router.Use(gin.Recovery())
	router.Use(cors.Default())                  // Access-Control-Allow-Origin: * for testing
	router.Use(otelgin.Middleware("my-server")) //TODO: unique identity for each instance?
	router.Use(requestID())
	// Add vars to context
	router.Use(addHspConfig(env.HspConfig))
	router.Use(util.MiddlewareLogger())
	// Add prometheus metrics
	metrics := prometheusMetrics(util.InitMetrics(ctx))
	router.Use(metrics)

	// Initial API release, no authn or rate limits
	v0 := router.Group("/") // Unauthn
	{
		v0.POST("/submit", PostInitialRequest)
		v0.GET("/runs", GetRidsHandler)
		v0.GET("/details", GetDetailsHandler)
		v0.GET("/stations", env.Stations.ListStationsAPI)
		v0.GET("/metrics", gin.WrapH(promhttp.Handler()))
	}

	v1 := router.Group("/v1")
	{
		// limiter with expirable token buckets. float requests per second + request burst limit, tokens expire 1 hour after set
		defaultLimiter := tollbooth.NewLimiter(1, &limiter.ExpirableOptions{DefaultExpirationTTL: time.Hour}).
			SetIPLookups([]string{"RemoteAddr", "X-Forwarded-For", "X-Real-IP"}).
			SetBurst(10).
			SetMessage("You have reached maximum request limit. Sign-up to access a higher limit. Anonymous users have a burst limit of 10 non-search requests followed by 1 non-search request per second")
		searchLimiter := tollbooth.NewLimiter(float64(1)/1800, &limiter.ExpirableOptions{DefaultExpirationTTL: time.Hour}).
			SetIPLookups([]string{"RemoteAddr", "X-Forwarded-For", "X-Real-IP"}).
			SetBurst(5).
			SetMessage("You have reached maximum request limit. Sign-up to access a higher limit. Anonymous users have a burst limit of 5 search requests followed by 1 search request per 30 mins")

		v1.Use(LimitHandler(defaultLimiter))

		// Anonymous requests. Limited endpoints, extra rate limit for /submit
		public := v1.Group("/public")
		{
			public.POST("/submit", LimitHandler(searchLimiter), PostInitialRequest)
			public.GET("/runs", GetRidsHandler)
			public.GET("/details", GetDetailsHandler)
			public.GET("/stations", env.Stations.ListStationsAPI)
		}

		// Authn requests. All endpoints
		authn := v1.Group("/")
		{
			authn.Use(jwtValidate(env.JwksURL))
			authn.POST("/submit", PostInitialRequest)
			authn.GET("/runs", GetRidsHandler)
			authn.GET("/details", GetDetailsHandler)
			authn.GET("/stations", env.Stations.ListStationsAPI)
		}

	}

	return router
}

func addHspConfig(hspConfig HspConfig) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("hspConfig", hspConfig)
		c.Next()
	}
}

func requestID() gin.HandlerFunc {
	return func(c *gin.Context) {
		rid := c.Request.Header
		if rid.Get("X-Request-ID") == "" {
			rid.Set("X-Request-ID", uuid.NewString())
		}
		c.Next()
	}
}

// Gather prometheus metrics for requests
func prometheusMetrics(
	counters []*prometheus.CounterVec,
	summaries map[string]*prometheus.SummaryVec,
	histograms []*prometheus.HistogramVec) gin.HandlerFunc {

	return func(ctx *gin.Context) {
		// Get time for duration later
		start := time.Now()

		// Increment all counters
		for _, c := range counters {
			c.With(prometheus.Labels{
				util.Path: ctx.Request.URL.Path,
			}).Inc()
		}

		// Run other pending handlers then return
		ctx.Next()

		duration := float64(time.Since(start)) / float64(time.Second)

		for _, h := range histograms {
			h.With(prometheus.Labels{
				util.Path: ctx.Request.URL.Path,
			}).Observe(duration)
		}

		// Existing summaries require specific input. Referenced by map key
		summaries["laterail_summary_request_duration_seconds"].With(prometheus.Labels{
			util.Path: ctx.Request.URL.Path,
		}).Observe(duration)

		summaries["laterail_response_size_bytes"].With(prometheus.Labels{
			util.Path: ctx.Request.URL.Path,
		}).Observe(float64(ctx.Writer.Size()))
	}
}

// GET all run IDs in database
func GetRidsHandler(c *gin.Context) {
	_, span := util.StartSpan(c.Request.Context())
	defer span.End()

	rids, err := db.New(db.Conn).ListRuns(c)
	if err != nil {
		c.JSON(500, gin.H{"error": "unable to return runs"})
	} else {
		c.JSON(200, rids)
	}
}

// GET all details in database
func GetDetailsHandler(c *gin.Context) {
	_, span := util.StartSpan(c.Request.Context())
	defer span.End()

	rids, err := db.New(db.Conn).ListDetails(c)
	if err != nil {
		msg := "unable to return details"
		util.NewErrorLogWithTrace(err, span, msg)
		c.JSON(500, gin.H{"error": msg})
	} else {
		c.JSON(200, rids)
	}
}

// POST new submitted initial request
func PostInitialRequest(c *gin.Context) {
	ctx, span := util.StartSpan(c.Request.Context())
	defer span.End()

	var newRequest InitialRequest
	if err := c.BindJSON(&newRequest); err != nil {
		msg := "Could not map body to new request"
		util.NewErrorLogWithTrace(err, span, msg)
		c.JSON(400, gin.H{"invalid request": msg})
		return
	}

	body, _ := json.Marshal(newRequest)
	span.SetAttributes(attribute.String("newRequest", string(body)))

	// Get HSP API creds from gin context. Requires type assertion
	hspConfig, ok := c.MustGet("hspConfig").(HspConfig)
	if !ok {
		c.JSON(500, gin.H{"error": "Failed to process new metrics request. PostMetrics()", "details": "HSP API credentials not found"})
	}

	// Break request into batches, submit to HSP Metrics, process and insert into database
	// Passing in OpenTelemetry context to build trace of dependent spans
	ridArray, err := PostMetrics(newRequest, GetHspResponse, hspConfig, ctx)
	if err != nil {

		// If PostMetrics returns no services then halt and return descriptive message
		_, ok := err.(NoMatchingServicesError)
		if ok {
			c.JSON(200, gin.H{"info": err.Error()})
		} else {
			msg := "Failed to process new metrics request. PostMetrics()"
			util.NewErrorLogWithTrace(err, span, msg)
			c.JSON(500, gin.H{"error": msg})
		}
		return
	}

	// Check db for existing details info. Return db entries and list of new rids to get
	new, err := NewDetails(ridArray, ctx)
	if err != nil {
		msg := "Failed to process new metrics request. NewDetails()"
		util.NewErrorLogWithTrace(err, span, msg)
		c.JSON(500, gin.H{"error": msg})
		return
	}

	// Submit rids to HSP Details, process and insert into database
	_, err = PostDetails(new, GetHspResponse, hspConfig, ctx)
	if err != nil {
		msg := "Failed to process new metrics request. PostDetails()"
		util.NewErrorLogWithTrace(err, span, msg)
		c.JSON(500, gin.H{"error": msg})
		return
	}

	// Get details of all requested rids - now new RIDs have been added to db
	detailsTotal, err := db.New(db.Conn).ListDetailsByRIDs(c, ridArray)
	if err != nil {
		msg := "Failed to process new metrics request. ListDetailsByRIDs()"
		util.NewErrorLogWithTrace(err, span, msg)
		c.JSON(500, gin.H{"error": msg})
		return
	}

	c.JSON(200, gin.H{"request": newRequest, "result": detailsTotal})
}
