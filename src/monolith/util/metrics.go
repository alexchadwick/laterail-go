package util

import (
	"context"
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	Path = "path"
)

type Metric struct {
	Name       string
	Help       string
	Objectives map[float64]float64
	Buckets    []float64
	Type       string
}

var defaultMetrics = []Metric{
	{
		Name: "laterail_endpoint_requests_total",
		Help: "Total number of requests processed",
		Type: "counter",
	},
	{
		Name:       "laterail_summary_request_duration_seconds",
		Help:       "Latency for processed requests",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		Type:       "summary",
	},
	{
		Name:    "laterail_histogram_request_duration_seconds",
		Help:    "Latency for processed requests",
		Buckets: []float64{1, 5, 10, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360},
		Type:    "histogram",
	},
	{
		Name:       "laterail_response_size_bytes",
		Help:       "Size of response for processed requests (bytes)",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		Type:       "summary",
	},
}

// Creates and registers new prometheus metrics
func InitMetrics(ctx context.Context) ([]*prometheus.CounterVec, map[string]*prometheus.SummaryVec, []*prometheus.HistogramVec) {
	_, span := StartSpan(ctx)
	defer span.End()

	var c []*prometheus.CounterVec
	var s = map[string]*prometheus.SummaryVec{}
	var h []*prometheus.HistogramVec

	for _, v := range defaultMetrics {
		if v.Type == "counter" {
			counter := prometheus.NewCounterVec(
				prometheus.CounterOpts{
					Name: v.Name,
					Help: v.Help,
				},
				[]string{Path})
			c = append(c, counter)
			if err := prometheus.Register(counter); err != nil {
				NewErrorLogWithTrace(err, span, fmt.Sprintf("Failed to register metric: %s", v.Name))
			}
		} else if v.Type == "summary" {
			summary := prometheus.NewSummaryVec(
				prometheus.SummaryOpts{
					Name:       v.Name,
					Help:       v.Help,
					Objectives: v.Objectives,
				},
				[]string{Path})
			s[v.Name] = summary
			if err := prometheus.Register(summary); err != nil {
				NewErrorLogWithTrace(err, span, fmt.Sprintf("Failed to register metric: %s", v.Name))
			}
		} else if v.Type == "histogram" {
			histogram := prometheus.NewHistogramVec(
				prometheus.HistogramOpts{
					Name:    v.Name,
					Help:    v.Help,
					Buckets: v.Buckets,
				},
				[]string{Path})
			h = append(h, histogram)
			if err := prometheus.Register(histogram); err != nil {
				NewErrorLogWithTrace(err, span, fmt.Sprintf("Failed to register metric: %s", v.Name))
			}
		} else {
			NewErrorLogWithTrace(fmt.Errorf("Failed to register metric %s", v.Name), span, fmt.Sprintf("Unknown Metric type %s", v.Type))
		}
	}

	return c, s, h
}
