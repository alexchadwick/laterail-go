# Summary

Quick setup and running notes for Golang API component

# Install external packages

```
PWD=src/api
go mod tidy
```

# Upgrade dependencies

```
# List all upgrades
go list -u -m all

# Upgrade
go get -u ./...
```

# Tests

```
PWD=src/api
# Run all tests
go test -v ./...

# Run named test
go test -v -run TestDetailsResponse_Station

# Get test coverage
go test --coverprofile=coverage.out ./... && go tool cover -func=coverage.out

# Run benchmark tests
go test -bench ./...

## Empty test cache
go clean -testcache
```

# vscode config

Add go.testEnvFile settings.json value to enable vscode-go "debug test" feature:

```
PWD=~/.config/Code/User/settings.json
{
  "go.testEnvFile": "${workspaceFolder}/deploy/local/.env",
}
```
