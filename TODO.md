# Ongoing work
- [Epic for monolith (on AKS) -> services (on EKS)](https://gitlab.com/alexchadwick/laterail-go/-/issues/46)

# Issues
## stage specific pipeline values fix
- Currently hardcodes a mixture of `dev` or `test` as the environment level. Affects:
  - ArgoCD: up/down pipelines = `test`
  - `ENV_CONTEXT`: First EKS stage should be `test`, current = `dev`
    - Create new AWS Access/secret env vars

## Why doesn't laterail:0.0.3 helm work in argocd appset?
- First one made with ci

## AWS permissions (fixed?)
- eks tf module is using the identity which creates the resources as sole owner
  - Not appropriate when pipeline creates it
- Check if the kms key permissions are fixed
  - Can you run a tfplan after gitlab-ci has ran apply

## CoreDNS addon
`module.eks.aws_eks_addon.this["coredns"]: Creation complete after 15m9s [id=dev-eu-west-2:coredns]`
- Quicker to install with a helm chart?

## Terraform
- Add MongoDB Atlas databases
  - Using free shared instance for dev
    - Cannot use PrivateLink with Shared tier. Needs Public IP allow listing
  - Use serverless for test and prod + PrivateLink
    - Deploy and destroy via TF in pipeline (charged for storage). Backup to s3 before destroy, restore on create
- Review [CockroachDB Terraform provider Preview](https://github.com/cockroachdb/terraform-provider-cockroach)
  - [Serverless example](https://github.com/cockroachdb/terraform-provider-cockroach/blob/main/examples/workflows/cockroach_serverless_cluster/main.tf)
  - No requirement to run as ephemeral, unless we outgrow the serverless free tier

### EKS
- ALB Controller
  - Configure ALB healthchecks on pods. [Ref](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/deploy/pod_readiness_gate/)
  - Configure [Ingress Groups](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/guide/ingress/annotations/#ingressgroup)
  - Review related limits and resource usage
  - Add WAF
- Misc networking things to review:
  - [Shouldn't run the EKS nodes on the cluster subnets](https://aws.github.io/aws-eks-best-practices/networking/subnets/#vpc-configurations)
- Use SGs per pods
  - [docs](https://aws.github.io/aws-eks-best-practices/networking/sgpp/)
- Assign IP prefixes to ENIs of EKS node groups
  - [docs](https://aws.github.io/aws-eks-best-practices/networking/prefix-mode/)
  - [Only supported for instance types where `IsTrunkingCompatible==true`](https://github.com/aws/amazon-vpc-resource-controller-k8s/blob/release/pkg/aws/vpc/limits.go#L32)
  - Increase number of IPs per ENI (limited no. of ENI per node) => more pods per node
- Create admin and dev level users, with their corresponding IAM roles, policies and k8s RBAC integration
  - [AWS IAM Authenticator for Kubernetes](https://github.com/kubernetes-sigs/aws-iam-authenticator)
    - RBAC for IAM identities using `aws-auth` configmap
  - [aws docs](https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html)
- Use [Pod Security Admission controller](https://kubernetes.io/docs/concepts/security/pod-security-admission/)
  - Replaces deprecated PodSecurityPolicy

## Helm
- Add better labels via chart templates
  - [Examples](https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/)
  - [More](https://kubernetes.io/docs/reference/labels-annotations-taints/#topologykubernetesioregion)
- Add [OpenTelemetry](https://opentelemetry.io/) instrumentation
  - [Use otel to get RabbitMQ stats](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/receiver/rabbitmqreceiver)
  - [Use otel receiver for MongoDB Atlas](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/receiver/mongodbatlasreceiver)

## App
### All backend
- Add service readiness and liveness endpoints `/health/livez|readyz`
  - Liveness: If false, restart container
    - failureThreshold > 1 (intermittent failures can be tolerated before restarting)
    - Wait for connections to db and message broker
  - Readiness: Container ready to accept traffic
    - failureThreshold = 1
    - Return false after SIGTERM, when app is shutting down, to prevent new requests as it's closing connections to db and message broker
    - Worker availability is managed my message broker QoS
- Review observability
  - Am I managing errors correctly?
    - [ref](https://dave.cheney.net/2016/04/27/dont-just-check-errors-handle-them-gracefully)
    - Should I be logging errors and bubbling them up? Probably No
  - Include service version info in start up log
- Add [OpenTelemetry](https://opentelemetry.io/) instrumentation
  - [Use otel instrumentation for pgx](https://github.com/exaring/otelpgx)
- Remove `go-migrate` and `dockertest`
  - Replace with a makefile target which prepares a docker psql container and runs migrations
  - These packages have a lot of dependencies and are only used in tests
    - And tests are currently neglected
- Currently use a global trace provider and logger
  - Best practice is to pass these as dependencies, but the current approach is much simpler
  - Look for opportunities to refactor to pass these as dependencies
- How to apply user rate limiting for a distributed service? On the ingress, CDN or backend (using a shared datastore)?
- Create an architecture diagram
  - Getting into the realm of tough to quickly explain/remember
  - Use something like [diagrams](https://github.com/mingrammer/diagrams) to create a high level summary diagram
- Define application service specs for documentation
  - e.g. [good example](https://developer.hashicorp.com/consul/tutorials/microservices/kubernetes-scope-microservice#example-worksheet)
- Investigate curcuit breaker pattern
  - Mainly thinking about Consumer service, which is dependent on external HSP API
  - [hystrix-go](https://github.com/afex/hystrix-go)

### Consumer service:
  - If a ServiceMetrics search 502s (exceeding 1min timeout on HSP API) then acknowledge the message and create 2 new messages, which cut the timeframe in half
    - This should allow the search to complete, without additional retries
    - Currently retries with backoff, does work but can take a long time (backoff quickly grows)
  - RabbitMQ can deliver the same message more than once. Need a way to make Consumer service idempotent
    - Currently re-process duplicate messages and rely on relational db unique indexes to prevent duplicate value inserts
    - Ideally want to skip processing duplicate messages, because HSP ServiceMetrics requests take 30s+ to complete
    - Book "Cloud Native Go" (p289) suggests using a transaction ID to make scalar operations idempoent
      - Include a transaction ID in message and use to check if it's already been processed
      - How best to store these transactional IDs? Only needed until parent request has completed/errored
        - Redis? Quick
        - Postgres/CockroachDB? Already setup. New `task` style table
  - A `/track` request which contains one or more unknown requestIDs will return a single "unknown request_id" message only
    - Should check all requests and return better info
    - May include mixture of known and unknown requestIDs
    - Currently doesn't affect frontend, which only searches for one requestID at a time
  - Remove mongodb collection schema migration actions. Will be handled outside of apps
  - Review `filterDetailsMessages()`. There will be much better ways to send a scaling proportion of messages to frontend

### Events service:
  - Test with Nginx reverse proxy/load balancers with additional config
    - (Should use http/2)[https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events]
    - Configure `proxy_read_timeout` [ref](https://stackoverflow.com/questions/21630509/server-sent-events-connection-timeout-on-node-js-via-nginx)
  - What if the frontend misses some SSEs during reconnect? All messages are currently de-queued/deleted
    - Return message to queue?
      - Only return the final message to queue? It's the most important
        - Notifies the frontend that the stream is closed and no more events will be sent
    - On connection error or message timeout duration, re-run Consumer track request, then get SSE as normal (if required)?

### Producer service:
  - Doesn't validate the stations in the request
    - Assumes react frontend will prevent this. N/A to API only queries

### Web react frontend
  - Handle responses with no results, "empty"
    - Displayed message should state that only direct train journeys are supported
  - When you refresh the track page, the `metrics_done` value used by the progress loading bar is reset to 0
    - Then updated by the next received SSE
    - Should get the current value from the tracker response
  - Filter Results accordian
    - Options to show only Red+Orange or Red
  - New loading page flow:
    - Publisher request (? Rapid)
      - Move to loading screen after Submit button click
        - Display error message for non-202 responses
        - Currently waits for 202 response before showing loading screen
          - So producer service errors which return 400/500 are not shown to user. Looks like nothing happens
      - Update text desciption
    - Tracker request (Slow if request retries or no Consumer workers available)
      - Update text desciption
        - "Fetching your journeys" -> "Waiting for available worker"
        - Only able to make 4 concurrent requests to the HSP API
    - Metrics requests (Very slow, 30+ seconds per ServiceMetrics request)
      - Update text description, take from Prodcuer loading
        - Request is active. Waiting on HSP API
        - Add message which estimates time remaining
          - Make time estimate (~30s per metrics query)
    - Details requests
      - Don't show "Waiting..." if:
        - tracker shows complete (refresh)
  - Add 404 page using switch statement on `App.js` Routes
  - Results accordian requires horizontal scrolling to view Details column on mobile
  - SSE connection doesn't reconnect when Events server WriteTimeOut value is met
    - Have a workaround which checks the error and attempts reconnect
    - Investigate replacing with [popular tool](https://github.com/Yaffle/EventSource)
      - Depends on "Last-Event-ID" event field, which would require me to queue events on the Events service. Currently throw missed events away
    - Could also use `react-query` to handle SSEs
  - Demo mockEvents misbehaves if you run a second search without refreshing
    - Actual events don't have this bug
  - Consider moving `/react/public/demo` and `/react/src/services/events.js` to blob storage and downloading if in Demo mode to reduce size by ~1MiB
  - Add a "About this project" page
  - Add "Recent searches" and "Example searches" to Home page
    - Big search results examples
      - http://localhost:3000/results/ad336352-9472-46cf-be4e-30b6f45e53bc
      - http://localhost:3000/results/e95d4c15-fdc3-4d38-8f3a-ed723612f32d

### Potential additional services
  - Controller service: Single point of ingress for all incoming public requests (via Nginx proxy/API gateway/Kong)
    - Investigate caching on a CDN. E.g. consumer `/stations` and completed `/track` requests
  - Update stations list: Pull newest station details from scotrail public API. Store data, history of actions in nosql db/blob storage and serve to frontend
  - Overall service health status:
    - Show if services are unhealthy
      - E.g. if rabbitmq, datastores or instances of a service are down
    - Consume on frontend, disabling search and display warning
    - Use Consul/service mesh health checks?
      - To aggregate health status of all instances of a laterail service and external services (rabbitmq, mongodb, cockroachdb)
  - Ticket scanner: Search related journeys using new Aztec ticket barcode. Refs:
    - [Awesome blog post](https://web.archive.org/web/20230214044302/https://eta.st/2023/01/31/rail-tickets.html)
    - [example project](https://git.eta.st/eta/rsp6-decoder)
  - Journey planner: Enable searches for journeys which include transfers
    - Currently only supports direct journeys between two stations
    - Journey planner APIs are supplied by National Rail, but there's a charge per request

# Golang Refactor
## Smaller tasks
- Rename `internal` to `pkg`. Needs it's own PR
- Use the [`string` struct json tag directive](https://pkg.go.dev/encoding/json#Marshal) to return values of other types as strings
  - Only works on struct fields which have int*, uint*, float* or bool types
- Use json.Decoder instead of json.Unmarshal, where practical
  - Uses less memeory and is faster

### Logging
- Create HTTP server error helper functions
  - See `Let's Go` book, p105
- Consider using [/x/exp/slog](https://pkg.go.dev/golang.org/x/exp/slog)
  - https://go.googlesource.com/proposal/+/master/design/56345-structured-logging.md

### Standardise using CodeReviewComments
- https://github.com/golang/go/wiki/CodeReviewComments#comment-sentences
- https://github.com/golang/go/wiki/CodeReviewComments#contexts
- https://github.com/golang/go/wiki/CodeReviewComments#declaring-empty-slices
- https://github.com/golang/go/wiki/CodeReviewComments#indent-error-flow
- https://github.com/golang/go/wiki/CodeReviewComments#initialisms
- https://github.com/golang/go/wiki/CodeReviewComments#named-result-parameters
- https://github.com/golang/go/wiki/CodeReviewComments#naked-returns
- https://github.com/golang/go/wiki/CodeReviewComments#package-comments
- https://github.com/golang/go/wiki/CodeReviewComments#package-names

## References
- https://peter.bourgon.org/go-best-practices-2016/#conclusion
- https://github.com/golang/go/wiki/CodeReviewComments

# Paused work
## Consul
  - LateRail currently has no/little service to service communication, so not really using Consul
  - Opportunities for use:
    - mTLS between k8s ingress and pods
      - This doesn't apply to AWS Load Balancer controller ingress with ALB, because the TLS termination doesn't happen within the k8s cluster, where a sidecar can manage it
      - Currently only used in Local DEV environment, where nginx ingress controller is used
    - Potential new Composite/API Gateway service within the k8s cluster which is the only point of communication for the frontend, or Follow service (expanded below)
    - Use for encryption and service disovery of connections to RabbitMQ and datastores?
  - Change how Consul k8s laterail resource annotations are managed using the helm value `consul.enabled`
    - This doesn't feel like good practise. Could remove Consul specific references from the laterail charts
      - Replace with generic `annotations` helm values for relevant resource types and add Consul values in env level values files
  - [Minikube requires Consul servers run as root](https://developer.hashicorp.com/consul/tutorials/kubernetes/kubernetes-minikube#create-a-values-file)
    - [Not true for kind](https://developer.hashicorp.com/consul/tutorials/kubernetes/kubernetes-minikube#create-a-values-file)

## Secrets
- Deploy Hashicorp Vault, use as secret service
  - [RabbitMQ credentials](https://www.rabbitmq.com/kubernetes/operator/using-operator.html#vault)
  - Use Hashicorp Cloud Vault
    - Create/Destroy using Terraform
- Opted to just use the [external secrets operator](https://external-secrets.io/latest/) and AWS Secrets

## ArgoCD vs FluxCD vs Helmfile comparison
- Replaced helmfile managed deployment with ArgoCD. See [archived](./deploy/archived/helmfile/)