#!/bin/bash
<<COMMENT
Package Helm charts from local source

Dependencies:
- Helm
- Helm ChartMuseum plugin
COMMENT
set -euo pipefail

# Add Gitlab package hosted Helm repo, force update to overwrite any existing repo of the same name
helm repo add \
  --username $GITLAB_USER \
  --password $GITLAB_ACCESS_TOKEN \
  --force-update \
  laterail https://gitlab.com/api/v4/projects/28689390/packages/helm/laterail-go

# Iterate over all charts in the charts directory, excluding the incomplete todo charts
for CHART_NAME in $(find ../charts/* -maxdepth 0 -type d -name "*" -not -path "../charts/todo*"); do
  helm package $CHART_NAME --destination ../charts
  helm cm-push $CHART_NAME-*.tgz laterail
done

# Remove all packaged charts
rm ../charts/*.tgz

