#!/bin/bash
<< COMMENT
Create kind cluster for local testing
COMMENT
set -euo pipefail

# Create docker network for kind
if ! $(docker network ls | grep kind -q); then docker network create -d bridge kind; fi

# Add cluster
cat <<EOF | kind create cluster --wait 30s --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role:  worker
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraMounts:
  - hostPath: /home/alex/git/laterail-go/deploy/migrations
    containerPath: /mnt/migrations
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
  - containerPort: 15672
    hostPort: 15672
    protocol: TCP
- role:  worker
  extraMounts:
  - hostPath: /home/alex/git/laterail-go/deploy/migrations
    containerPath: /mnt/migrations
- role:  worker
  extraMounts:
  - hostPath: /home/alex/git/laterail-go/deploy/migrations
    containerPath: /mnt/migrations
EOF

# MetalLB install
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.5/config/manifests/metallb-native.yaml --context kind-kind
kubectl wait --for=condition=Ready pods --all --namespace metallb-system --timeout=300s --context kind-kind

# Configure using a subset of the kind bridge network adaptor range (e.g. 172.19.0.1/16)
# Get current range: docker network inspect -f '{{.IPAM.Config}}' kind
KIND_SUBNET=$(docker network inspect kind | jq -r '.[].IPAM.Config[0].Subnet' | sed 's/\.0\.0\/16//g')
cat <<EOF | kubectl apply --context kind-kind -f -
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: laterail
  namespace: metallb-system
spec:
  addresses:
  - ${KIND_SUBNET}.255.100-${KIND_SUBNET}.255.250
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: laterail
  namespace: metallb-system
EOF

# Deploy metrics-server, required by HPA
kubectl apply --namespace kube-system \
  -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.1/components.yaml \
  --context kind-kind
kubectl patch deployment \
  metrics-server \
  --namespace kube-system \
  --type='json' \
  -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/args", "value": [
  "--cert-dir=/tmp",
  "--secure-port=4443",
  "--kubelet-insecure-tls",
  "--kubelet-preferred-address-types=InternalIP",
  "--kubelet-use-node-status-port",
  "--metric-resolution=15s"
]}]'

# Setup argocd w/ ApplicationSet controller
# kubectl create namespace argocd --context kind-kind
# kubectl apply --namespace argocd \
#   -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.8.4/manifests/install.yaml \
#   --context kind-kind
helm repo add argo https://argoproj.github.io/argo-helm
helm install argo-cd argo/argo-cd \
  --create-namespace \
  --version "5.46.8" \
  --namespace argocd -f - <<EOF
fullnameOverride: argocd
configs:
  params:
    applicationsetcontroller.enable.progressive.syncs: true
redis-ha:
  enabled: true
controller:
  replicas: 1
server:
  replicas: 2
  service:
    type: LoadBalancer
repoServer:
  replicas: 2
applicationSet:
  replicas: 2
EOF
kubectl wait --for=condition=Ready pods --all --namespace argocd --timeout=300s --context kind-kind
kubectl port-forward svc/argocd-server --namespace argocd 8080:443 --context kind-kind &> /dev/null &

# Ensure pre-requisite secrets exist
for NS in laterail external-dns; do
  kubectl get namespace/${NS} &> /dev/null || kubectl create namespace $NS
  kubectl get secret/azure-secret-sp -n $NS &> /dev/null || kubectl create secret generic azure-secret-sp \
    --from-literal clientid=${SERVICE_PRINCIPAL_CLIENT_ID} \
    --from-literal clientsecret=${SERVICE_PRINCIPAL_CLIENT_SECRET}\
    --namespace $NS \
    --context kind-kind
done
kubectl get secret/cloudflare -n external-dns &> /dev/null || kubectl create secret generic cloudflare \
  --from-literal API_TOKEN=${CLOUDFLARE_API_TOKEN} \
  --namespace external-dns \
  --context kind-kind

# Wait for argocd secret
until kubectl --namespace argocd get secret --context kind-kind | grep "argocd-initial-admin-secret"; do : ; done
ARGO_PASS=$(kubectl --namespace argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)

# Update argocd "in-cluster" cluster. Creating secret, adding labels
argocd login localhost:8080 \
  --username admin \
  --password $ARGO_PASS \
  --insecure # Selfsigned TLS

argocd cluster add $(kubectl config current-context) \
  --name dev \
  --label env=dev \
  --label distribution=kind \
  --in-cluster \
  --upsert \
  --yes

# Deploy ApplicationSet
kubectl apply -n argocd -f ../argocd/root.yaml --context kind-kind
echo -e "$ARGO_PASS https://localhost:8080\n"
