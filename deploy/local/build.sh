#!/bin/bash
set -e

REPO_ROOT=$(git rev-parse --show-toplevel)

# Build container images
# cd "$REPO_ROOT/../../src/api"
# docker build . -t registry.gitlab.com/alexchadwick/laterail-go/api:dev

cd "$REPO_ROOT/src/services"
buildah bud -t registry.gitlab.com/alexchadwick/laterail-go/consumer:alpha -f Containerfile.consumer
buildah bud -t registry.gitlab.com/alexchadwick/laterail-go/producer:alpha -f Containerfile.producer
buildah bud -t registry.gitlab.com/alexchadwick/laterail-go/events:alpha -f Containerfile.events

cd "$REPO_ROOT/react"
npm install
npm run build
docker build . -t registry.gitlab.com/alexchadwick/laterail-go/web:alpha

# Push to gitlab
# docker push registry.gitlab.com/alexchadwick/laterail-go/api:dev
buildah push registry.gitlab.com/alexchadwick/laterail-go/web:alpha
buildah push registry.gitlab.com/alexchadwick/laterail-go/consumer:alpha
buildah push registry.gitlab.com/alexchadwick/laterail-go/producer:alpha
buildah push registry.gitlab.com/alexchadwick/laterail-go/events:alpha