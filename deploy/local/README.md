# Summary

Resources for local deployment. Includes:

- Kind deployment: Full k8s deployment using local workstation
- docker compose: Limited deployment, intended for quick testing during development

# Kind deployment

- Create and source `/deploy/local/.env` using `/deploy/local/example-env` as a reference
- Run `deploy/local/build.sh`. Updating the `alpha` container image tags
- Run `/deploy/local/kind.sh`. Deploys the kind resources, bootstraps ArgoCD and adds public DNS records in CloudFlare, with private IPv4 values to local services
- Access ArgoCD on [https://localhost:8080](https://localhost:8080). Username: `admin`, Password: Final output line of `/deploy/local/kind.sh`
- Destroy kind cluster with `kind delete cluster`

Notes:

- `/deploy/local/kind.sh` will replace values in `deploy/k8s/root.yaml`, just like the gitlab-ci managed deployments. Don't commit these changes to the repo.

## Quick reference
```
#PWD=deploy/local
set -a && . .env && set +a
./build.sh
./kind.sh
```

# Docker compose deployment
  - Create and source `/deploy/local/.env` using `/deploy/local/example-env` as a reference
    - Special consideration to service URLs which will have different DNS addresses (equivalent to container name) in the docker compose network
      - Refer to the compose file. Some env vars are overwritten there directly
  - Ensure required paths exist for any volume mounts
    - Not relevant to services based deployment (vs monolith) and when using cloud or ephemeral datastores in containers
```
# $PWD = `deploy/local`

# Source env vars. Some equivalent values are overwritten in compose file
# Accounts for docker networking differences (vs k8s environments)
set -a && . ./.env && set +a

# Pull latest container images. Overwriting because we re-use `alpha` tag for local dev images
docker compose pull

docker compose --env-file ./.docker.env --env-file ./.secret.dev.env up -d
docker compose --env-file ./.docker.env --env-file ./.secret.dev.env down
```

# Local AgroCD
By default ArgoCD Application Sets are configured to continuously pull config from the remote repo origin. Disable this for local development investigating/troubleshooting by:

- set `ApplicationSet.spec.template.spec.syncPolicy.automated.selfHeal` == false in `deploy/k8s/root.yaml`
- use the argocd cli to update applications to sync from file paths local to the cli

Example:

```bash
#$PWD=./laterail-go/deploy/local
ARGO_PASS=$(kubectl --namespace argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
argocd login localhost:8080 --username admin --password $ARGO_PASS --insecure

# Disable sync policy on parent ApplicationSet
kubectl -n argocd patch --type='merge' applicationset cluster-shared -p "{\"spec\":{\"template\":{\"spec\":{\"syncPolicy\":null}}}}"

# Disable sync policy on specific child Application
kubectl -n argocd patch --type='merge' app cluster-common -p "{\"spec\":{\"template\":{\"spec\":{\"syncPolicy\":null}}}}"

# Update with local config
argocd app diff cluster-common --local ../argocd/laterail-common/

argocd app sync cluster-common --prune --local ../argocd/laterail-common/
```

# Misc notes
## Helm
```bash
# Debug
helm template ../charts/$CHART_NAME --debug > test.yaml

# Package and push to gitlab project package registry
helm package ../charts/$CHART_NAME
helm repo add --username $GITLAB_USER --password $GITLAB_ACCESS_TOKEN laterail https://gitlab.com/api/v4/projects/28689390/packages/helm/$CHART_NAME
helm cm-push "$CHART_NAME-<VERSION>.tgz" laterail

# Install from remote registry
helm repo update
helm install shared laterail/laterail-shared -n laterail --debug --dry-run

# Install from local chart
helm install laterail -n laterail ../charts/laterail --debug --dry-run

helm status laterail --namespace laterail
helm get all laterail --namespace laterail # Show current k8s manifests
```

## kubectl
```bash
kubectl get events -n laterail --sort-by='.metadata.creationTimestamp' -w
kubectl logs --selector app=consumer -n laterail -f
kubectl logs --selector app.kubernetes.io/name=external-dns -n external-dns -f
kubectl top pods -n laterail
```
