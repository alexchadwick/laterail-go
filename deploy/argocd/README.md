# Summary
- [ArgoCD Application Sets](https://argo-cd.readthedocs.io/en/stable/operator-manual/applicationset/) which deploy all required Kubernetes resources:
  - `cluster`: Cluster wide tools/dependencies (operators etc)
  - `app`:
    - LateRail app namespace scoped tools/dependencies (rabbitmq service, secrets etc)
    - The LateRail app helm chart (service, ingress etc)

# Environment context
- Dev Environment is deployed to a local [kind](https://kind.sigs.k8s.io/) k8s cluster
- Test and Prod are deployed to AWS EKS

## K8s distribution type specific values
- Each kubernetes cluster/distribution type (kind/EKS) has different "cluster wide" helm chart requirements
  - This is controlled using list files in [cluster-common](./cluster-common/)
  - Where the ArgoCD ApplicationSet `cluster` uses a nested [matrix generator](https://argo-cd.readthedocs.io/en/stable/operator-manual/applicationset/Generators-Matrix/) which selects the required list file using the ArgoCD Cluster resource label `distribution`
    - This label is created by this project as the k8s clusters are provisioned. It's not a standardised label

## Environment level values
- Each deployment environment level (dev/test/prod) has different Laterail helm chart value requirements
  - This is controlled using list files in [laterail](./laterail/)
  - Where the ArgoCD ApplicationSet `app` uses a nested matrix generator which selects the required list file using the ArgoCD Cluster resource name
  - Right now there is one k8s cluster per environment level  only

## Progressive RollingSync
- Using the alpha feature [RollingSync strategy (Progressive Sync)](https://argo-cd.readthedocs.io/en/stable/operator-manual/applicationset/Progressive-Syncs) for ArgoCD Application Sets to ensure that deployments don't get stuck, when deployments/other parent resources don't fail but their children (pods/etc) do, because dependencies (other helm charts) are not satisfied
  - In this scenario, the failing child resources are recreated but the parents are not. So the child resource config not updated when the dependencies are eventually met
    - This previously required manual termination of resources to resolve
- It's not possible for the RollingSync steps to sync across multiple Application Sets
- It's not practical/fun to combine the `cluster` and `app` Application Sets into one
- Argocd app `laterail-shared` has dependencies on several of the `cluster` appset apps
  - Including the `rolloutWave=last` app `opentelemetry-operator`
  - So the `app` appset apps always install after the `cluster` appset is healthy

# AWS Load Balancer Controller
- The AWS Load Balancer Controller is not deployed via ArgoCD because it needs to still exist when k8s services of type LoadBalancer (and other resources) are destroyed
  - Where ArgoCD will ([currently](https://github.com/argoproj/argo-cd/issues/14505)) attempt to delete all helm charts at once
- This allows the AWS Load Balancer Controller to destroy the AWS Load Balancers, Target Groups, Security Groups etc that it manages, before it is also removed
- If the AWS Load Balancer Controller is destroy before the dependent k8s services are deleted, some resources will fail to delete (finalizers) and the `terraform destroy` step will fail, because the VPC contains resources which are not managed by the terraform state
