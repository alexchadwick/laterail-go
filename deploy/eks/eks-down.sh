#!/bin/bash
<< COMMENT
Tear down EKS resources
COMMENT
set -euo pipefail

ACCOUNT="test"
K8S_CONTEXT="app0"
K8S_CONFIG_CONTEXT="arn:aws:eks:eu-west-2:016254607257:cluster/${ACCOUNT}-${K8S_CONTEXT}"
export AWS_PROFILE="dwick-${ACCOUNT}"
export AWS_REGION="eu-west-2"

# Save any letsencrypt generated TLS certificates
for SECRET in apps rabbitmq; do
    TLS_ENV="tls_${SECRET}_${ACCOUNT}"
    if [ "$(kubectl get secrets -n laterail -o=name laterail-${SECRET}-tls-secret 2> /dev/null)" == "secret/laterail-${SECRET}-tls-secret" ]
    then
        echo "🔵 Getting gitlab-ci varibale: ${TLS_ENV} ..."
        GET_RESPONSE=$(curl --request GET \
            --url "https://gitlab.com/api/v4/projects/28689390/variables/tls_${SECRET}_${ACCOUNT}" \
            --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
            --write-out "%{response_code}" \
            --output /dev/null \
            --silent)
        TLS=$(kubectl get secret "laterail-${SECRET}-tls-secret" -n laterail -ojson | base64 -w 0)
        
        if [ $GET_RESPONSE == '404' ]; then
            echo "🟢 No existing gitlab-ci var: ${TLS_ENV}"
            echo "🔵 Adding new certificate gitlab-ci var: ${TLS_ENV} ..."
            RESPONSE_CODE=$(curl --request POST \
                --url "https://gitlab.com/api/v4/projects/28689390/variables" \
                --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
                --header "Content-Type: application/json" \
                --write-out "%{response_code}" \
                --output /dev/null \
                --silent \
                --data '{
                "variable_type": "env_var",
                "key": "'$TLS_ENV'",
                "value": "'$TLS'",
                "masked": true
            }')
        elif [ $GET_RESPONSE == '200' ]; then
            echo "🟢 Found existing gitlab-ci var: ${TLS_ENV}"
            echo "🔵 Updating existing certificate gitlab-ci var: ${TLS_ENV} ..."
            RESPONSE_CODE=$(curl --request PUT \
                --url "https://gitlab.com/api/v4/projects/28689390/variables/${TLS_ENV}" \
                --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
                --header "Content-Type: application/json" \
                --write-out "%{response_code}" \
                --output /dev/null \
                --silent \
                --data '{
                "variable_type": "env_var",
                "value": "'$TLS'",
                "masked": true
            }')
        else
            echo "🔴 Unexpected response: $GET_RESPONSE, from gitlab-ci for var: laterail-${SECRET}-tls-secret"
        fi
        if [ "$RESPONSE_CODE" != '200' ]; then
            echo "🔴 Failed to upload laterail-${SECRET}-tls-secret"
        else
            echo "🟢 Uploaded laterail-${SECRET}-tls-secret"
        fi
    else
        echo "🟠 k8s certificate secret laterail-${SECRET}-tls-secret not found. Nothing to backup"
    fi
done

echo
read -rsp $'Press any key to delete ArgoCD resources\n' -n1 key
kubectl delete svc/argocd-server \
    --namespace argocd \
    --ignore-not-found=true \
    --context $K8S_CONFIG_CONTEXT

echo "🔵 $(date -uIseconds) Deleting ArgoCD application sets ..."
kubectl delete -f deploy/argocd/root.yaml \
    --namespace argocd \
    --ignore-not-found=true \
    --timeout=60s \
    --context $K8S_CONFIG_CONTEXT ||
    (
    echo "🟠 $(date -uIseconds) ArgoCD AppSett delete timed out after 60 seconds. Deleting any remaining ingress and rabbitmqcluster resources manually" &&
    echo "    This occurs when the ingress-nginx and/or rabbitmq operators are deleted before the resources they manage (kinds ingress and rabbitmqcluster)" &&
    for INGRESS in laterail rabbitmq; do
        kubectl get ingress/${INGRESS} --context $K8S_CONFIG_CONTEXT &> /dev/null &&
        "🟠 $(date -uIseconds) ingress/${INGRESS} still exists. Deleting ..."
        kubectl delete ingress/${INGRESS} \
        --namespace laterail \
        --ignore-not-found=true \
        --timeout=60s \
        --context $K8S_CONFIG_CONTEXT
    done &&
    kubectl get rabbitmqcluster/rabbitmq --context $K8S_CONFIG_CONTEXT &> /dev/null &&
    (echo "🟠 $(date -uIseconds) rabbitmqcluster/rabbitmq still exists. Deleting ..." &&
    kubectl patch rabbitmqcluster/rabbitmq -p '{"metadata":{"finalizers":[]}}' --type=merge --context $K8S_CONFIG_CONTEXT &&
    kubectl delete rabbitmqcluster/rabbitmq \
    --namespace laterail \
    --ignore-not-found=true \
    --timeout=60s \
    --context $K8S_CONFIG_CONTEXT)
    ) &&
echo "🟢 $(date -uIseconds) Finished deleting resources"

echo "🔵 $(date -uIseconds) Waiting 1min for AWS LB Controller to tidy up resources it manages..."
sleep 60
echo "🟢 $(date -uIseconds) Finished waiting"

read -rsp $'Press any key to delete AWS resources\n' -n1 key
rm ${ACCOUNT}-${K8S_CONTEXT}.tfplan
terraform destroy \
  -var-file ./variables/${ACCOUNT}.tfvars \
  -var-file ./variables/${ACCOUNT}/${K8S_CONTEXT}.tfvars \
  -auto-approve
