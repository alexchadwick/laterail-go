variable "account_name" {
  type        = string
  description = "AWS Account friendly name"
}

variable "cluster_context" {
  type        = string
  description = "Name of EKS cluster resource context. E.g. A region within an AWS account"
}

variable "kubernetes_version" {
  type        = string
  description = "Kubernetes version used in EKS cluster"
}

variable "vpc_cidr" {
  description = "VPC CIDR for all resources in this AWS account context"
  type        = string
}
