#!/bin/bash
<< COMMENT
Deploy Helm charts to Test site EKS cluster. Used during testing, pre-pipeline creation

Some env vars are set from "deploy/local/example-env" E.g.
  GITLAB_USER
  GITLAB_ACCESS_TOKEN
COMMENT
set -euo pipefail

ACCOUNT="test"
K8S_CONTEXT="app0"
K8S_CONFIG_CONTEXT="arn:aws:eks:eu-west-2:016254607257:cluster/${ACCOUNT}-${K8S_CONTEXT}"
export AWS_PROFILE="dwick-${ACCOUNT}"
export AWS_REGION="eu-west-2"

# Provision EKS Cluster
terraform fmt -recursive
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/28689390/terraform/state/${ACCOUNT}-${K8S_CONTEXT}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/28689390/terraform/state/${ACCOUNT}-${K8S_CONTEXT}/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/28689390/terraform/state/${ACCOUNT}-${K8S_CONTEXT}/lock" \
    -backend-config="username=$GITLAB_USER" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5" \
    -upgrade
terraform validate
terraform plan \
  -out ${ACCOUNT}-${K8S_CONTEXT}.tfplan \
  -var-file ./variables/${ACCOUNT}.tfvars \
  -var-file ./variables/${ACCOUNT}/${K8S_CONTEXT}.tfvars

echo
read -p "Press enter to apply"
terraform apply ${ACCOUNT}-${K8S_CONTEXT}.tfplan

# Update kubeconfig on workstation
aws eks update-kubeconfig --name ${ACCOUNT}-${K8S_CONTEXT}

# Deploy metrics-server, required by HPA
helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm upgrade --install metrics-server metrics-server/metrics-server \
  --create-namespace \
  --version "3.11.0" \
  --namespace kube-system

# Deploy the AWS LB Controller. This can't have the same lifecycle as ArgoCD managed resources
# Need to still exist after all others have been destroyed, to clean up the AWS resources it deletes
# Not using AWS Addon, because it takes much longer to install using that method
ACCOUNT_ID=$(aws sts get-caller-identity --query 'Account' --output text)
helm repo add eks https://aws.github.io/eks-charts
helm upgrade --install aws-load-balancer-controller eks/aws-load-balancer-controller \
  --create-namespace \
  --version "1.6.1" \
  --namespace kube-system -f - <<EOF
clusterName: ${ACCOUNT}-${K8S_CONTEXT}
serviceAccount:
  create: true
  name: aws-load-balancer-controller
  annotations:
    eks.amazonaws.com/role-arn: arn:aws:iam::${ACCOUNT_ID}:role/aws_load_balancer_controller
EOF

# https://github.com/argoproj/argo-helm/blob/argo-cd-5.46.8/charts/argo-cd/README.md
helm repo add argo https://argoproj.github.io/argo-helm
helm upgrade --install argo-cd argo/argo-cd \
  --create-namespace \
  --version "5.46.8" \
  --namespace argocd -f - <<EOF
fullnameOverride: argocd
configs:
  params:
    applicationsetcontroller.enable.progressive.syncs: true
redis-ha:
  enabled: true
controller:
  replicas: 1
server:
  replicas: 2
  service:
    type: LoadBalancer
repoServer:
  replicas: 2
applicationSet:
  replicas: 2
EOF

# Once ArgoCD is ready, enable access from local machine
kubectl wait \
  --for=condition=Ready pods \
  --all \
  --namespace argocd \
  --timeout=180s \
  --context $K8S_CONFIG_CONTEXT

# Create secrets from env vars
for ns in laterail external-dns
do
  if ! kubectl get ns "$ns" &> /dev/null; then
    kubectl create namespace "$ns" --context $K8S_CONFIG_CONTEXT
    kubectl create secret generic azure-secret-sp \
      --from-literal clientid=${SERVICE_PRINCIPAL_CLIENT_ID} \
      --from-literal clientsecret=${SERVICE_PRINCIPAL_CLIENT_SECRET} \
      --namespace "$ns" \
      --context $K8S_CONFIG_CONTEXT
    kubectl create secret generic cloudflare \
      --from-literal API_TOKEN=${CLOUDFLARE_API_TOKEN} \
      --namespace "$ns" \
      --context $K8S_CONFIG_CONTEXT
  fi
done

# Get any existing letsencrypt certs stored in gitlab-ci variables
for SECRET in apps rabbitmq; do
  TLS_ENV="tls_${SECRET}_${ACCOUNT}"
  echo "🔵 Getting gitlab-ci varibale: ${TLS_ENV} ..."
  GET_RESPONSE=$(curl --request GET \
      --url "https://gitlab.com/api/v4/projects/28689390/variables/tls_${SECRET}_${ACCOUNT}" \
      --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
      --write-out "%{response_code}" \
      --output /dev/null \
      --silent)
    
  if [ $GET_RESPONSE == '404' ]; then
    echo "🟢 No existing gitlab-ci var: ${TLS_ENV}"
  elif [ $GET_RESPONSE == '200' ]; then
    echo "🟢 Found existing gitlab-ci var: ${TLS_ENV}"
    VAR_FULL=$(curl --request GET \
      --url "https://gitlab.com/api/v4/projects/28689390/variables/tls_${SECRET}_${ACCOUNT}" \
      --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN")
    echo $VAR_FULL | jq -r .value | base64 -d  | kubectl apply \
      --namespace laterail \
      --context $K8S_CONFIG_CONTEXT \
      -f -
  else
    echo "🔴 Unexpected response: $GET_RESPONSE, from gitlab-ci for var: laterail-${SECRET}-tls-secret"
  fi
done

# Wait for argocd secret
until kubectl --namespace argocd get secret --context $K8S_CONFIG_CONTEXT | grep "argocd-initial-admin-secret"; do : ; done
ARGO_PASS=$(kubectl --namespace argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)

# Update argocd "in-cluster" cluster. Creating secret, adding labels
# Used by ArgoCD matrix generator
kubectl port-forward svc/argocd-server \
  --namespace argocd 8080:443 \
  --context $K8S_CONFIG_CONTEXT &> /dev/null &
argocd login localhost:8080 \
  --username admin \
  --password $ARGO_PASS \
  --insecure # Selfsigned TLS

argocd cluster add $(kubectl config current-context) \
  --name test \
  --label env=test \
  --label distribution=eks \
  --in-cluster \
  --upsert \
  --yes

# Deploy ApplicationSet
kubectl apply -n argocd -f ../argocd/root.yaml --context $K8S_CONFIG_CONTEXT
echo -e "$ARGO_PASS https://localhost:8080\n"

# Delete resources
# source ./eks-down.sh
