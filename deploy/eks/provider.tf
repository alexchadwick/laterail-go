terraform {
  required_version = "~> 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0"
    }
  }

  # Using gitlab's tf backend
  backend "http" {
  }
}

provider "aws" {
  default_tags {
    tags = {
      Environment = var.account_name
      Context     = var.cluster_context
      Project     = "gitlab.com/alexchadwick/laterail-go"
    }
  }
}

# Timeout 15 mins
data "aws_eks_cluster_auth" "this" {
  name = module.eks.cluster_name
  # depends_on = [ <some resource created late on, so still valid ]
}

# Required to manage the k8s configmap "aws-auth"
# See create_aws_auth_configmap, manage_aws_auth_configmap
provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.this.token

  # This is the recommended method, because using a token will expire and requires additional public networking changes
  # Requires the aws cli to be installed
  # exec {
  #   api_version = "client.authentication.k8s.io/v1beta1"
  #   command     = "aws"
  #   args        = ["eks", "get-token", "--cluster-name", module.eks.cluster_name]
  # }
}