# Intro
Host all k8s resources on AWS
  - Previous deployment method used Azure Kuberenetes Service
## Aims
  - Use mixture of spot and on-demand instances
  - Use self managed nodes (support AWS local zones etc) running Bottlerocket
  - Use [amazon-vpc-cni-k8s](https://github.com/aws/amazon-vpc-cni-k8s) (plugin for pod VPC networking)
  - Avoid [AWS TF EKS Blueprints](https://aws-ia.github.io/terraform-aws-eks-blueprints/main) and EKS addons, where practical
    - Keep process for setting up local/other managed k8s service and EKS as similar as possible
    - Use [upstream TF module](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest) to make this easier
  - Ephemeral. Setup and tear down all resources with each pipeline run
  - PrivateLink/VPC Peering to MongoDB Atlas (TODO)
  - Multi-region (eventually)

# Terraform
## Variable files structure
- Each AWS Account has a variables file in `deploy/tf/eks/variables/<AWS-ACCOUNT-NAME>.tfvars`
- Each AWS Account may contain multiple EKS clusters. Cluster specific variables are stored under `deploy/tf/eks/variables/<AWS-ACCOUNT-NAME>/<CLUSTER-TYPE>`
  - E.g. `deploy/tf/eks/variables/dev/app0.tfvars`

## Deploy
```bash
# Working dir == `deploy/eks` 
export AWS_PROFILE="dwick-dev"
export AWS_REGION="eu-west-2"
ACCOUNT="dev"
TF_CONTEXT="app0"

terraform fmt -recursive
terraform init
terraform validate
terraform plan \
  -out ${ACCOUNT}-${TF_CONTEXT}.tfplan \
  -var-file ./variables/${ACCOUNT}.tfvars \
  -var-file ./variables/${ACCOUNT}/${TF_CONTEXT}.tfvars
# ~25 mins to deploy
  # ~11 of which is CoreDNS addon...
terraform apply ${ACCOUNT}-${TF_CONTEXT}.tfplan
terraform destroy \
  -var-file ./variables/${ACCOUNT}.tfvars \
  -var-file ./variables/${ACCOUNT}/${TF_CONTEXT}.tfvars

# Update kubeconfig on workstation
aws eks update-kubeconfig --name ${ACCOUNT}-${TF_CONTEXT}
```
- See [local setup script](./eks.sh) or [gitlab-ci pipelines](../pipelines/) for other setup steps

# Bottlerocket notes
- Userdata (`module.eks.self_managed_node_groups.bootstrap_extra_args`) needs to be a valid TOML file
  - Quote keys containing `/` e.g. `"node.kubernetes.io/lifecycle"`

# TODO: Ingress and TLS
## Initial Plan:
  - Use ingress-nginx with AWS Load Balancer controller managed ALB and cert-manager (letsencrypt)
  - Use Ingress Groups, ALB target IP (Pods) etc (see notes below)
  - Investigate how ALB service limits affect deployment

## Secondary investigations:
Use:
  1. Add CloudFront + WAF
  2. Use ACM Private CA with cert-manager for TLS on the nginx/haproxy ingress
    - Terminating TLS on the nginx/haproxy ingress
  3. Later add some service mesh (e.g. Consul) so Pods use mTLS (with ACM Private CA)
    - Is this possible? -> ALB (TLS=ACM) -> nginx -> App (via service mesh proxy)

## Notes:
  - Only the ALB:
    - can use the WAF directly, unlike Classic and Network Load Balancers
    - is available in Outposts/Local Zones
  - [Beware ALB quotas/limits](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-limits.html)
    - Potentially an issue for clusters with many apps. E.g. path based routing rules, target groups per ALB etc
      - Workaround this by deploying a single ALB which passes all a ingress, which has it's own routing process?
      - Could route traffic from ALB ingress to nginx/haproxy ingress. Examples:
        - [Using an ingress-nginx controller NodePort service](https://github.com/kubernetes-sigs/aws-load-balancer-controller/issues/1384#issuecomment-698591561)
  - [Misc notes from the EKS docs on ALBs](https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html)
    - Use ingress annotation `alb.ingress.kubernetes.io/target-type: ip` to register pods as ALB targets (rather than the nodes)
      - Use with [Pod readiness gate](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/deploy/pod_readiness_gate/) to signal to ALB when Pod is ready for traffic
    - Use Ingress groups to re-use a single ALB for many services
  - [Misc notes from AWS LB Controller docs](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4)
    - [Redirect incoming http -> https](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/guide/tasks/ssl_redirect/)
    - [AWS LB Controller ACM cert discovery](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/guide/ingress/cert_discovery/)
      - Rather than specify the ACM cert ARN on the ingress annotation