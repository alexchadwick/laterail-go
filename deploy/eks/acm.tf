# provider "cloudflare" {
#   api_token = var.cloudflare_api_token
# }

# locals {
#   # Try to get the base domain from any fqdn
#   zone_name = regex(
#     "([^\\.\\s]+\\.co\\.[^\\.\\s]+$)|([^\\.\\s]+\\.[^\\.\\s]+$)",
#     var.public_dns.domain_name
#   )
# }

# output "zone_name" {
#   value = local.zone_name
# }

# data "cloudflare_zone" "this" {
#   name = element(local.zone_name, length(local.zone_name) - 1)
# }

# module "acm" {
#   source  = "terraform-aws-modules/acm/aws"
#   version = "~> 4.0"

#   domain_name = var.public_dns.domain_name
#   zone_id     = data.cloudflare_zone.this.id

#   subject_alternative_names = var.public_dns.subject_alternative_names

#   create_route53_records  = false
#   validation_record_fqdns = cloudflare_record.this.*.hostname

#   # Might take a while. Don't force any pipeline to wait
#   wait_for_validation = false
# }

# resource "cloudflare_record" "this" {
#   count = length(module.acm.distinct_domain_names)

#   zone_id = data.cloudflare_zone.this.id
#   name    = element(module.acm.validation_domains, count.index)["resource_record_name"]
#   type    = element(module.acm.validation_domains, count.index)["resource_record_type"]
#   value   = trimsuffix(element(module.acm.validation_domains, count.index)["resource_record_value"], ".")
#   ttl     = 60
#   proxied = false
# }
