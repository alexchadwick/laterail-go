# Context level variables
# Eventual aim: Support multiple deployments (contexts) per AWS account
# E.g.
#  - Deployments in multiple regions
#  - Central "config management" k8s cluster: Host ArgoCD which manages other clusters in AWS Account

cluster_context    = "app0"
kubernetes_version = "1.26"
vpc_cidr           = "10.0.0.0/16"
