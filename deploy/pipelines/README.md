# Gitlab-ci pipelines

## Triggers and Environment levels
**Aim**: Simplify the pipeline. Gitlab `needs` get gross for environment level inter-job dependencies 
- E.g. dev stage `terraform apply` depends on `terraform plan` in dev only, not test/prod/other environments
  - Result was a pretty silly pipeline

**Solution**: Pipeline runs in only one environment stage context (dev/test/prod)

### Environment pipeline triggers
- Dev env: Local environment, not managed by pipelines
- Test env: Only commit on tags `test-*` can trigger automatically
- Prod env: Only default branch commit can trigger automatically (merge to main)
- Any env: Via manual [Run pipeline](https://gitlab.com/alexchadwick/laterail-go/-/pipelines/new) web ui feature
  - Specify the pipeline variable `ENV_CONTEXT: '<chosen-environment>'` (e.g. `test`, `prod`)

## ArgoCD actions
### Up
- Check for existing namespace
  - Setup ArgoCD namespace + pre-req secrets if missing
- Apply argo root manifest
### Down
- Backup any letsencrypt generated TLS ceriticates as Gitlab-ci variables
- Tear down ArgoCD managed resources

## Limitations
### Manual jobs
- Gitlab-ci [manual jobs aren't ideal](https://docs.gitlab.com/ee/ci/jobs/job_control.html#types-of-manual-jobs)
  - Either don't block dependent jobs (even on failure) or pipeline doesn't succead unless all blocking manual jobs are ran
    - Expected option + block on failure
  - [Protected Environment Approvals have the same behaviour](https://docs.gitlab.com/ee/ci/environments/deployment_approvals.html)
