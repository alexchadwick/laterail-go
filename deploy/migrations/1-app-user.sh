#!/bin/bash
set -e

mongosh -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD <<EOF
use laterail
db.createUser({
  user:  '$APP_MONGO_USER',
  pwd: '$APP_MONGO_PASS',
  roles: ['readWrite']
})
db.history.createIndex(
  { request_id: 1 }, 
  { unique: true }
)
EOF
