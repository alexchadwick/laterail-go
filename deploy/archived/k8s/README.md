# Kustomize

Used to create stage environment (test/prod) specific manifests

`kustomize build ./deploy/k8s/kustomize/overlays/dev/ > dev.yaml`

# Letsencrypt certificates

- TEST environment deployment uses a staging letsencrypt certificate, to prevent accidental rate limiting while testing. This is controlled in `deploy/k8s/kustomize/overlays/test/ingress.yaml`
- PROD environment deployment uses a production letsencrypt certificate

After successful http validation, cert-manager stores new TLS certificates as k8s secret called `laterail-tls-secret` (same name in both environments). These secrets are preserved between deployments to prevent rate limiting/abuse of the letsencrypt services while testing. Current process:

- The destroy pipeline, during stage "backup", saves the secret yaml to gitlab-ci as a base64 encoded masked variable
- The deploy pipeline, during stage "apply", applies the secret yaml (excluding the `uid` and `resourceVersion` properties) before the deployment of cert-manager and ingress resources. Using the [recovery process described in the cert-manager docs](https://cert-manager.io/docs/tutorials/backup/#order-of-restore)

# Relational database cluster

~~Using postgres, managed by the [Kubegres operator](https://www.kubegres.io).~~

- ~~[Streaming Replication](https://www.postgresql.org/docs/current/warm-standby.html#STREAMING-REPLICATION) between the primary and read replicas and automatic failover, where a read replica is promoted to primary~~
- ~~Backups using `pg_dump` to a PVC using K8s CronJob, using read replica as the source~~

Using [CockroachDB serverless](https://www.cockroachlabs.com/blog/announcing-cockroachdb-serverless/), one persistent free tier cluster per environment stage

- Uses the golang postgres driver [pgx](https://github.com/jackc/pgx)
- Switching between postgres and crockroachdb requires changing the database url used by [golang-migrate](https://github.com/golang-migrate/migrate)
  - "cockroachdb://" vs "postgres://"

# API managed datasource migrations/initialisation

The API will attempt to complete postgres schema migrations and first run setup actions for mongodb using the contents of `deploy/migrations`

- Current process uses Terraform to upload the contents of `deploy/migrations` to an Azure storage account file share, which is mounted on the pod
- During setup, a K8s secret called `azure-storage-secret` is created which contains the storage account name and key. This is required to authenticate volume access

# External Secrets

[external-secrets](https://github.com/external-secrets/external-secrets/) is used to import K8s secrets from an Azure Key Vault

# External DNS

[external-dns](https://github.com/kubernetes-sigs/external-dns) is used to dynamically update public DNS records for service and ingress resources
