# Intro
Aim to replace current [Kustomize deployment process](../k8s/README.md) with Helmfile

# Structure
Opted to use a single helmfile because it simplified inter-chart dependencies, using the helmfile release property `needs`
- Alternative popular structure: A subdirectory for each chart, containing a helmfile, value template and whatever else. Examples:
  - https://lyz-code.github.io/blue-book/devops/helmfile/#multi-environment-project-structure
  - https://itnext.io/setup-your-kubernetes-cluster-with-helmfile-809828bc0a9f

## Custom charts
Two custom charts:
  - ["cluster"](./charts/cluster/Chart.yaml): Setup cluster level pre-requisites
  - [laterail](./charts/laterail/Chart.yaml): Application level resources

# Misc notes
- Using Jaeger AllinOne RAM only deployment for in-cluster tracing
  - Using Tempo (Grafana Cloud) for persistent storage