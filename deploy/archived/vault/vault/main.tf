terraform {
  required_version = "~> 1.0"
  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "3.13.0"
    }
  }
}

provider "vault" {
  # Not possible to define these parameters using output from the hcp_vault_cluster resource
  # because the provider doesn't wait for these values to exist
  # https://github.com/hashicorp/terraform-provider-vault/issues/1198
  # This is why the HCP and Vault tf sections are separate
  # address          = hcp_vault_cluster.this.vault_public_endpoint_url
  # token            = hcp_vault_cluster_admin_token.this.token
  namespace = "admin"
}

# Create namespaces
#------------------------------------------------------------------------------
#   admin
#    ├── laterail
#    │   └── consul
#    │       └── dev
#    └── consul (kv-v2)
#------------------------------------------------------------------------------

# # Create 'admin/laterail' namespace
# resource "vault_namespace" "laterail" {
#   path = "laterail"
# }

# # Create 'admin/laterail/consul' namespace
# resource "vault_namespace" "consul" {
#   namespace = vault_namespace.laterail.path
#   path      = "consul"
# }

# # Create 'admin/laterail/consul/dev' namespace
# resource "vault_namespace" "dev" {
#   namespace = vault_namespace.consul.path_fq
#   path      = "dev"
# }

# Create secret engines
resource "vault_mount" "consul" {
  path = "consul"
  type = "kv-v2"
}

# Create Gossip key
resource "vault_kv_secret_v2" "gossip" {
  mount = vault_mount.consul.path
  name  = "gossip"
  data_json = jsonencode({
    gossip = var.gossip_key
  })
}

resource "vault_mount" "pki-root" {
  path = "pki"
  type = "pki"

  default_lease_ttl_seconds = 3600
  max_lease_ttl_seconds     = 86400
}

# Role for Consul CA root cert
resource "vault_pki_secret_backend_role" "root-consul-ca-role" {
  backend            = vault_mount.pki-root.path
  name               = "root-consul-ca-role"
  max_ttl            = 720 * 60 * 60
  generate_lease     = true
  allow_localhost    = true
  allow_bare_domains = true
  allow_subdomains   = true
  allowed_domains = [
    "dc1.consul",
    "consul-server",
    "consul-server.consul",
    "consul-server.consul.svc",
  ]
}

# Generate the root certificate for Consul CA
resource "vault_pki_secret_backend_root_cert" "root-consul-ca-cert" {
  backend     = vault_mount.pki-root.path
  type        = "internal"
  common_name = "dc1.consul"
  ttl         = 87600 * 60 * 60
}

resource "vault_mount" "connect-root" {
  path = "connect-root"
  type = "pki"
}

# Enable k8s authN
resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"
}

# Create policies
resource "vault_policy" "gossip" {
  name = "gossip-policy"

  policy = <<EOF
# Gossip encryption key
path "consul/data/secret/gossip" {
  capabilities = ["read"]
}
EOF
}

resource "vault_policy" "server" {
  name = "consul-server"

  policy = <<EOF
# Allows consul server to create TLS certs & retreive the CA cert
path "kv/data/consul-server"
{
  capabilities = ["read"]
}
path "pki/issue/consul-server"
{
  capabilities = ["read","update"]
}
path "pki/cert/ca"
{
  capabilities = ["read"]
}
EOF
}

# Consul CA access policy
resource "vault_policy" "ca" {
  name = "ca-policy"

  policy = <<EOF
# Access to the Consul root CA so that Consul agents and services can verify the certificates used in the service mesh are authentic
path "pki/cert/ca" {
  capabilities = ["read"]
}
EOF
}

# Consul service mesh CA policy
resource "vault_policy" "connect" {
  name = "connect"

  policy = <<EOF
# Allows Consul to create and manage the root and intermediate PKI secrets engines for generating service mesh certificates
path "/sys/mounts/connect-root" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}
path "/sys/mounts/connect-intermediate-dc1" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}
path "/sys/mounts/connect-intermediate-dc1/tune" {
  capabilities = [ "update" ]
}
path "/connect-root/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}
path "/connect-intermediate-dc1/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}
path "auth/token/renew-self" {
  capabilities = [ "update" ]
}
path "auth/token/lookup-self" {
  capabilities = [ "read" ]
}
EOF
}