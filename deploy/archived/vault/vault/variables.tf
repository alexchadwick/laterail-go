variable "gossip_key" {
  type        = string
  description = "Gossip encryption key"
  sensitive   = true
}
