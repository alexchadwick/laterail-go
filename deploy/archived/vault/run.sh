#!/bin/bash
<< COMMENT
Deploy Vault HCP

Dependencies:
- Terraform
- Vault CLI
COMMENT
set -euo pipefail

# Vault provider can't wait for HCP resources to be ready, so need to run HCP
# and Vault tf separately

cd ./hcp
terraform fmt -recursive
terraform init
terraform validate
terraform plan -out dev.tfplan
terraform apply dev.tfplan

export VAULT_ADDR=$(terraform output -json | jq -r .vault_addr_public.value)
export VAULT_TOKEN=$(terraform output -json | jq -r .vault_admin_token.value )

# terraform destroy -auto-approve

cd ../vault
terraform fmt -recursive
terraform init
terraform validate
terraform plan -out dev.tfplan -var "gossip_key=$(consul keygen)"
terraform apply dev.tfplan
