variable "cloud_provider" {
  type        = string
  description = "HCP Cloud Provider"
  default     = "aws"
}

variable "environment" {
  type        = string
  description = "The application environment stage. E.g. dev, test, prod"
  default     = "dev"
}

variable "hcp_region" {
  type        = string
  description = "Region for HCP"
  default     = "eu-west-2"
}

variable "cidr_block" {
  type        = string
  description = "Private IPv4 CIDR block for HVN. Must be between /16 and /25"
  default     = "172.16.0.0/24"
}

variable "hcp_vault_tier" {
  description = "Tier for HCP Vault"
  type        = string
  default     = "dev"
}

variable "vault_public_endpoint" {
  description = "Enable publicly accessible endpoint for Vault"
  type        = bool
  default     = true
}
