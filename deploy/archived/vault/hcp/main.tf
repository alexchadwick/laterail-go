terraform {
  required_version = "~> 1.0"
  required_providers {
    hcp = {
      source  = "hashicorp/hcp"
      version = "~> 0.55.0"
    }
  }
}

provider "hcp" {}

# Used by resources which require unique names
resource "random_string" "this" {
  length  = 4
  special = false
  upper   = false
}

resource "hcp_hvn" "this" {
  cloud_provider = var.cloud_provider
  hvn_id         = "laterail-${var.environment}"
  region         = var.hcp_region
  cidr_block     = var.cidr_block
}

resource "hcp_vault_cluster" "this" {
  cluster_id      = "laterail-${var.environment}-${random_string.this.result}"
  hvn_id          = hcp_hvn.this.hvn_id
  public_endpoint = var.vault_public_endpoint
  tier            = var.hcp_vault_tier
}

resource "hcp_vault_cluster_admin_token" "this" {
  cluster_id = hcp_vault_cluster.this.cluster_id
}

# The Private Endpoint for HCP Vault
output "vault_addr_public" {
  value = hcp_vault_cluster.this.vault_public_endpoint_url
}

# The Vault admin token
output "vault_admin_token" {
  value     = hcp_vault_cluster_admin_token.this.token
  sensitive = true
}
