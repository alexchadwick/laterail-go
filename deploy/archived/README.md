# Secret storage

Each app environment stage requires secrets but the primary source of these secrets differs per stage

- local/DEV: All secrets are stored in Azure Key Vault or local `.env` files
- TEST/PROD: Secrets are primarily stored in Gitlab CI/CD variables, as a single masked variable containing base64 encoded json
  - This is consumed by Terraform to create Azure Key Vault secrets, which are accessed by external-secrets in k8s
  - This setup allows the TEST/PROD environments to be automatically destroyed/recreated, as persistent secrets are stored in gitlab CI/CD

## Secrets

Pretty printed format of secret set stored in Gitlab CI/CD

- They retain the name when added to Azure Key Vault

```
[
  {
    "name": "postgres-APP-W-PASS",
    "secret": "<secret>"
  },
  {
    "name": "postgres-uri",
    "secret": "<user>:<secret>@<db-uri>:<db-port>/defaultdb?sslmode=verify-full&sslrootcert=/root/.postgresql/root.crt&options=--cluster%3D<cluster-name>"
  },
  {
    "name": "mongo-root-password",
    "secret": "<secret>"
  },
  {
    "name": "mongo-app-password",
    "secret": "<secret>"
  },
  {
    "name": "mongo-uri",
    "secret": "mongodb://<user>:<secret>@<db-uri>:<db-port>/?authSource=laterail"
  },
  {
    "name": "hsp-user",
    "secret": "<user>"
  },
  {
    "name": "hsp-password",
    "secret": "<secret>"
  },
  {
    "name": "cloudflare-api-token",
    "secret": "<secret>"
  },
  {
    "name": "loki-uri",
    "secret": "https://<user>:<secret>@logs-prod-eu-west-0.grafana.net/loki/api/v1/push"
  },
  {
    "name": "jaeger-auth0-client-secret",
    "secret": "<secret>"
  },
  {
    "name": "jaeger-auth0-cookie-secret",
    "secret": "<secret>"
  },
  {
    "name": "tempo-basic-authn",
    "secret": "<secret>"
  },
  {
    "name": "prometheus-secret-key",
    "secret": "<secret>"
  }
]

```
