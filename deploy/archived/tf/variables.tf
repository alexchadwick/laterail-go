variable "project_name" {
  type    = string
  default = "laterail"
}

variable "location" {
  type    = string
  default = "uksouth"
}

variable "stage" {
  type    = string
  default = "prod"
}

variable "azure_tenant_id" {
  type = string
}

variable "ssh_public_key" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}

variable "resource_groups" {
  type = map(string)
  default = {
    main    = "laterail-main"
    cluster = "laterail-cluster"
  }
}

variable "cluster_name" {
  type    = string
  default = "cluster0"
}

variable "node_pool" {
  type = any
  default = {
    name   = "default"
    number = 3
    size   = "Standard_B2s"
  }
}

variable "k8s_config" {
  type = map(any)
  default = {
    namespace = "laterail"
  }
}

variable "secrets_set" {
  type      = any
  sensitive = true
}

variable "gitlab_token" {
  type      = string
  sensitive = true
}