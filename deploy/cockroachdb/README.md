# CockroachDB serverless

Using [CockroachDB serverless](https://www.cockroachlabs.com/blog/announcing-cockroachdb-serverless/) as primary relational datastore.
  - One persistent free tier cluster per environment stage
  - Uses the golang postgres driver [pgx](https://github.com/jackc/pgx)

CockroachDB serverless is currently in beta and doesn't support any IaC tools. I manually created separate DEV, TEST and PROD clusters using their webapp. 
These run continuously, with a lifecycle independent of the gitlab-ci and terraform managed provisioning and deployment process.

# Setup

- Create new cluster using the webapp
- Create new sql user(s) using the webapp
- Use the cockroachdb cli tool to run the initialisation migration which creates databases and roles required for application managed schema migrations
  ```
  cockroach sql --url <DB-URL> -f "./deploy/cockroachdb/up.sql"
  ```
- Connect using `psql`
```
PGSSLMODE=verify-full PGSSLROOTCERT=~/.postgresql/root.crt psql -h <URL> -p 26257 -U <user> -d laterail
```

## Cluster CA
  - [Before connecting to a cluster you must download and trust a CA](https://www.cockroachlabs.com/docs/cockroachcloud/connect-to-a-serverless-cluster.html?filters=connection-string#step-2-connect-to-your-cluster)
    ```
    curl --create-dirs -o ~/.postgresql/root.crt -O https://cockroachlabs.cloud/clusters/5b6f79f9-3129-4d1b-b944-c98602fd4b1a/cert
    ```
    - Same CA cert for all clusters
    - This process was added to the `api` component container image:
    ```
    RUN mkdir -p /root/.postgresql && wget -O /root/.postgresql/root.crt https://cockroachlabs.cloud/clusters/5b6f79f9-3129-4d1b-b944-c98602fd4b1a/cert
    ```

# Other supported databases
- postgreSQL only
  - Toggle by changing the `api` container env var `DATABASE_ENGINE`. Only the values `cockroachdb` and `postgres` are valid.
  - This value is only used by [golang-migrate](https://github.com/golang-migrate/migrate). Other database connection strings reference the cockroachdb clusters as though they were postgres
    - Toggles connection strings which begin "cockroachdb://" or "postgres://"