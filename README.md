[![pipelines](https://gitlab.com/alexchadwick/laterail-go/badges/main/pipeline.svg?ignore_skipped=true&key_text=pipelines)](https://gitlab.com/alexchadwick/laterail-go/-/pipelines)

# Aim

This project is a testing ground for devops tools, processes and general software development experience.

The app pulls National Rail UK train journey data from official APIs, calculates delays/cancellations, and reports when you're eligble to claim money back via the rail service delay repay schemes.

# Demo

[Here: laterail.com](https://laterail.com)

The full project is only deployed when I'm actively working on it but the React frontend can also be deployed as a standalone static site demo on CloudFlare Pages.

Gitlab pipelines ensure the demo site is running when the full project isn't.

# Progress

- **Backend**: Grabs journey info for UK train services and processes to determine delays/cancellations
- **Frontend**: Search feature for backend API requests. Displays results with some stats
- **Fully automatic**: Test, build, provision, deploy and destroy process using Gitlab CI/CD pipelines
- **Observability**: Export linked distributed logs and traces from the app services and Kubernetes resources

# Tools

## Using

- **Golang backend**: Multiple different backend services, which independently scale, and share information using message queues
  - Processing and storing journey data fetched from the [Historical Service Performance (HSP) Darwin API](https://wiki.openraildata.com/index.php/HSP)
  - Originally structured as a single service backend, but was split up to learn about microservice service systems and the supporting infrastructure and workflows they require
- **React frontend**: Single Page App using Material-UI components and Auth0 for user authentication
- **Kubernetes**: Using EKS and kind (local development)
  - Incl. cert-manager, ingress-nginx, external-dns, external-secrets and more
- **Observability**:
  - Traces - OpenTelemetry Collector exporting to Tempo (Grafana Cloud)
  - Logs (linked to traces) - Loki (Grafana Cloud) via Promtail
  - Metrics - OpenTelemetry Collector exporting to Prometheus (Grafana Cloud)
- **ArgoCD**: Deploy and manage Kubernetes resources with GitOps driven Continuous Delivery
- **Terraform**: Provision/destroy infrastructure, and bootstrap Kubernetes cluster with ArgoCD
- **Gitlab CI/CD**:
  - Build app service container images via a multi-stage build process
  - Provision and destroy public cloud resources using Terraform
  - Bootstrap new ArgoCD clusters and deploy ApplicationSets
  - Manage public DNS changes for resources which aren't managed by Kubernetes external-dns
- **Databases**:
  - PostgreSQL/CockroachDB serverless
  - MongoDB Atlas
- **Message broker**: RabbitMQ
- **User identity service**: Auth0

# Status
## Wanted/Planned
- Security scanning:
  - SAST: [sonarqube](https://www.sonarqube.org/downloads/), [semgrep](https://semgrep.dev/)
  - DAST: [zaproxy](https://www.zaproxy.org/)
  - IaC: [KICS](https://kics.io/), [tfsec](https://aquasecurity.github.io/tfsec)
- [Liquibase](https://www.liquibase.org/): db schema migrations as code, incl. extensions e.g. [Precona Toolkit](https://github.com/liquibase/liquibase-percona/blob/main/README.md), for online schema migrations etc
- [Pluto](https://github.com/FairwindsOps/pluto): Detect deprecated k8s config
- [KEDA](https://keda.sh/): Scale k8s pods using events from [scalers](https://keda.sh/docs/latest/scalers/) e.g. Prometheus metrics
- A dependency update bot for pipelines e.g. [Renovate](https://github.com/renovatebot/renovate)
- Feature Flags: [Gitlab's Unleash service](https://docs.gitlab.com/ee/operations/feature_flags.html)
- Load testing tool e.g. [k6](https://k6.io)