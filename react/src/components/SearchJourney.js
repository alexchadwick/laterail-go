import React, { useState, useEffect, useRef } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Alert from "@mui/material/Alert";
import Link from "@mui/material/Link";
import { matchSorter } from "match-sorter";
import { useForm } from "react-hook-form";
import { useAuth0 } from "@auth0/auth0-react";
import { useNavigate } from "react-router-dom";
import { useQuery } from "@tanstack/react-query";
import StationAutoComplete from "./StationAutoComplete";
import DateTimePickerMui from "./DateTimePickerMui";
import { SetPostBody } from "../services/ParseRequestBody.js";
import { producerRequest } from "../services/APIClient";
import { fetchStations } from "../services/fetch";

export default function SearchJourneys() {
  const defaultValues = {
    stationTo: null,
    stationFrom: null,
    datetimeOutbound: null,
    datetimeInbound: null,
  };
  const { loginWithRedirect, isAuthenticated } = useAuth0();
  const {
    handleSubmit,
    reset,
    control,
    getValues,
    formState: { errors },
  } = useForm({ defaultValues });
  const navigate = useNavigate();
  
  // Demo mode simulates API requests only
  const isDemo = window.config.demoMode === "true";
  const demoDelay = 1000; // Producer request
  const isTest = process.env.NODE_ENV !== "production";

  // Get stations info
  // Placeholder data prevents error while still fetching
  const stations = useQuery({
    queryKey: ['stations'],
    queryFn: async () => {
      return fetchStations(isDemo,demoDelay)
    },
    staleTime: Infinity,
    placeholderData: []
  })

  // Station autocomplete
  const filterOptions = (options, { inputValue }) =>
    matchSorter(options, inputValue, {
      keys: [
        { threshold: matchSorter.rankings.STARTS_WITH, key: "name" },
        { threshold: matchSorter.rankings.STARTS_WITH, key: "crs" },
      ],
    });

  // Test/Demo post body
  const mockBody = {
    from_loc: "GLQ",
    to_loc: "EDB",
    from_time: "0830",
    to_time: "0900",
    from_date: "2021-10-01",
    to_date: "2021-10-01",
  };

  // Main search button with DEMO mode flag
  let bodyRef = useRef({})
  const onSubmit = (data) => {
    // If no react hook form errors
    if (Object.keys(errors).length === 0) {
      bodyRef.current =  isDemo === true ? mockBody : SetPostBody(data);
      producerRequest(bodyRef.current, demoDelay, isDemo, setProducerResponse);
    } else {
      console.log("Errors: ", errors);
    }
  };

  // Test search button handler
  const onMockSubmit = () => {
    bodyRef.current =  mockBody;
    producerRequest(bodyRef.current, demoDelay, isDemo, setProducerResponse);
  };

  // Catch producer service response and
  // navigate to results page using request-id path
  const [producerResponse, setProducerResponse] = useState(new Map([]));
  useEffect(() => {
    if (Object.keys(producerResponse).length !== 0) {
      navigate(`/results/${producerResponse.data.request_id}`);
    }
  }, [producerResponse]);

  return (
    <React.Fragment>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography variant="h4">Search Journeys</Typography>
        <Box component="form" onSubmit={handleSubmit(onSubmit)} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <StationAutoComplete
                control={control}
                stationsList={stations.data}
                filterOptions={filterOptions}
                name="From"
                rules={{
                  required: "Required",
                  validate: {
                    SameStation: () =>
                      getValues("stationFrom") !== getValues("stationTo") ||
                      "Stations must be different",
                  },
                }}
              />
              {errors.stationFrom && (
                <Alert severity="error">{errors.stationFrom.message}</Alert>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <StationAutoComplete
                control={control}
                stationsList={stations.data}
                filterOptions={filterOptions}
                name="To"
                rules={{
                  required: "Required",
                  validate: {
                    SameStation: () =>
                      getValues("stationFrom") !== getValues("stationTo") ||
                      "Stations must be different",
                  },
                }}
              />
              {errors.stationTo && (
                <Alert severity="error">{errors.stationTo.message}</Alert>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <DateTimePickerMui
                control={control}
                name="Outbound"
                rules={{ required: "Required" }}
              />
              {errors.datetimeOutbound && (
                <Alert severity="error">
                  {errors.datetimeOutbound.message}
                </Alert>
              )}
            </Grid>
            <Grid item xs={12} sm={6}>
              <DateTimePickerMui
                control={control}
                name="Inbound"
                rules={{
                  required: "Required",
                  validate: {
                    InAfterOut: () =>
                      getValues("datetimeInbound") >
                        getValues("datetimeOutbound") ||
                      "Inbound journey must occur after Outbound",
                  },
                }}
              />
              {errors.datetimeInbound && (
                <Alert severity="error">{errors.datetimeInbound.message}</Alert>
              )}
            </Grid>
            <Grid item xs={12} sm={8} justifyContent="center">
              <Button type="submit" variant="contained" sx={{ mr: 2 }}>
                Search
              </Button>
              {isTest && !isDemo && (
                <Button
                  onClick={onMockSubmit}
                  variant="contained"
                  sx={{ mr: 2 }}
                  color="secondary"
                >
                  TEST
                </Button>
              )}
              {isDemo && (
                <Button
                  onClick={onMockSubmit}
                  variant="contained"
                  sx={{ mr: 2 }}
                  color="warning"
                >
                  DEMO
                </Button>
              )}
              <Button
                type="reset"
                variant="outlined"
                onClick={() => {
                  reset();
                }}
              >
                Reset
              </Button>
            </Grid>
          </Grid>
          {/* TODO: Enable after adding some authenticated user features */}
          {/* {!isAuthenticated &&
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link disabled href="#" variant="body2" onClick={() => loginWithRedirect()}>
                  Sign up to save your results
                </Link>
              </Grid>
            </Grid>
          } */}
        </Box>
      </Box>
    </React.Fragment>
  );
}
