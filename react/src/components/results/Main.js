import React, {useState, useEffect} from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import CircularProgress from '@mui/material/CircularProgress';
import { useQuery } from "@tanstack/react-query";
import AccordianMany from "./AccordianMany";
import Stats from "./Stats";
import { stationFind } from "../../services/stationFilter";
import { fetchStations } from "../../services/fetch";


export default function Main(props) {
  // Get stations info
  // Placeholder data prevents error while still fetching
  const stations = useQuery({
    queryKey: ['stations'],
    queryFn: async () => {
      return fetchStations(props.isDemo,props.demoDelay)
    },
    staleTime: Infinity,
  })

  const [fromStation, setFromStation] = useState(null)
  const [toStation, setToStation] = useState(null)
  useEffect(() => {
    if (typeof stations.data === "undefined") return
    setFromStation(stationFind(props.body.from_loc, stations.data))
    setToStation(stationFind(props.body.to_loc, stations.data))
  }, [stations])

  return (
    <React.Fragment>
      <Grid
        container
        sx={{ px: 2 }} // Horizontal padding most evident on mobile
        rowSpacing={{
          xs: 5,
          md: 5,
        }}
        columnSpacing={{ xs: 1, sm: 2, md: 4 }}
      >
        {/* Grid order different between mobile and larger screens */}
        <Grid item xs={0.5} md={12}></Grid>
        <Grid item xs={12} md={2} order={{ xs: 1, md: 1 }} rowSpacing={4}>
          <Box
            sx={{
              py: 2,
              mb: 2,
              borderRadius: 2,
              backgroundColor: (theme) =>
                theme.palette.mode === "dark"
                  ? theme.palette.primary.dark
                  : theme.palette.primary.light,
            }}
          >
            <Grid 
              container
              rowSpacing={0} 
              columnSpacing={0}
              justifyContent="center"
              alignItems="center" 
              sx={{ pl: 4, pr: 0 }}
            >
              <Grid item xs={3}>
                <Typography>
                  <strong>From</strong>
                </Typography>
              </Grid>
              <Grid item  xs={9} sx={{display: 'flex', justifyContent: "center", alignItems: "center"}}>
                <Typography>
                  {fromStation}
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography> 
                  <strong>To</strong>
                </Typography>
              </Grid>
              <Grid item  xs={9} sx={{display: 'flex', justifyContent: "center", alignItems: "center"}}>
                <Typography>
                  {toStation}
                </Typography>
              </Grid>
              <Grid item xs={3}>
              {props.body.from_date === props.body.to_date && (
                  <Typography>
                    <strong>Date</strong>
                  </Typography>
                )}
                {props.body.from_date !== props.body.to_date && (
                  <Typography>
                    <strong>Dates</strong>
                  </Typography>
                )}
              </Grid>
              <Grid item  xs={9} sx={{display: 'flex', justifyContent: "center", alignItems: "center"}}>
                {props.body.from_date === props.body.to_date && (
                  <Typography>
                    {props.body.from_date}
                  </Typography>
                )}
                {props.body.from_date !== props.body.to_date && (
                  <Typography>
                    {props.body.from_date} - {props.body.to_date}
                  </Typography>
                )}
              </Grid>
              <Grid item xs={3}>
                <Typography> 
                  <strong>Times</strong>
                </Typography>
              </Grid>
              <Grid item  xs={9} sx={{display: 'flex', justifyContent: "center", alignItems: "center"}}>
                <Typography>
                {props.body.from_time} -{" "} {props.body.to_time}
                </Typography>
              </Grid>
            </Grid>
          </Box>
          {typeof props.results.metadata.details_total !== "undefined" && (
          <Box
            sx={{
              py: 2,
              borderRadius: 2,
              backgroundColor: (theme) =>
                theme.palette.mode === "dark"
                  ? theme.palette.secondary.dark
                  : theme.palette.secondary.light,
            }}
          >
            <Typography variant="h5" align="center" gutterBottom={true}>
                Search Progress
            </Typography><Grid container>
              {/* Left column */}
              <Grid container item xs={12} md={7} sx={{ pl: 4 }}>
                <Grid item xs={6}>
                  <Typography>
                    <strong>New</strong>
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{display: 'flex', justifyContent: "center", alignItems: "center"}}>
                  {typeof props.results.metadata.details_done === "undefined" && (
                    <Typography>
                      <strong>Waiting...</strong>
                    </Typography>
                  )}
                  {typeof props.results.metadata.details_done !== "undefined" && (
                    <Typography>
                      {props.results.metadata.details_done}
                    </Typography>
                  )}
                </Grid>
                {typeof props.results.metadata.results_existing !== "undefined" && (
                  <>
                  <Grid item xs={6}>
                    <Typography>
                      <strong>Existing</strong>
                    </Typography>
                  </Grid>
                  <Grid item xs={6} sx={{display: 'flex', justifyContent: "center", alignItems: "center"}}>
                    <Typography>
                      {props.results.metadata.results_existing}
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>
                      <strong>Total</strong>
                    </Typography>
                  </Grid>
                  <Grid item xs={6} sx={{display: 'flex', justifyContent: "center", alignItems: "center"}}>
                    <Typography>
                      {props.results.metadata.details_total + props.results.metadata.results_existing}
                    </Typography>
                  </Grid>
                  </>
                )}
              </Grid>
              {/* Right column */}
              <Grid container item xs={12} md={5}>
                <Grid item xs={12} sx={{display: 'flex', justifyContent: "center", alignItems: "center"}}>
                  {typeof props.results.metadata.details_done !== "undefined" &&
                    (props.results.metadata.details_done !== props.results.metadata.details_total) && (
                      <CircularProgress color={"warning"} />
                    )}
                </Grid>
              </Grid>
            </Grid>
          </Box>
          )}
        </Grid>
        {typeof props.results.details !== "undefined" && (
          <Grid item xs={12} md={4} order={{ xs: 3, md: 2 }}>
            <AccordianMany results={props.results.details} />
          </Grid>
        )}
        <Grid item xs={12} md={6} order={{ xs: 2, md: 3 }}>
          <Stats
            results={props.results.details}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
