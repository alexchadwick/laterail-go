import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import SummaryBox from "./SummaryBox";
import Pie from "./Pie";
import BoxPlot from "./BoxPlot";
import { summaryStats } from "../../services/ProcessResults";

export default function Stats(props) {
  // Calculate journey detail stats
  const [summary, SetSummary] = useState(null)
  useEffect(() => {
    if (typeof props.results === "undefined") return
    SetSummary(summaryStats(props.results))
  }, [props.results])

  return (
    <React.Fragment>
      <Grid
        container
        rowSpacing={{
          xs: 5,
          md: 4,
        }}
        columnSpacing={{ xs: 1, sm: 2, md: 4 }}
      >
        {summary && (
          <>
          <Grid item xs={12} md={5}>
            <SummaryBox summary={summary.stats} />
          </Grid>
          <Grid item xs={12} md={7}>
            <Pie summary={summary.stats} />
          </Grid>
          <Grid item xs={12}>
            <BoxPlot delays={summary.delays} />
          </Grid>
          </>
        )}
      </Grid>
    </React.Fragment>
  );
}
