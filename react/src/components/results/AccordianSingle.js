import React, { useState, useEffect } from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { useTheme } from "@mui/material/styles";

export default function AccordionSingle(props) {
  const theme = useTheme();
  const [expanded, setExpanded] = useState(false);

  useEffect(() => {
    if (
      props.detials.to.delay_valid && props.detials.to.delay > 0 ||
      !props.detials.to.delay_valid
    ) {
      setExpanded(props.rid);
    }
  }, [props.detials]);

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  // Set accordian theme incl. pallette mode (dark/light)
  function statusTheme(theme, to) {
    if (theme.palette.mode === "dark") {
      if (!to.delay_valid || to.delay >= 30 ) {
        return theme.palette.error.dark;
      }
      if ( to.delay > 0 && to.delay < 30
      ) {
        // setExpanded(props.rid)
        return theme.palette.warning.dark;
      } else {
        return theme.palette.primary.dark;
      }
    } else if (theme.palette.mode === "light") {
      if (!to.delay_valid || to.delay >= 30 ) {
        return theme.palette.error.light;
      }
      if ( to.delay > 0 && to.delay < 30
      ) {
        // setExpanded(value[0])
        return theme.palette.warning.light;
      } else {
        return theme.palette.primary.light;
      }
    }
  }

  return (
    <React.Fragment>
      <Accordion
        TransitionProps={{ unmountOnExit: true }}
        expanded={expanded === props.rid}
        onChange={handleChange(props.rid)}
        disableGutters={true}
        sx={{ borderRadius: 1 }}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls={props.rid}
          id={props.rid}
          sx={{
            backgroundColor: statusTheme(theme, props.detials.to),
            borderRadius: 1,
          }}
        >
          <Typography variant="body1" style={{ fontWeight: 600 }}>
            {new Date(
              props.detials.from.sched_depart
            ).toLocaleDateString("en-GB")}{" "}
            {new Date(
              props.detials.from.sched_depart
            ).toLocaleTimeString("en-GB", {
              hour: "2-digit",
              minute: "2-digit",
            })}
            {" - "}
            {new Date(
              props.detials.to.sched_arrival
            ).toLocaleTimeString("en-GB", {
              hour: "2-digit",
              minute: "2-digit",
            })}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Station</TableCell>
                <TableCell>Scheduled</TableCell>
                <TableCell>Actual</TableCell>
                <TableCell>Delay</TableCell>
                {/* Show Details column if delayed or reason provided*/}
                {((
                  (props.detials.from.delay_valid &&
                  props.detials.from.delay > 0 &&
                  props.detials.from.late_cancel_reason > 0) ||
                  (!props.detials.from.delay_valid &&
                    props.detials.from.late_cancel_reason > 0)
                  ) || (
                  (props.detials.to.delay_valid &&
                  props.detials.to.delay > 0 &&
                  props.detials.to.late_cancel_reason > 0) ||
                  (!props.detials.to.delay_valid &&
                    props.detials.to.late_cancel_reason > 0)              
                  )) && (
                  <TableCell>Details</TableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                {/* Departure */}
                <TableCell>{props.detials.from.station}</TableCell>
                <TableCell>
                  {new Date(
                    props.detials.from.sched_depart
                  ).toLocaleTimeString("en-GB", {
                    hour: "2-digit",
                    minute: "2-digit",
                  })}
                </TableCell>
                {props.detials.from.delay_valid && (
                  <>
                    <TableCell>
                      {new Date(
                        props.detials.from.actual_depart
                      ).toLocaleTimeString("en-GB", {
                        hour: "2-digit",
                        minute: "2-digit",
                      })}
                    </TableCell>
                    <TableCell>
                      {props.detials.from.delay}
                    </TableCell>
                    {props.detials.from.delay > 0 &&
                    props.detials.from.late_cancel_reason > 0 && (
                      <TableCell>
                        {props.reasons[props.detials.from.late_cancel_reason].reason}
                      </TableCell>
                    )}
                  </>
                )}
                {!props.detials.from.delay_valid && (
                  <>
                    <TableCell>Unknown</TableCell>
                    <TableCell>N/A</TableCell>
                    {props.detials.from.late_cancel_reason > 0 && (
                      <TableCell>
                        {props.reasons[props.detials.from.late_cancel_reason].reason}
                      </TableCell>
                    )}
                    {/* {props.detials.from.late_cancel_reason === 0 && (
                      <TableCell>No reason provided</TableCell>
                    )} */}
                  </>
                )}
              </TableRow>
              <TableRow>
                {/* Arrival */}
                <TableCell>{props.detials.to.station}</TableCell>
                <TableCell>
                  {new Date(
                    props.detials.to.sched_arrival
                  ).toLocaleTimeString("en-GB", {
                    hour: "2-digit",
                    minute: "2-digit",
                  })}
                </TableCell>
                {props.detials.to.delay_valid && (
                  <>
                    <TableCell>
                      {new Date(
                        props.detials.to.actual_arrival
                      ).toLocaleTimeString("en-GB", {
                        hour: "2-digit",
                        minute: "2-digit",
                      })}
                    </TableCell>
                    <TableCell>
                      {props.detials.to.delay}
                    </TableCell>
                    {props.detials.to.delay > 0 &&
                    props.detials.to.late_cancel_reason > 0 && (
                      <TableCell>
                        {props.reasons[props.detials.to.late_cancel_reason].reason}
                      </TableCell>
                    )}
                  </>
                )}
                {!props.detials.to.delay_valid && (
                  <>
                    <TableCell>Unknown</TableCell>
                    <TableCell>N/A</TableCell>
                    {props.detials.to.late_cancel_reason > 0 && (
                      <TableCell>
                        {props.reasons[props.detials.to.late_cancel_reason].reason}
                      </TableCell>
                    )}
                    {/* {props.detials.to.late_cancel_reason === 0 && (
                      <TableCell>No reason provided</TableCell>
                    )} */}
                  </>
                )}
              </TableRow>
            </TableBody>
          </Table>
        </AccordionDetails>
      </Accordion>
    </React.Fragment>
  );
}
