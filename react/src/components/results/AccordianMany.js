import React from "react";
import AccordionSingle from "./AccordianSingle";
import {reasons} from "../../services/reason_codes"

export default function AccordianMany(props) {

  // Sort Results RIDs by the from.sched_depart
  let sorted = Object.keys(props.results).sort((a, b) => {
    return (
      new Date(props.results[a].from.sched_depart) -
      new Date(props.results[b].from.sched_depart)
    )
  })

  return (
    <React.Fragment>
        { sorted.map((rid) =>
          <AccordionSingle 
            detials={props.results[rid]} 
            rid={rid} 
            reasons={reasons}
            key={rid}/>
        )}
    </React.Fragment>
  );
}
