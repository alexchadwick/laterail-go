import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import LinearProgress from '@mui/material/LinearProgress';
import PropTypes from 'prop-types';

export default function Waiting(props) {
  // Update metrics requests progress bar
  const [progress, setProgress] = React.useState(10);
  React.useEffect(() => {
    setProgress((props.requestProgress / props.requestCount) * 100)
  }, [props.requestProgress]);

  return (
    <React.Fragment>
      <Grid
        container
        sx={{ px: 2 }} // Horizontal padding most evident on mobile
        rowSpacing={{ xs: 5 }}
        direction="row"
        justifyContent="center"
        alignItems="flex-start"
      >
        <Grid item xs={0.5} md={12}></Grid> {/* spacing*/}
        <Grid item xs={12} md={5}>
          <Typography variant="h3" align="center" gutterBottom>
            Request Submitted
          </Typography>
          <Box sx={{ py: 2, backgroundColor: "warning.main" }}>
            <Typography variant="body1" align="center" component="p">
              {"Waiting for response from the "}
              <Link href="https://wiki.openraildata.com/HSP" color="inherit">
                Open Rail Data Historical Service Performance API
              </Link>
            </Typography>
            <Typography variant="body1" align="left" sx={{ pl: 4 }}>
              <strong>Request ID:</strong> {props.requestID} <br />
              <strong>Number of requests:</strong> {props.requestCount} <br />
            </Typography>
            <LinearProgressWithLabel value={progress} thickness={4} sx={{ pl: 4 }}/>
          </Box>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

function LinearProgressWithLabel(props) {
  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      <Box sx={{ width: '100%', mr: 1 }}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="text.secondary">{`${Math.round(
          props.value,
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

LinearProgressWithLabel.propTypes = {
  /**
   * The value of the progress indicator for the determinate and buffer variants.
   * Value between 0 and 100.
   */
  value: PropTypes.number.isRequired,
};