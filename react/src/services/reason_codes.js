export const reasons = {
  "100": {
    "reason": "broken down train"
  },
  "101": {
    "reason": "delay on a previous journey"
  },
  "102": {
    "reason": "derailed train"
  },
  "104": {
    "reason": "fire at a station"
  },
  "105": {
    "reason": "fire at a station earlier"
  },
  "106": {
    "reason": "landslip"
  },
  "107": {
    "reason": "line-side fire"
  },
  "108": {
    "reason": "member of train crew being unavailable"
  },
  "109": {
    "reason": "passenger being taken ill"
  },
  "110": {
    "reason": "passenger having been taken ill earlier"
  },
  "111": {
    "reason": "person hit by a train"
  },
  "112": {
    "reason": "person hit by a train earlier"
  },
  "113": {
    "reason": "problem at a level crossing"
  },
  "114": {
    "reason": "problem currently under investigation"
  },
  "115": {
    "reason": "problem near the railway"
  },
  "116": {
    "reason": "problem with a river bridge"
  },
  "117": {
    "reason": "problem with line side equipment"
  },
  "118": {
    "reason": "security alert"
  },
  "119": {
    "reason": "train derailed earlier"
  },
  "120": {
    "reason": "train fault"
  },
  "121": {
    "reason": "train late from the depot"
  },
  "122": {
    "reason": "train late from the depot earlier"
  },
  "123": {
    "reason": "trespass incident"
  },
  "124": {
    "reason": "vehicle striking a bridge"
  },
  "125": {
    "reason": "vehicle striking a bridge earlier"
  },
  "126": {
    "reason": "earlier broken down train"
  },
  "128": {
    "reason": "earlier landslip"
  },
  "129": {
    "reason": "earlier line-side fire"
  },
  "130": {
    "reason": "earlier operating incident"
  },
  "131": {
    "reason": "earlier problem at a level crossing"
  },
  "132": {
    "reason": "earlier problem near the railway"
  },
  "133": {
    "reason": "earlier problem with a river bridge"
  },
  "134": {
    "reason": "earlier problem with line side equipment"
  },
  "135": {
    "reason": "earlier security alert"
  },
  "136": {
    "reason": "earlier train fault"
  },
  "137": {
    "reason": "earlier trespass incident"
  },
  "138": {
    "reason": "obstruction on the line"
  },
  "139": {
    "reason": "obstruction on the line earlier"
  },
  "140": {
    "reason": "operating incident"
  },
  "141": {
    "reason": "unusually large passenger flow"
  },
  "142": {
    "reason": "unusually large passenger flow earlier"
  },
  "143": {
    "reason": "animals on the line"
  },
  "144": {
    "reason": "animals on the line earlier"
  },
  "145": {
    "reason": "congestion caused by earlier delays"
  },
  "146": {
    "reason": "disruptive passengers"
  },
  "147": {
    "reason": "disruptive passengers earlier"
  },
  "148": {
    "reason": "earlier electrical supply problems"
  },
  "149": {
    "reason": "earlier emergency engineering works"
  },
  "150": {
    "reason": "earlier industrial action"
  },
  "151": {
    "reason": "earlier overhead wire problems"
  },
  "152": {
    "reason": "earlier over-running engineering works"
  },
  "153": {
    "reason": "earlier signalling problems"
  },
  "154": {
    "reason": "earlier vandalism"
  },
  "155": {
    "reason": "electrical supply problems"
  },
  "156": {
    "reason": "emergency engineering works"
  },
  "157": {
    "reason": "emergency services dealing with a prior incident"
  },
  "158": {
    "reason": "emergency services dealing with an incident"
  },
  "159": {
    "reason": "fire alarms sounding at a station"
  },
  "160": {
    "reason": "fire alarms sounding earlier at a station"
  },
  "161": {
    "reason": "flooding"
  },
  "162": {
    "reason": "flooding earlier"
  },
  "163": {
    "reason": "fog"
  },
  "164": {
    "reason": "fog earlier"
  },
  "165": {
    "reason": "high winds"
  },
  "166": {
    "reason": "high winds earlier"
  },
  "167": {
    "reason": "industrial action"
  },
  "168": {
    "reason": "lightning having damaged equipment"
  },
  "169": {
    "reason": "overhead wire problems"
  },
  "170": {
    "reason": "over-running engineering works"
  },
  "171": {
    "reason": "passengers transferring between trains"
  },
  "172": {
    "reason": "passengers transferring between trains earlier"
  },
  "173": {
    "reason": "poor rail conditions"
  },
  "174": {
    "reason": "poor rail conditions earlier"
  },
  "175": {
    "reason": "poor weather conditions"
  },
  "176": {
    "reason": "poor weather conditions earlier"
  },
  "177": {
    "reason": "safety checks being made"
  },
  "178": {
    "reason": "safety checks having been made earlier"
  },
  "179": {
    "reason": "signalling problems"
  },
  "180": {
    "reason": "snow"
  },
  "181": {
    "reason": "snow earlier"
  },
  "182": {
    "reason": "speed restrictions having been imposed"
  },
  "183": {
    "reason": "train crew having been unavailable earlier"
  },
  "184": {
    "reason": "vandalism"
  },
  "185": {
    "reason": "waiting earlier for a train crew member"
  },
  "186": {
    "reason": "waiting for a train crew member"
  },
  "187": {
    "reason": "engineering works"
  },
  "501": {
    "reason": "broken down train"
  },
  "502": {
    "reason": "broken windscreen on the train"
  },
  "503": {
    "reason": "shortage of trains because of accident damage"
  },
  "504": {
    "reason": "shortage of trains because of extra safety inspections"
  },
  "505": {
    "reason": "shortage of trains because of vandalism"
  },
  "506": {
    "reason": "shortage of trains following damage by snow and ice"
  },
  "507": {
    "reason": "more trains than usual needing repairs at the same time"
  },
  "508": {
    "reason": "the train for this service having broken down"
  },
  "509": {
    "reason": "this train breaking down"
  },
  "510": {
    "reason": "collision between trains"
  },
  "511": {
    "reason": "collision with the buffers at a station"
  },
  "512": {
    "reason": "derailed train"
  },
  "513": {
    "reason": "derailment within the depot"
  },
  "514": {
    "reason": "low speed derailment"
  },
  "515": {
    "reason": "train being involved in an accident"
  },
  "516": {
    "reason": "trains being involved in an accident"
  },
  "517": {
    "reason": "fire at a station"
  },
  "518": {
    "reason": "fire at a station earlier today"
  },
  "519": {
    "reason": "landslip"
  },
  "520": {
    "reason": "fire next to the track"
  },
  "521": {
    "reason": "fire on a train"
  },
  "522": {
    "reason": "member of on train staff being taken ill"
  },
  "523": {
    "reason": "shortage of on train staff"
  },
  "524": {
    "reason": "shortage of train conductors"
  },
  "525": {
    "reason": "shortage of train crew"
  },
  "526": {
    "reason": "shortage of train drivers"
  },
  "527": {
    "reason": "shortage of train guards"
  },
  "528": {
    "reason": "shortage of train managers"
  },
  "529": {
    "reason": "severe weather preventing train crew getting to work"
  },
  "530": {
    "reason": "the train conductor being taken ill"
  },
  "531": {
    "reason": "the train driver being taken ill"
  },
  "532": {
    "reason": "the train guard being taken ill"
  },
  "533": {
    "reason": "the train manager being taken ill"
  },
  "534": {
    "reason": "passenger being taken ill at a station"
  },
  "535": {
    "reason": "passenger being taken ill on a train"
  },
  "536": {
    "reason": "passenger being taken ill on this train"
  },
  "537": {
    "reason": "passenger being taken ill at a station earlier today"
  },
  "538": {
    "reason": "passenger being taken ill on a train earlier today"
  },
  "539": {
    "reason": "passenger being taken ill on this train earlier in its journey"
  },
  "540": {
    "reason": "person being hit by a train"
  },
  "541": {
    "reason": "person being hit by a train earlier today"
  },
  "542": {
    "reason": "collision at a level crossing"
  },
  "543": {
    "reason": "fault with barriers at a level crossing"
  },
  "544": {
    "reason": "road accident at a level crossing"
  },
  "545": {
    "reason": "road vehicle colliding with level crossing barriers"
  },
  "546": {
    "reason": "road vehicle damaging track at a level crossing"
  },
  "547": {
    "reason": "problem currently under investigation"
  },
  "548": {
    "reason": "burst water main near the railway"
  },
  "549": {
    "reason": "chemical spillage near the railway"
  },
  "550": {
    "reason": "fire near the railway involving gas cylinders"
  },
  "551": {
    "reason": "fire near the railway suspected to involve gas cylinders"
  },
  "552": {
    "reason": "fire on property near the railway"
  },
  "553": {
    "reason": "gas leak near the railway"
  },
  "554": {
    "reason": "road accident near the railway"
  },
  "555": {
    "reason": "wartime bomb near the railway"
  },
  "556": {
    "reason": "ambulance service dealing with an incident near the railway"
  },
  "557": {
    "reason": "emergency services dealing with an incident near the railway"
  },
  "558": {
    "reason": "fire brigade dealing with an incident near the railway"
  },
  "559": {
    "reason": "police dealing with an incident near the railway"
  },
  "560": {
    "reason": "boat colliding with a bridge"
  },
  "561": {
    "reason": "fault with a swing bridge over a river"
  },
  "562": {
    "reason": "problem with a river bridge"
  },
  "563": {
    "reason": "problem with line-side equipment"
  },
  "564": {
    "reason": "security alert at a station"
  },
  "565": {
    "reason": "security alert on another train"
  },
  "566": {
    "reason": "security alert on this train"
  },
  "567": {
    "reason": "train derailment earlier today"
  },
  "568": {
    "reason": "train derailment yesterday"
  },
  "569": {
    "reason": "fault occurring when attaching a part of a train"
  },
  "570": {
    "reason": "fault occurring when attaching a part of this train"
  },
  "571": {
    "reason": "fault occurring when detaching a part of a train"
  },
  "572": {
    "reason": "fault occurring when detaching a part of this train"
  },
  "573": {
    "reason": "fault on a train in front of this one"
  },
  "574": {
    "reason": "fault on this train"
  },
  "575": {
    "reason": "this train being late from the depot"
  },
  "576": {
    "reason": "trespassers on the railway"
  },
  "577": {
    "reason": "bus colliding with a bridge"
  },
  "578": {
    "reason": "lorry colliding with a bridge"
  },
  "579": {
    "reason": "road vehicle colliding with a bridge"
  },
  "580": {
    "reason": "bus colliding with a bridge earlier on this train's journey"
  },
  "581": {
    "reason": "bus colliding with a bridge earlier today"
  },
  "582": {
    "reason": "lorry colliding with a bridge earlier on this train's journey"
  },
  "583": {
    "reason": "lorry colliding with a bridge earlier today"
  },
  "584": {
    "reason": "road vehicle colliding with a bridge earlier on this train's journey"
  },
  "585": {
    "reason": "road vehicle colliding with a bridge earlier today"
  },
  "586": {
    "reason": "broken down train earlier today"
  },
  "587": {
    "reason": "earlier landslip"
  },
  "588": {
    "reason": "fire next to the track earlier today"
  },
  "589": {
    "reason": "fire on a train earlier today"
  },
  "590": {
    "reason": "coach becoming uncoupled on a train earlier in its journey"
  },
  "591": {
    "reason": "coach becoming uncoupled on a train earlier today"
  },
  "592": {
    "reason": "coach becoming uncoupled on this train earlier in its journey"
  },
  "593": {
    "reason": "coach becoming uncoupled on this train earlier today"
  },
  "594": {
    "reason": "train not stopping at a station it was supposed to earlier in its journey"
  },
  "595": {
    "reason": "train not stopping at a station it was supposed to earlier today"
  },
  "596": {
    "reason": "train not stopping in the correct position at a station earlier in its journey"
  },
  "597": {
    "reason": "train not stopping in the correct position at a station earlier today"
  },
  "598": {
    "reason": "train's automatic braking system being activated earlier in its journey"
  },
  "599": {
    "reason": "train's automatic braking system being activated earlier today"
  },
  "600": {
    "reason": "operational incident earlier in its journey"
  },
  "601": {
    "reason": "operational incident earlier today"
  },
  "602": {
    "reason": "this train not stopping at a station it was supposed to earlier in its journey"
  },
  "603": {
    "reason": "this train not stopping at a station it was supposed to earlier today"
  },
  "604": {
    "reason": "this train not stopping in the correct position at a station earlier in its journey"
  },
  "605": {
    "reason": "this train not stopping in the correct position at a station earlier today"
  },
  "606": {
    "reason": "this train's automatic braking system being activated earlier in its journey"
  },
  "607": {
    "reason": "this train's automatic braking system being activated earlier today"
  },
  "608": {
    "reason": "collision at a level crossing earlier today"
  },
  "609": {
    "reason": "collision at a level crossing yesterday"
  },
  "610": {
    "reason": "fault with barriers at a level crossing earlier today"
  },
  "611": {
    "reason": "fault with barriers at a level crossing yesterday"
  },
  "612": {
    "reason": "road accident at a level crossing earlier today"
  },
  "613": {
    "reason": "road accident at a level crossing yesterday"
  },
  "614": {
    "reason": "road vehicle colliding with level crossing barriers earlier today"
  },
  "615": {
    "reason": "road vehicle colliding with level crossing barriers yesterday"
  },
  "616": {
    "reason": "road vehicle damaging track at a level crossing earlier today"
  },
  "617": {
    "reason": "road vehicle damaging track at a level crossing yesterday"
  },
  "618": {
    "reason": "burst water main near the railway earlier today"
  },
  "619": {
    "reason": "burst water main near the railway yesterday"
  },
  "620": {
    "reason": "chemical spillage near the railway earlier today"
  },
  "621": {
    "reason": "chemical spillage near the railway yesterday"
  },
  "622": {
    "reason": "fire near the railway involving gas cylinders earlier today"
  },
  "623": {
    "reason": "fire near the railway involving gas cylinders yesterday"
  },
  "624": {
    "reason": "fire near the railway suspected to involve gas cylinders earlier today"
  },
  "625": {
    "reason": "fire near the railway suspected to involve gas cylinders yesterday"
  },
  "626": {
    "reason": "fire on property near the railway earlier today"
  },
  "627": {
    "reason": "fire on property near the railway yesterday"
  },
  "628": {
    "reason": "gas leak near the railway earlier today"
  },
  "629": {
    "reason": "gas leak near the railway yesterday"
  },
  "630": {
    "reason": "road accident near the railway earlier today"
  },
  "631": {
    "reason": "road accident near the railway yesterday"
  },
  "632": {
    "reason": "wartime bomb near the railway earlier today"
  },
  "633": {
    "reason": "wartime bomb near the railway yesterday"
  },
  "634": {
    "reason": "wartime bomb which has now been made safe"
  },
  "635": {
    "reason": "ambulance service dealing with an incident near the railway earlier today"
  },
  "636": {
    "reason": "ambulance service dealing with an incident near the railway yesterday"
  },
  "637": {
    "reason": "emergency services dealing with an incident near the railway earlier today"
  },
  "638": {
    "reason": "emergency services dealing with an incident near the railway yesterday"
  },
  "639": {
    "reason": "fire brigade dealing with an incident near the railway earlier today"
  },
  "640": {
    "reason": "fire brigade dealing with an incident near the railway yesterday"
  },
  "641": {
    "reason": "police dealing with an incident near the railway earlier today"
  },
  "642": {
    "reason": "police dealing with an incident near the railway yesterday"
  },
  "643": {
    "reason": "boat colliding with a bridge earlier today"
  },
  "644": {
    "reason": "fault with a swing bridge over a river earlier today"
  },
  "645": {
    "reason": "problem with a river bridge earlier today"
  },
  "646": {
    "reason": "earlier problem with line-side equipment"
  },
  "647": {
    "reason": "security alert earlier today"
  },
  "648": {
    "reason": "fault on this train which is now fixed"
  },
  "649": {
    "reason": "trespassers on the railway earlier in this train's journey"
  },
  "650": {
    "reason": "trespassers on the railway earlier today"
  },
  "651": {
    "reason": "bicycle on the track"
  },
  "652": {
    "reason": "road vehicle blocking the railway"
  },
  "653": {
    "reason": "supermarket trolley on the track"
  },
  "654": {
    "reason": "train hitting an obstruction on the line"
  },
  "655": {
    "reason": "tree blocking the railway"
  },
  "656": {
    "reason": "obstruction on the track"
  },
  "657": {
    "reason": "checking reports of an obstruction on the line"
  },
  "658": {
    "reason": "this train hitting an obstruction on the line"
  },
  "659": {
    "reason": "bicycle on the track earlier on this train's journey"
  },
  "660": {
    "reason": "bicycle on the track earlier today"
  },
  "661": {
    "reason": "road vehicle blocking the railway earlier on this train's journey"
  },
  "662": {
    "reason": "road vehicle blocking the railway earlier today"
  },
  "663": {
    "reason": "supermarket trolley on the track earlier on this train's journey"
  },
  "664": {
    "reason": "supermarket trolley on the track earlier today"
  },
  "665": {
    "reason": "train hitting an obstruction on the line earlier on this train's journey"
  },
  "666": {
    "reason": "train hitting an obstruction on the line earlier today"
  },
  "667": {
    "reason": "tree blocking the railway earlier on this train's journey"
  },
  "668": {
    "reason": "tree blocking the railway earlier today"
  },
  "669": {
    "reason": "obstruction on the track earlier on this train's journey"
  },
  "670": {
    "reason": "obstruction on the track earlier today"
  },
  "671": {
    "reason": "checking reports of an obstruction on the line earlier on this train's journey"
  },
  "672": {
    "reason": "checking reports of an obstruction on the line earlier today"
  },
  "673": {
    "reason": "this train hitting an obstruction on the line earlier in its journey"
  },
  "674": {
    "reason": "this train hitting an obstruction on the line earlier on this train's journey"
  },
  "675": {
    "reason": "this train hitting an obstruction on the line earlier today"
  },
  "676": {
    "reason": "coach becoming uncoupled on a train"
  },
  "677": {
    "reason": "coach becoming uncoupled on this train"
  },
  "678": {
    "reason": "train not stopping at a station it was supposed to"
  },
  "679": {
    "reason": "train not stopping in the correct position at a station"
  },
  "680": {
    "reason": "train's automatic braking system being activated"
  },
  "681": {
    "reason": "operational incident"
  },
  "682": {
    "reason": "this train not stopping at a station it was supposed to"
  },
  "683": {
    "reason": "this train not stopping in the correct position at a station"
  },
  "684": {
    "reason": "this train's automatic braking system being activated"
  },
  "685": {
    "reason": "overcrowding"
  },
  "686": {
    "reason": "overcrowding as this train has fewer coaches than normal"
  },
  "687": {
    "reason": "overcrowding because an earlier train had fewer coaches than normal"
  },
  "688": {
    "reason": "overcrowding because of a concert"
  },
  "689": {
    "reason": "overcrowding because of a football match"
  },
  "690": {
    "reason": "overcrowding because of a marathon"
  },
  "691": {
    "reason": "overcrowding because of a rugby match"
  },
  "692": {
    "reason": "overcrowding because of a sporting event"
  },
  "693": {
    "reason": "overcrowding because of an earlier cancellation"
  },
  "694": {
    "reason": "overcrowding because of an event"
  },
  "695": {
    "reason": "overcrowding earlier on this train's journey"
  },
  "696": {
    "reason": "animals on the railway"
  },
  "697": {
    "reason": "cattle on the railway"
  },
  "698": {
    "reason": "horses on the railway"
  },
  "699": {
    "reason": "sheep on the railway"
  },
  "700": {
    "reason": "animals on the railway earlier today"
  },
  "701": {
    "reason": "cattle on the railway earlier today"
  },
  "702": {
    "reason": "horses on the railway earlier today"
  },
  "703": {
    "reason": "sheep on the railway earlier today"
  },
  "704": {
    "reason": "passengers causing a disturbance on a train"
  },
  "705": {
    "reason": "passengers causing a disturbance on this train"
  },
  "706": {
    "reason": "passengers causing a disturbance earlier in this train's journey"
  },
  "707": {
    "reason": "passengers causing a disturbance on a train earlier today"
  },
  "708": {
    "reason": "fault with the electric third rail earlier on this train's journey"
  },
  "709": {
    "reason": "fault with the electric third rail earlier today"
  },
  "710": {
    "reason": "damage to the electric third rail earlier on this train's journey"
  },
  "711": {
    "reason": "damage to the electric third rail earlier today"
  },
  "712": {
    "reason": "failure of the electricity supply earlier on this train's journey"
  },
  "713": {
    "reason": "failure of the electricity supply earlier today"
  },
  "714": {
    "reason": "the electricity being switched off for safety reasons earlier on this train's journey"
  },
  "715": {
    "reason": "the electricity being switched off for safety reasons earlier today"
  },
  "716": {
    "reason": "urgent repairs to a bridge earlier today"
  },
  "717": {
    "reason": "urgent repairs to a tunnel earlier today"
  },
  "718": {
    "reason": "urgent repairs to the railway earlier today"
  },
  "719": {
    "reason": "urgent repairs to the track earlier today"
  },
  "720": {
    "reason": "expected industrial action earlier today"
  },
  "721": {
    "reason": "expected industrial action yesterday"
  },
  "722": {
    "reason": "industrial action earlier today"
  },
  "723": {
    "reason": "industrial action yesterday"
  },
  "724": {
    "reason": "object being caught on the overhead electric wires earlier on this train's journey"
  },
  "725": {
    "reason": "object being caught on the overhead electric wires earlier today"
  },
  "726": {
    "reason": "damage to the overhead electric wires earlier on this train's journey"
  },
  "727": {
    "reason": "damage to the overhead electric wires earlier today"
  },
  "728": {
    "reason": "earlier engineering works not being finished on time"
  },
  "729": {
    "reason": "fault with the on train signalling system earlier on this train's journey"
  },
  "730": {
    "reason": "fault with the on train signalling system earlier today"
  },
  "733": {
    "reason": "fault with the signalling system earlier on this train's journey"
  },
  "734": {
    "reason": "fault with the signalling system earlier today"
  },
  "737": {
    "reason": "the fire alarm sounding in a signalbox earlier on this train's journey"
  },
  "738": {
    "reason": "the fire alarm sounding in a signalbox earlier today"
  },
  "739": {
    "reason": "the fire alarm sounding in the signalling centre earlier on this train's journey"
  },
  "740": {
    "reason": "the fire alarm sounding in the signalling centre earlier today"
  },
  "741": {
    "reason": "attempted theft of overhead line electrification equipment earlier today"
  },
  "742": {
    "reason": "attempted theft of overhead line electrification equipment yesterday"
  },
  "743": {
    "reason": "attempted theft of railway equipment earlier today"
  },
  "744": {
    "reason": "attempted theft of railway equipment yesterday"
  },
  "745": {
    "reason": "attempted theft of signalling cables earlier today"
  },
  "746": {
    "reason": "attempted theft of signalling cables yesterday"
  },
  "747": {
    "reason": "attempted theft of third rail electrification equipment earlier today"
  },
  "748": {
    "reason": "attempted theft of third rail electrification equipment yesterday"
  },
  "749": {
    "reason": "theft of overhead line electrification equipment earlier today"
  },
  "750": {
    "reason": "theft of overhead line electrification equipment yesterday"
  },
  "751": {
    "reason": "theft of railway equipment earlier today"
  },
  "752": {
    "reason": "theft of railway equipment yesterday"
  },
  "753": {
    "reason": "theft of signalling cables earlier today"
  },
  "754": {
    "reason": "theft of signalling cables yesterday"
  },
  "755": {
    "reason": "theft of third rail electrification equipment earlier today"
  },
  "756": {
    "reason": "theft of third rail electrification equipment yesterday"
  },
  "757": {
    "reason": "vandalism at a station earlier today"
  },
  "758": {
    "reason": "vandalism at a station yesterday"
  },
  "759": {
    "reason": "vandalism of railway equipment earlier today"
  },
  "760": {
    "reason": "vandalism of railway equipment yesterday"
  },
  "761": {
    "reason": "vandalism on a train earlier today"
  },
  "762": {
    "reason": "vandalism on a train yesterday"
  },
  "763": {
    "reason": "vandalism on this train earlier today"
  },
  "764": {
    "reason": "vandalism on this train yesterday"
  },
  "765": {
    "reason": "fault with the electric third rail"
  },
  "766": {
    "reason": "damage to the electric third rail"
  },
  "767": {
    "reason": "failure of the electricity supply"
  },
  "768": {
    "reason": "the electricity being switched off for safety reasons"
  },
  "769": {
    "reason": "urgent repairs to a bridge"
  },
  "770": {
    "reason": "urgent repairs to a tunnel"
  },
  "771": {
    "reason": "urgent repairs to the railway"
  },
  "772": {
    "reason": "urgent repairs to the track"
  },
  "773": {
    "reason": "the emergency services dealing with an incident earlier today"
  },
  "774": {
    "reason": "ambulance service dealing with an incident"
  },
  "775": {
    "reason": "fire brigade dealing with an incident"
  },
  "776": {
    "reason": "police dealing with an incident"
  },
  "777": {
    "reason": "the emergency services dealing with an incident"
  },
  "778": {
    "reason": "the fire alarm sounding at a station"
  },
  "779": {
    "reason": "the fire alarm sounding at a station earlier today"
  },
  "780": {
    "reason": "burst water main flooding the railway"
  },
  "781": {
    "reason": "river flooding the railway"
  },
  "782": {
    "reason": "flood water making the railway potentially unsafe"
  },
  "783": {
    "reason": "flooding"
  },
  "784": {
    "reason": "heavy rain flooding the railway"
  },
  "785": {
    "reason": "predicted flooding"
  },
  "786": {
    "reason": "the sea flooding the railway"
  },
  "787": {
    "reason": "burst water main flooding the railway earlier today"
  },
  "788": {
    "reason": "river flooding the railway earlier today"
  },
  "789": {
    "reason": "flood water making the railway potentially unsafe earlier today"
  },
  "790": {
    "reason": "flooding earlier in this train's journey"
  },
  "791": {
    "reason": "flooding earlier today"
  },
  "792": {
    "reason": "heavy rain flooding the railway earlier today"
  },
  "793": {
    "reason": "predicted flooding earlier today"
  },
  "794": {
    "reason": "the sea flooding the railway earlier today"
  },
  "795": {
    "reason": "thick fog"
  },
  "796": {
    "reason": "thick fog earlier in this train's journey"
  },
  "797": {
    "reason": "thick fog earlier today"
  },
  "798": {
    "reason": "forecasted high winds"
  },
  "799": {
    "reason": "high winds"
  },
  "800": {
    "reason": "high winds earlier in this train's journey"
  },
  "801": {
    "reason": "high winds earlier today"
  },
  "802": {
    "reason": "expected industrial action"
  },
  "803": {
    "reason": "industrial action"
  },
  "804": {
    "reason": "lightning damaging a station"
  },
  "805": {
    "reason": "lightning damaging a train"
  },
  "806": {
    "reason": "lightning damaging equipment"
  },
  "807": {
    "reason": "lightning damaging the electricity supply"
  },
  "808": {
    "reason": "lightning damaging the signalling system"
  },
  "809": {
    "reason": "lightning damaging this train"
  },
  "810": {
    "reason": "object being caught on the overhead electric wires"
  },
  "811": {
    "reason": "damage to the overhead electric wires"
  },
  "812": {
    "reason": "engineering works not being finished on time"
  },
  "813": {
    "reason": "forecasted slippery rails"
  },
  "814": {
    "reason": "ice preventing this train getting electricity from the third rail"
  },
  "815": {
    "reason": "ice preventing trains getting electricity from the third rail"
  },
  "816": {
    "reason": "slippery rails"
  },
  "817": {
    "reason": "slippery rails earlier in this train's journey"
  },
  "818": {
    "reason": "slippery rails earlier today"
  },
  "819": {
    "reason": "forecasted severe weather"
  },
  "820": {
    "reason": "severe weather"
  },
  "821": {
    "reason": "severe weather earlier"
  },
  "822": {
    "reason": "severe weather earlier in this train's journey"
  },
  "823": {
    "reason": "severe weather earlier today"
  },
  "824": {
    "reason": "safety inspection of the track"
  },
  "825": {
    "reason": "safety inspection on a train"
  },
  "826": {
    "reason": "safety inspection on this train"
  },
  "827": {
    "reason": "safety inspection of the track earlier today"
  },
  "828": {
    "reason": "safety inspection on a train earlier today"
  },
  "829": {
    "reason": "safety inspection on this train earlier in its journey"
  },
  "830": {
    "reason": "fault with the on train signalling system"
  },
  "831": {
    "reason": "fault with the radio system between the driver and the signaller"
  },
  "832": {
    "reason": "fault with the signalling system"
  },
  "834": {
    "reason": "the fire alarm sounding in a signalbox"
  },
  "835": {
    "reason": "the fire alarm sounding in the signalling centre"
  },
  "836": {
    "reason": "forecasted heavy snow"
  },
  "837": {
    "reason": "heavy snow"
  },
  "838": {
    "reason": "heavy snow earlier in this train's journey"
  },
  "839": {
    "reason": "heavy snow earlier today"
  },
  "840": {
    "reason": "heavy snow over recent days"
  },
  "841": {
    "reason": "speed restriction"
  },
  "842": {
    "reason": "speed restriction because of fog"
  },
  "843": {
    "reason": "speed restriction because of fog earlier on this train's journey"
  },
  "844": {
    "reason": "speed restriction because of fog earlier today"
  },
  "845": {
    "reason": "speed restriction because of heavy rain"
  },
  "846": {
    "reason": "speed restriction because of heavy rain earlier on this train's journey"
  },
  "847": {
    "reason": "speed restriction because of heavy rain earlier today"
  },
  "848": {
    "reason": "speed restriction because of high track temperatures"
  },
  "849": {
    "reason": "speed restriction because of high track temperatures earlier on this train's journey"
  },
  "850": {
    "reason": "speed restriction because of high track temperatures earlier today"
  },
  "851": {
    "reason": "speed restriction because of high winds"
  },
  "852": {
    "reason": "speed restriction because of high winds earlier on this train's journey"
  },
  "853": {
    "reason": "speed restriction because of high winds earlier today"
  },
  "854": {
    "reason": "speed restriction because of severe weather"
  },
  "855": {
    "reason": "speed restriction because of severe weather earlier on this train's journey"
  },
  "856": {
    "reason": "speed restriction because of severe weather earlier today"
  },
  "857": {
    "reason": "speed restriction because of snow and ice"
  },
  "858": {
    "reason": "speed restriction because of snow and ice earlier on this train's journey"
  },
  "859": {
    "reason": "speed restriction because of snow and ice earlier today"
  },
  "860": {
    "reason": "speed restriction earlier on this train's journey"
  },
  "861": {
    "reason": "speed restriction earlier today"
  },
  "862": {
    "reason": "speed restriction in a tunnel"
  },
  "863": {
    "reason": "speed restriction in a tunnel earlier on this train's journey"
  },
  "864": {
    "reason": "speed restriction in a tunnel earlier today"
  },
  "865": {
    "reason": "speed restriction over a bridge"
  },
  "866": {
    "reason": "speed restriction over a bridge earlier on this train's journey"
  },
  "867": {
    "reason": "speed restriction over a bridge earlier today"
  },
  "868": {
    "reason": "speed restriction over an embankment"
  },
  "869": {
    "reason": "speed restriction over an embankment earlier on this train's journey"
  },
  "870": {
    "reason": "speed restriction over an embankment earlier today"
  },
  "871": {
    "reason": "speed restriction over defective track"
  },
  "872": {
    "reason": "speed restriction over defective track earlier on this train's journey"
  },
  "873": {
    "reason": "speed restriction over defective track earlier today"
  },
  "874": {
    "reason": "attempted theft of overhead line electrification equipment"
  },
  "875": {
    "reason": "attempted theft of railway equipment"
  },
  "876": {
    "reason": "attempted theft of signalling cables"
  },
  "877": {
    "reason": "attempted theft of third rail electrification equipment"
  },
  "878": {
    "reason": "theft of overhead line electrification equipment"
  },
  "879": {
    "reason": "theft of railway equipment"
  },
  "880": {
    "reason": "theft of signalling cables"
  },
  "881": {
    "reason": "theft of third rail electrification equipment"
  },
  "882": {
    "reason": "vandalism at a station"
  },
  "883": {
    "reason": "vandalism of railway equipment"
  },
  "884": {
    "reason": "vandalism on a train"
  },
  "885": {
    "reason": "vandalism on this train"
  },
  "886": {
    "reason": "train crew being delayed"
  },
  "887": {
    "reason": "train crew being delayed by service disruption"
  },
  "888": {
    "reason": "bridge being damaged"
  },
  "889": {
    "reason": "bridge being damaged by a boat"
  },
  "890": {
    "reason": "bridge being damaged by a road vehicle"
  },
  "891": {
    "reason": "bridge having collapsed"
  },
  "892": {
    "reason": "broken rail"
  },
  "893": {
    "reason": "late departure while the train was cleaned specially"
  },
  "894": {
    "reason": "late running freight train"
  },
  "895": {
    "reason": "late running train being in front of this one"
  },
  "896": {
    "reason": "points failure"
  },
  "897": {
    "reason": "power cut at the station"
  },
  "898": {
    "reason": "problem with the station lighting"
  },
  "899": {
    "reason": "rail buckling in the heat"
  },
  "900": {
    "reason": "railway embankment being damaged"
  },
  "901": {
    "reason": "shortage of station staff"
  },
  "902": {
    "reason": "tunnel being closed for safety reasons"
  },
  "903": {
    "reason": "incident at the airport"
  },
  "904": {
    "reason": "congestion"
  },
  "905": {
    "reason": "the communication alarm being activated on a train"
  },
  "906": {
    "reason": "the communication alarm being activated on this train"
  },
  "907": {
    "reason": "the train departing late to maintain customer connections"
  },
  "908": {
    "reason": "the train making extra stops because a train was cancelled"
  },
  "909": {
    "reason": "the train making extra stops because of service disruption"
  },
  "910": {
    "reason": "waiting for a part of the train to be attached"
  },
  "911": {
    "reason": "fault on a train"
  },
  "912": {
    "reason": "problem with platform equipment"
  },
  "913": {
    "reason": "fault on a train earlier"
  },
  "914": {
    "reason": "issues with communication systems"
  },
  "915": {
    "reason": "problem in the depot"
  },
  "916": {
    "reason": "signalling staff being unavailable"
  },
  "917": {
    "reason": "staff training"
  },
  "918": {
    "reason": "short-notice change to the timetable"
  },
  "919": {
    "reason": "misuse of a level crossing"
  },
  "731": {
    "reason": "fault with the radio system between the driver and the signaller earlier on this train's journey"
  },
  "732": {
    "reason": "fault with the radio system between the driver and the signaller earlier today"
  },
  "735": {
    "reason": "signalling staff being taken ill earlier on this train's journey"
  },
  "736": {
    "reason": "signalling staff being taken ill earlier today"
  },
  "833": {
    "reason": "signalling staff being taken ill"
  }
}