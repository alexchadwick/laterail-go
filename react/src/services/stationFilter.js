// Return station name from array of station info, using station crs code
// Used on response of stationFilter
export function stationFind(crs, info) {
  const station = info.find(s => {
    return s.crs == crs
  })

  return station.name
}