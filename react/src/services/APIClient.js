import axios from "axios";
import { MockEvent, EventSource as MockEventSource } from 'mocksse';
import * as e from "./events.js"

const apiClient = axios.create({
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export function postSubmit(body) {
  return apiClient.post(window.config.producerURL + "/submit", body);
}

export function getDemoProducerResponse(isDemo) {
  return isDemo ? axios.get("/demo/producer_response.json") : null;
}

// Make submission request to Producer service
export function producerRequest(body, demoDelay, isDemo, setProducerResponse) {
  // Demo mode simulates API requests only
  if (isDemo === true) {
    setTimeout(
      () =>
        getDemoProducerResponse(isDemo)
          .then((resp) => {
            resp.status = 202
            setProducerResponse(resp);
          })
          .catch((error) => {
            console.log(error);
          }),
      demoDelay
    );
  } else {
    postSubmit(body)
      .then((resp) => {
        setProducerResponse(resp);
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

// Mock incoming SSE messages
export function initEventSource(URL, isDemo) {
  // Demo mode simulates SSE events
  if (isDemo === true) {
    // Instantiate a MockEvent.
    new MockEvent({
      url: URL,
      setInterval: 2000,
      responses: [
        { type: 'message', data: e.event0 },
        { type: 'message', data: e.event1 },
        { type: 'message', data: e.event2 },
        { type: 'message', data: e.event3 },
        { type: 'message', data: e.event4 },
        { type: 'message', data: e.event5 },
        { type: 'message', data: e.event6 },
        { type: 'message', data: e.event7 },
        { type: 'message', data: e.event8 },
      ]
    });
    return new MockEventSource(URL)
  } else {
    return new EventSource(URL)
  }
}
