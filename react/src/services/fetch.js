// Get all station info from Consumer service Stations endpoint
export async function fetchStations(isDemo, demoDelay) {
  let requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow'
  };

  let response
  if (isDemo) {
    await sleep(demoDelay)
    response = await fetch("/demo/stations.json")
  } else {
    response = await fetch(window.config.consumerURL + "/stations", requestOptions)
  }

  if (!response.ok){
    throw new Error(response.json())
  }
  return response.json()
}

// Get state of a request ID from Consumer service Track endpoint
export async function fetchTrack(body, isDemo, demoDelay) {
  let requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body),
    redirect: 'follow'
  };

  let response
  if (isDemo) {
    await sleep(demoDelay)
    response = await fetch("/demo/track_created.json")
  } else {
    response = await fetch(window.config.consumerURL + "/track", requestOptions)
  }

  if (!response.ok){
    throw new Error(response.json())
  }
  return response.json()
}

// Used to simulate external API call wait in Demo mode
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}