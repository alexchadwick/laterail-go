import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { initEventSource } from "../services/APIClient";
import Loading from "../components/results/Loading";
import Main from "../components/results/Main";
import Empty from "../components/results/Empty";
import Waiting from "../components/results/Waiting";
import Error from "../components/results/Error";
import { fetchTrack } from "../services/fetch";

export default function ResultsContent() {
  const isDemo = window.config.demoMode === "true";
  const demoDelay = 3000;
  const params = useParams()

  // Used to refetch queries
  const queryClient = useQueryClient()
  
  // Consumer service track request
  // Keep stale data forever once request is complete to reduce no. requests
  const cachedData = queryClient.getQueryData({
    queryKey: ['track', { type: params.id }]
  })
  const track = useQuery({
    queryKey: ['track', { type: params.id }],
    queryFn: async () => {
      return fetchTrack(
        { request_ids: [params.id] },
        isDemo,
        demoDelay,
      )
    },
    staleTime: typeof cachedData !== "undefined" &&
      cachedData[0].metadata.status === "complete" ? Infinity : 0,
  })

  // Open the SSE connection after Mount and tracker update, close on Unmount
  const [events, setEvents] = useState(null);
  useEffect( () => {
    if (!track.isSuccess) return;

    // Request is done if not "created"
    if (track.data[0].metadata.status !== "created") {
      setMetricsDone(true)
      setDetails(track.data[0])

      // Necessary for page refreshes where `events` already has a value
      setEvents(null)
    } else {
      const evtSource = initEventSource(
        window.config.eventsURL + track.data[0].request_id,
        isDemo,
      )
      setEvents(evtSource)
      
      // Close Eventsource connection when component unmounted
      return () => evtSource.close();
    }
  }, [track.isSuccess]);

  // Use SSE events
  const [eventMessage, setEventMessage] = useState(null);
  useEffect( () => {
    if (!events) return;

    events.onopen = (e) => {
      // console.log("The connection has been established. EventSource: ", e);
      console.log(events)
      // console.log("Waiting for Events...");
    };
    
    events.onerror = (e) => {
      console.log("EventSource error: ", e);

      // Workaround where Events service http.WriteTimeout is met
      // and the client fails to reconnect automatically
      if (events.readyState === EventSource.CLOSED) {
        console.log("reconnecting...");
        const evtSource = initEventSource(
          window.config.eventsURL + track.data[0].request_id,
          isDemo,
        )
        setEvents(evtSource)
      }
    };

    events.addEventListener("message", (event) => {
      let dataJson = JSON.parse(event.data)
      setEventMessage(dataJson)
    });

    return () => events.close();
  }, [events]);

  // Handle SSE event updates
  const [metricsDone, setMetricsDone] = useState(false);
  const [details, setDetails] = useState(null)
  const [metricsProgress, setMetricsProgress] = useState(0)
  useEffect( () => {
    if (!eventMessage) return
    
    // Close the EventSource connection if final message received
    if (eventMessage.metadata.status === "created") {
      setDetails(eventMessage)
      setMetricsProgress(eventMessage.metadata.metrics_done)
      // console.log("Event: ", eventMessage)
    } else if (eventMessage.metadata.status === "complete" ||
    eventMessage.metadata.status === "empty"){
      events.close()

      // Refetch the completed track request. Enables back/forward browser actions
      queryClient.refetchQueries({ queryKey: ['track', { type: params.id }] })
    }
    
    // If metrics requests finished use Results Main component
    if (!metricsDone && (eventMessage.metadata.metrics_total === eventMessage.metadata.metrics_done)){
      setMetricsDone(true)
      setMetricsProgress(eventMessage.metadata.metrics_done)
      // console.log("Metrics done")
    }

  }, [eventMessage])

  // Check response for errors or empty response (no matching services)
  const [error, setError] = useState(false);
  //   useEffect(() => {
  //     if (searchResponse.result !== undefined) {
  //       console.log("Returned", searchResponse.result.length, "services")
  //     } else if (searchResponse.info !== undefined) {
  //       console.log("No services returned")
  //     } else if (searchResponse.info !== undefined) {
  //     setError(true)
  //     console.log("API Error response")
  //   }
  // }, [searchResponse]);

  return (
    <React.Fragment>
      {/* Tracker loading screen */}
      {!track.isSuccess && (
        <Loading isDemo={isDemo} demoDelay={demoDelay} />
      )}
      {/* Metrics loading screen */}
      {track.isSuccess &&
        metricsDone !== true &&
        error === false && (
          <Waiting
            requestID={params.id}
            requestCount={track.data[0].metadata.metrics_total}
            requestProgress={metricsProgress}
          />
        )}
      {/* Results view */}
      { metricsDone === true &&
        error === false && (
          <Main
            body={track.data[0].initial_request}
            results={details} // May be undefined
            isDemo={isDemo}
            demoDelay={demoDelay}
            // summary={summary}
          />
        )}
      {/* No services returned
      {consumerRequestComplete === true &&
        producerResponse.info !== undefined &&
        error === false && <Empty />} */}
      {/* TODO: Error processing request */}
      {error === true && <Error />}
    </React.Fragment>
  );
}
