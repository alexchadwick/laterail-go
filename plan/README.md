# Features
## Main
Users search for UK train journeys and get a report of delays. Includes some basic stats/figures and a table showing how many journeys are eligibile for a refund

## [Scotrail delay repay table](https://www.scotrail.co.uk/plan-your-journey/our-delay-repay-guarantee)

|Ticket Type| 30 to 59 minutes | 1 hour to 1 hour 59 minutes |2 hours or more |
| ------ | ------ | ------ | ------ |
|Single| 50% of the cost | The full cost  |The full cost  |
|Return| 25% of the cost | 50% of the cost|The full cost  | 
|Season week | ticket cost ÷ 20 | ticket cost ÷ 10 |ticket cost ÷ 5|
|Season month | ticket cost ÷ 80| ticket cost ÷ 40 |ticket cost ÷ 20|

## Other planned
- User sign in: Save searches and track overall search history delay and refund stats
- General stats component on home page: Report aggregating data from all searched journeys
- Automatically keep data for previously searched services up to date: Run scheduled jobs to fetch new journeys for previously searched routes/services

# Process outline
## Summary
- Accept API request for ondemand user request
- Make requests to HSP APIs:
  - Parse initial/user request into valid serviceMetrics requests
  - Store new HSP serviceMetrics responses in relational database
  - Check if database already contains the HSP serviceDetails responses
  - If not, make HSP serviceDetails request for all new rid and store in database
- Calculate stats in frontend and show report to user

## Details
- Initial request: User submitted request incl.
  - Time of day range, e.g. 0800-1400
  - Date range, e.g. 2021-09-01, 2021-09-14
  - Start and end station, e.g. GLC, BDG
- Parse into list of HSP ServiceMetrics requests where:
  - time of day ranges length <= var irTimeRangeMax (6 hrs)
  - consecutive date range lenth <= var MetricRequestDateRangeMax (14 days)
  - date ranges are checked for HSP Metric day types (WEEK/SAT/SUNDAY)
  - Each time of day range is requested once per date range, per day type
    - E.g. Initial Request "0800-1400, 2021-09-01 to 2021-09-04, GLC to BDG"
      - 1 time of day range
      - 1 date range
      - 2 day types (WEEKDAY + SATURDAY only)
      ==> 2 HSP ServiceMetrics requests total
    - Initial Request "0600-2330, 2021-06-01 to 2021-08-31, GLC to BDG"
      - 3 time of day ranges, for irTimeRangeMax==6 (17.5 hr)
      - 7 date ranges, for MetricRequestDateRangeMax==14 (91 days)
      - 3 day types
      ==> 63 HSP ServiceMetrics requests total
- Make HSP ServiceDetails request for all returned ServiceMetrics rids, which don't already exist within the database

# Data Sources

Pull historic train data from the [Historical Service Performance (HSP) Darwin API](https://wiki.openraildata.com/index.php/HSP)
  - This includes info on completed or cancelled journeys. Unlike the [Darwin Push Port](https://wiki.openraildata.com/index.php?title=Darwin:Push_Port) which continuously streams updates

UK train station codes and coordinates
  - [ScotRail](https://www.scotrail.co.uk/cache/trainline_stations/abellio_franchise): All stations and location info
  - [NationalRail Stations](https://ojp.nationalrail.co.uk/find/stations/a?includeGroups=false&callback=json1): All stations, one document per letter of alphabet
  - [NationalRail Station groups](https://ojp.nationalrail.co.uk/find/stationsDLRLU/a): All stations and region groupings, one doc per letter of alphabet
  - [NationalRail Stations connected to origin](https://ojp.nationalrail.co.uk/find/stationsDLRLU/a/Bridgeton): Appears to show stations that can be reached from a specific station. One doc per letter of alphabet. Haven't found example where returned data doesn't match all stations yet.

Latest HSP delay reason codes are stored in an s3 bucket (find under National Rail Data Portal, "Darwin File Information")

## HSP serviceMetrics
Summary of journeys which depart within the input time range and matches the from and to locations. Returns journey RIDs, which are required for `serviceDetails` queries.

Returned data we care about:
  - Times are local to UK, not UTC
  - `Services.serviceAttributesMetrics.rids`: The run id. First 8 digits are the date, remaining are the service id. Where one service is typically run on many days
  - Considered using the Metrics tolerance values to judge lateness, but this approach evaluates the whole run, start to end. Ignoring delays between specific stations on a run.

## HSP serviceDetails
Details of all stops in run. Also returns cancelation reasons

## Known issues

HSP api will return 502 after 1 min if all workers are busy
  - Workaround: Retry requests if response http 502

Retries will continue to fail if the time period for the request is too large (unplublished limit)
  - Originally noticed issue when submitting requests with time duration == 1 day
    - Workaround: Submit requests in 4 hour batches (var MetricRequestMaxInterval or irTimeRangeMax)

HSP api will return 502 if more than 4 concurrent requests are made
  - Workaround: Limit to 4 workers in api.PostMetrics(), using a semaphore channel

# Misc Notes
- Database design
  - Intentionally using relational database for most data
    - May revist and test graph database
    - Want to pull stats about delays + cancellations between two stations
    - Used sqlc w/ pgx to generate go sql models from schema and queries
  - Storing station data used by the frontend as json in MongoDB
    - Used in input forms and to generate maps
    - Considered CouchDB, but significantly less popular/lower demand
    - May cache delay data used by frontend to mongo collection too
      - User specific stat summaries?
- HSP serviceMetrics requests return small time periods over many days very quickly
  - Initially tried to break serviceMetric requests into 6 hr periods, which didn't span days. Resulted in many more requests and much slower total response time
  - 6hr period, over 1 weekday = 25s
  - 6hr period, over 10 weekdays (2 weeks, filtered using days=WEEKDAY) = 15s (server side cache? Less competition for resource on service?)
    - Assume speed for grabbing one weekday period roughly same as many and grab up to 10 days per request
    - Requests will have max duration of 14 days, but the day type parameter ensures that the largest request period is really 10 days (type=WEEKDAY)
